<?php
namespace Ik\ProductAttributes\Setup;

use Magento\Framework\Setup\{
    ModuleContextInterface,
    ModuleDataSetupInterface,
    InstallDataInterface
};

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

use Ik\ProductAttributes\Model\Attributes;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        // Add EAN and MPN codes
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'ean',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'EAN',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'Product Details',
                'attribute_set' => 'Default',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'mpn',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'mpn',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'Product Details',
                'attribute_set' => 'Default',
            ]
        );

        // Add attributes for sizes
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'colours',
            [
                'group' => 'Product Details',
                'type' => 'varchar',
                'label' => 'Colours',
                'input' => 'select',
                'source' => Attributes\Source\Colours::class,
                'frontend' => Attributes\Frontend\Colours::class,
                'backend' => Attributes\Backend\Colours::class,
                'required' => false,
                'searchable' => true,
                'filterable' => true,
                'comparable' => true,
                'sort_order' => 50,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'visible' => true,
                'is_html_allowed_on_front' => true,
                'visible_on_front' => true,
                'visible_in_advanced_search' => true,
                'is_user_defined' => true,
            ]
        );
    }
}
