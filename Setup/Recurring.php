<?php

/*
  Rename this to Recurring.php - run once, and delete
*/

namespace Ik\ProductAttributes\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Db\Select;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface as UninstallInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Ik\ProductAttributes\Model\Attributes;

/**
 * Class Uninstall
 */
class Recurring implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $_eavSetupFactory;

    private $_mDSetup;
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        ModuleDataSetupInterface $mDSetup
    )
    {
        $this->_eavSetupFactory = $eavSetupFactory;
        $this->_mDSetup = $mDSetup;
    }

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $eavSetup = $this->_eavSetupFactory->create();
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'colours',
            [
                'group' => 'Product Details',
                'type' => 'varchar',
                'label' => 'Colours',
                'input' => 'select',
                'source' => Attributes\Source\Colours::class,
                'frontend' => Attributes\Frontend\Colours::class,
                'backend' => Attributes\Backend\Colours::class,
                'required' => false,
                'searchable' => true,
                'filterable' => true,
                'comparable' => true,
                'sort_order' => 50,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'visible' => true,
                'is_html_allowed_on_front' => true,
                'visible_on_front' => true,
                'visible_in_advanced_search' => true,
                'is_user_defined' => true,
            ]
        );
    }
}