<?php

namespace Ik\ProductAttributes\Model\Attributes\Source;

class Colours extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Get all options
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['label' => __('N/A'), 'value' => 'not_applicable'],
                ['label'=>__('TAN (SHINY)'),'value'=>'TAN (SHINY)'],
                ['label'=>__('SILVER (SHINY)'),'value'=>'SILVER (SHINY)'],
                ['label'=>__('BLUE STEEL (SHINY)'),'value'=>'BLUE STEEL (SHINY)'],
                ['label'=>__('GUNMETAL (SHINY)'),'value'=>'GUNMETAL (SHINY)'],
                ['label'=>__('TAN'),'value'=>'TAN'],
                ['label'=>__('SILVER'),'value'=>'SILVER'],
                ['label'=>__('BLUE STEEL'),'value'=>'BLUE STEEL'],
                ['label'=>__('LIGHT GOLD'),'value'=>'LIGHT GOLD'],
                ['label'=>__('GUNMETAL'),'value'=>'GUNMETAL'],
                ['label'=>__('BLACK CHROME'),'value'=>'BLACK CHROME'],
                ['label'=>__('COFFEE 218'),'value'=>'COFFEE 218'],
                ['label'=>__('TORTOISE/NATURAL'),'value'=>'TORTOISE/NATURAL'],
                ['label'=>__('GEP'),'value'=>'GEP'],
                ['label'=>__('SILVER ROSE'),'value'=>'SILVER ROSE'],
                ['label'=>__('BURGUNDY'),'value'=>'BURGUNDY'],
                ['label'=>__('CHOCO LATTE'),'value'=>'CHOCO LATTE'],
                ['label'=>__('CHAMPAGNE'),'value'=>'CHAMPAGNE'],
                ['label'=>__('BURGUNDY WINE'),'value'=>'BURGUNDY WINE'],
                ['label'=>__('(033)GUNMETAL'),'value'=>'(033)GUNMETAL'],
                ['label'=>__('(210)MATTE BROWN'),'value'=>'(210)MATTE BROWN'],
                ['label'=>__('(714)GEP'),'value'=>'(714)GEP'],
                ['label'=>__('DARK SILVER 040'),'value'=>'DARK SILVER 040'],
                ['label'=>__('TORTOISE/BRONZE'),'value'=>'TORTOISE/BRONZE'],
                ['label'=>__('COFFEE 249'),'value'=>'COFFEE 249'],
                ['label'=>__('SHINY BROWN'),'value'=>'SHINY BROWN'],
                ['label'=>__('DENIM'),'value'=>'DENIM'],
                ['label'=>__('STEEL'),'value'=>'STEEL'],
                ['label'=>__('LIGHT BRONZE'),'value'=>'LIGHT BRONZE'],
                ['label'=>__('SATIN BURGUNDY'),'value'=>'SATIN BURGUNDY'],
                ['label'=>__('BLUSH'),'value'=>'BLUSH'],
                ['label'=>__('SATIN BROWN'),'value'=>'SATIN BROWN'],
                ['label'=>__('MAT BARK'),'value'=>'MAT BARK'],
                ['label'=>__('BLACK/GREY LENS'),'value'=>'BLACK/GREY LENS'],
                ['label'=>__('BLACK CHROME SMOKE'),'value'=>'BLACK CHROME SMOKE'],
                ['label'=>__('MAT BARK ESPRESSO'),'value'=>'MAT BARK ESPRESSO'],
                ['label'=>__('CHARCOAL/MATTE BLUE'),'value'=>'CHARCOAL/MATTE BLUE'],
                ['label'=>__('WALNUT'),'value'=>'WALNUT'],
                ['label'=>__('SATIN BLACK CHROME'),'value'=>'SATIN BLACK CHROME'],
                ['label'=>__('GUNMETAL/SATIN BLACK'),'value'=>'GUNMETAL/SATIN BLACK'],
                ['label'=>__('WALNUT/BLACK CHROME'),'value'=>'WALNUT/BLACK CHROME'],
                ['label'=>__('NEW BLUE/CHARCOAL'),'value'=>'NEW BLUE/CHARCOAL'],
                ['label'=>__('JET'),'value'=>'JET'],
                ['label'=>__('SATIN GUN'),'value'=>'SATIN GUN'],
                ['label'=>__('LIGHT GOLDEN'),'value'=>'LIGHT GOLDEN'],
                ['label'=>__('COFFEE 246'),'value'=>'COFFEE 246'],
                ['label'=>__('SHINY GUN'),'value'=>'SHINY GUN'],
                ['label'=>__('TORTOISE/GREEN LENS'),'value'=>'TORTOISE/GREEN LENS'],
                ['label'=>__('MATTE BLACK/GREY MAX POLA'),'value'=>'MATTE BLACK/GREY MAX POLA'],
                ['label'=>__('SHINY DARK BROWN'),'value'=>'SHINY DARK BROWN'],
                ['label'=>__('CHARCOAL/DEEP GREEN'),'value'=>'CHARCOAL/DEEP GREEN'],
                ['label'=>__('CHARCOAL/SATIN BLACK'),'value'=>'CHARCOAL/SATIN BLACK'],
                ['label'=>__('MATTE BLACK/ANTHRACITE'),'value'=>'MATTE BLACK/ANTHRACITE'],
                ['label'=>__('MATTE BLACK/RED'),'value'=>'MATTE BLACK/RED'],
                ['label'=>__('STEALTH'),'value'=>'STEALTH'],
                ['label'=>__('MATTE DARK GREY'),'value'=>'MATTE DARK GREY'],
                ['label'=>__('BLACK GOLDEN'),'value'=>'BLACK GOLDEN'],
                ['label'=>__('TOFFEE'),'value'=>'TOFFEE'],
                ['label'=>__('SPARKLING ROSE'),'value'=>'SPARKLING ROSE'],
                ['label'=>__('GLOSS BLACK'),'value'=>'GLOSS BLACK'],
                ['label'=>__('ANTHRACITE'),'value'=>'ANTHRACITE'],
                ['label'=>__('DARK METALLIC GREY'),'value'=>'DARK METALLIC GREY'],
                ['label'=>__('MATTE BLACK'),'value'=>'MATTE BLACK'],
                ['label'=>__('DARK GREY/ORANGE'),'value'=>'DARK GREY/ORANGE'],
                ['label'=>__('MATTE PLATINUM/SLATE BLUE'),'value'=>'MATTE PLATINUM/SLATE BLUE'],
                ['label'=>__('BLACK/WHITE BLACK'),'value'=>'BLACK/WHITE BLACK'],
                ['label'=>__('SATIN BLACK / GREY'),'value'=>'SATIN BLACK / GREY'],
                ['label'=>__('BLACK/COOL GREY'),'value'=>'BLACK/COOL GREY'],
                ['label'=>__('BLACK/VOLT'),'value'=>'BLACK/VOLT'],
                ['label'=>__('GREY/BLUE/CYBER GREEN'),'value'=>'GREY/BLUE/CYBER GREEN'],
                ['label'=>__('GAME ROYAL'),'value'=>'GAME ROYAL'],
                ['label'=>__('MATTE BLCK/GREY/SLVR FLSH'),'value'=>'MATTE BLCK/GREY/SLVR FLSH'],
                ['label'=>__('ANTHRACITE/OUTDOOR LENS'),'value'=>'ANTHRACITE/OUTDOOR LENS'],
                ['label'=>__('MT DP PWTR/TOT ORG/GRY SI'),'value'=>'MT DP PWTR/TOT ORG/GRY SI'],
                ['label'=>__('CRG KHAKI/GRN PULSE/OTDR'),'value'=>'CRG KHAKI/GRN PULSE/OTDR'],
                ['label'=>__('CRYS MD NVY/PHO BL/GRY W/'),'value'=>'CRYS MD NVY/PHO BL/GRY W/'],
                ['label'=>__('MT CRYSL MERCRY GRY/VLT/G'),'value'=>'MT CRYSL MERCRY GRY/VLT/G'],
                ['label'=>__('MATTE BLACK/GREY POLARIZE'),'value'=>'MATTE BLACK/GREY POLARIZE'],
                ['label'=>__('STEALTH/GREY LENS'),'value'=>'STEALTH/GREY LENS'],
                ['label'=>__('MATTE BLK/GRY W/SLVR FLSH'),'value'=>'MATTE BLK/GRY W/SLVR FLSH'],
                ['label'=>__('MERCURY GREY/SILVER/GREY'),'value'=>'MERCURY GREY/SILVER/GREY'],
                ['label'=>__('MATTE ANTHRACITE/ BLACK/'),'value'=>'MATTE ANTHRACITE/ BLACK/'],
                ['label'=>__('MATTE TORTOISE/BROWN'),'value'=>'MATTE TORTOISE/BROWN'],
                ['label'=>__('CRGO KHAK/GRN STRK/OUTDR/'),'value'=>'CRGO KHAK/GRN STRK/OUTDR/'],
                ['label'=>__('DEEP RYAL BLUE/SILVR GREY'),'value'=>'DEEP RYAL BLUE/SILVR GREY'],
                ['label'=>__('MATTE BLACK/GREY POLARIZD'),'value'=>'MATTE BLACK/GREY POLARIZD'],
                ['label'=>__('MAT TORT/CARGOKHAKI/POL B'),'value'=>'MAT TORT/CARGOKHAKI/POL B'],
                ['label'=>__('BLACK/GRY ORNG BLAZE LENS'),'value'=>'BLACK/GRY ORNG BLAZE LENS'],
                ['label'=>__('MT BLCK/WHT W/GRY W/SLVR'),'value'=>'MT BLCK/WHT W/GRY W/SLVR'],
                ['label'=>__('BLACK/VOLT/GRY SIL FL OUT'),'value'=>'BLACK/VOLT/GRY SIL FL OUT'],
                ['label'=>__('WOLF/WHT W/GRY W/SLVR FLS'),'value'=>'WOLF/WHT W/GRY W/SLVR FLS'],
                ['label'=>__('CLEAR/CLEAR'),'value'=>'CLEAR/CLEAR'],
                ['label'=>__('BLACK/CRYSTAL CLEAR'),'value'=>'BLACK/CRYSTAL CLEAR'],
                ['label'=>__('GREY W/SILVER FLASH LENS'),'value'=>'GREY W/SILVER FLASH LENS'],
                ['label'=>__('GUNMETAL/GREY LENS'),'value'=>'GUNMETAL/GREY LENS'],
                ['label'=>__('WALNUT/BROWN LENS'),'value'=>'WALNUT/BROWN LENS'],
                ['label'=>__('GUNMETAL/GREY MAX POLARIZ'),'value'=>'GUNMETAL/GREY MAX POLARIZ'],
                ['label'=>__('BLACK'),'value'=>'BLACK'],
                ['label'=>__('HAVANA'),'value'=>'HAVANA'],
                ['label'=>__('HAVANA AMBER'),'value'=>'HAVANA AMBER'],
                ['label'=>__('BLUE/AZURE'),'value'=>'BLUE/AZURE'],
                ['label'=>__('HAVANA/CARAMEL'),'value'=>'HAVANA/CARAMEL'],
                ['label'=>__('BORDEAUX RED'),'value'=>'BORDEAUX RED'],
                ['label'=>__('ANTIQUE ROSE/ROSE'),'value'=>'ANTIQUE ROSE/ROSE'],
                ['label'=>__('BLACK/GRAD GREY LENS'),'value'=>'BLACK/GRAD GREY LENS'],
                ['label'=>__('HAVANA/GRAD KHAKI LENS'),'value'=>'HAVANA/GRAD KHAKI LENS'],
                ['label'=>__('SATIN BLACK'),'value'=>'SATIN BLACK'],
                ['label'=>__('BLACK/VOLT/GRY SIL FL LEN'),'value'=>'BLACK/VOLT/GRY SIL FL LEN'],
                ['label'=>__('FOREST'),'value'=>'FOREST'],
                ['label'=>__('DARK TORTOISE'),'value'=>'DARK TORTOISE'],
                ['label'=>__('OCEAN'),'value'=>'OCEAN'],
                ['label'=>__('BLUE HAVANA'),'value'=>'BLUE HAVANA'],
                ['label'=>__('YELLOW HAVANA'),'value'=>'YELLOW HAVANA'],
                ['label'=>__('BLUE'),'value'=>'BLUE'],
                ['label'=>__('MATTE PETROLEUM'),'value'=>'MATTE PETROLEUM'],
                ['label'=>__('BRUSHED DARK GUNMETAL'),'value'=>'BRUSHED DARK GUNMETAL'],
                ['label'=>__('SHINY WALNUT'),'value'=>'SHINY WALNUT'],
                ['label'=>__('TOKYO TORTOISE'),'value'=>'TOKYO TORTOISE'],
                ['label'=>__('SOFT TORTOISE'),'value'=>'SOFT TORTOISE'],
                ['label'=>__('NUDE'),'value'=>'NUDE'],
                ['label'=>__('BLACK/GREEN/CRYSTAL'),'value'=>'BLACK/GREEN/CRYSTAL'],
                ['label'=>__('NAVY/HYPER JADE'),'value'=>'NAVY/HYPER JADE'],
                ['label'=>__('CONCORD/FUCHSIA'),'value'=>'CONCORD/FUCHSIA'],
                ['label'=>__('BLACK/BROWN'),'value'=>'BLACK/BROWN'],
                ['label'=>__('BLACK/BLUE'),'value'=>'BLACK/BLUE'],
                ['label'=>__('BLACK/GREEN'),'value'=>'BLACK/GREEN'],
                ['label'=>__('BLACK/BLUE/BLACK'),'value'=>'BLACK/BLUE/BLACK'],
                ['label'=>__('DARK GREEN/GREEN'),'value'=>'DARK GREEN/GREEN'],
                ['label'=>__('BLUE/YELLOW/BLUE'),'value'=>'BLUE/YELLOW/BLUE'],
                ['label'=>__('BLUE/TURQUOISE'),'value'=>'BLUE/TURQUOISE'],
                ['label'=>__('BURGUNDY/WHITE/PURPLE'),'value'=>'BURGUNDY/WHITE/PURPLE'],
                ['label'=>__('MATTE BLACK/PHOTO BLUE'),'value'=>'MATTE BLACK/PHOTO BLUE'],
                ['label'=>__('MATTE BLACK/SONIC YELLOW'),'value'=>'MATTE BLACK/SONIC YELLOW'],
                ['label'=>__('OBSIDIAN'),'value'=>'OBSIDIAN'],
                ['label'=>__('MATTE BLACK/POISON GREEN'),'value'=>'MATTE BLACK/POISON GREEN'],
                ['label'=>__('MATTE BLACK/CHALLENGE RED'),'value'=>'MATTE BLACK/CHALLENGE RED'],
                ['label'=>__('NAUTICAL NAVY'),'value'=>'NAUTICAL NAVY'],
                ['label'=>__('DARK GUN'),'value'=>'DARK GUN'],
                ['label'=>__('DARK BROWN'),'value'=>'DARK BROWN'],
                ['label'=>__('DARK GUNMETAL'),'value'=>'DARK GUNMETAL'],
                ['label'=>__('MATTE HAVANA GEP'),'value'=>'MATTE HAVANA GEP'],
                ['label'=>__('PURPLE TORTOISE'),'value'=>'PURPLE TORTOISE'],
                ['label'=>__('SHINY BLACK'),'value'=>'SHINY BLACK'],
                ['label'=>__('STRIPED GREY/PETROL'),'value'=>'STRIPED GREY/PETROL'],
                ['label'=>__('STRIPED BROWN/GREY'),'value'=>'STRIPED BROWN/GREY'],
                ['label'=>__('CHARCOAL'),'value'=>'CHARCOAL'],
                ['label'=>__('CHOCOLATE'),'value'=>'CHOCOLATE'],
                ['label'=>__('MIDNIGHT'),'value'=>'MIDNIGHT'],
                ['label'=>__('EGGPLANT'),'value'=>'EGGPLANT'],
                ['label'=>__('BLACK SILVER'),'value'=>'BLACK SILVER'],
                ['label'=>__('BROWN SIERRA'),'value'=>'BROWN SIERRA'],
                ['label'=>__('BORDEAUX'),'value'=>'BORDEAUX'],
                ['label'=>__('BRUSHED GUNMETAL'),'value'=>'BRUSHED GUNMETAL'],
                ['label'=>__('BRUSHED NAVY'),'value'=>'BRUSHED NAVY'],
                ['label'=>__('LIGHT BROWN'),'value'=>'LIGHT BROWN'],
                ['label'=>__('IVORY'),'value'=>'IVORY'],
                ['label'=>__('STRIPED TOBACCO'),'value'=>'STRIPED TOBACCO'],
                ['label'=>__('TORTOISE'),'value'=>'TORTOISE'],
                ['label'=>__('BORDEAUX/RED'),'value'=>'BORDEAUX/RED'],
                ['label'=>__('PEACH/NUDE'),'value'=>'PEACH/NUDE'],
                ['label'=>__('STRIPED BROWN GREY'),'value'=>'STRIPED BROWN GREY'],
                ['label'=>__('STRIPED BLUE GREEN'),'value'=>'STRIPED BLUE GREEN'],
                ['label'=>__('SATIN BLUE'),'value'=>'SATIN BLUE'],
                ['label'=>__('LIGHT TORTOISE'),'value'=>'LIGHT TORTOISE'],
                ['label'=>__('BLUE NAVY'),'value'=>'BLUE NAVY'],
                ['label'=>__('CYCLAMINE'),'value'=>'CYCLAMINE'],
                ['label'=>__('ANTIQUE ROSE'),'value'=>'ANTIQUE ROSE'],
                ['label'=>__('CRYSTAL KHAKI GREEN'),'value'=>'CRYSTAL KHAKI GREEN'],
                ['label'=>__('BROWNFADE'),'value'=>'BROWNFADE'],
                ['label'=>__('DEMIGREY'),'value'=>'DEMIGREY'],
                ['label'=>__('GREYFADE'),'value'=>'GREYFADE'],
                ['label'=>__('OLIVE TORTOISE'),'value'=>'OLIVE TORTOISE'],
                ['label'=>__('TEAL TORTOISE'),'value'=>'TEAL TORTOISE'],
                ['label'=>__('BLUEROSE'),'value'=>'BLUEROSE'],
                ['label'=>__('BROWNROSE'),'value'=>'BROWNROSE'],
                ['label'=>__('CRYSTALPINK'),'value'=>'CRYSTALPINK'],
                ['label'=>__('GREYROSE'),'value'=>'GREYROSE'],
                ['label'=>__('TRANSPARENT GREY'),'value'=>'TRANSPARENT GREY'],
                ['label'=>__('BLUE/YELLOW'),'value'=>'BLUE/YELLOW'],
                ['label'=>__('TRANSPARENT BLUE'),'value'=>'TRANSPARENT BLUE'],
                ['label'=>__('BROWN MOSAIC'),'value'=>'BROWN MOSAIC'],
                ['label'=>__('BLUE MIST'),'value'=>'BLUE MIST'],
                ['label'=>__('LILAC MOSAIC'),'value'=>'LILAC MOSAIC'],
                ['label'=>__('BROWN'),'value'=>'BROWN'],
                ['label'=>__('AZURE HAVANA'),'value'=>'AZURE HAVANA'],
                ['label'=>__('ROSE HAVANA'),'value'=>'ROSE HAVANA'],
                ['label'=>__('PURPLE'),'value'=>'PURPLE'],
                ['label'=>__('BLACK/CRYSTAL'),'value'=>'BLACK/CRYSTAL'],
                ['label'=>__('MISTY BLUE'),'value'=>'MISTY BLUE'],
                ['label'=>__('GREY BLUE H0RN'),'value'=>'GREY BLUE H0RN'],
                ['label'=>__('BROWN PINK HORN'),'value'=>'BROWN PINK HORN'],
                ['label'=>__('BLACK/GREY'),'value'=>'BLACK/GREY'],
                ['label'=>__('GREY'),'value'=>'GREY'],
                ['label'=>__('BROWN/BLUE'),'value'=>'BROWN/BLUE'],
                ['label'=>__('DARK BLUE'),'value'=>'DARK BLUE'],
                ['label'=>__('DARK BLUE/BLUE'),'value'=>'DARK BLUE/BLUE'],
                ['label'=>__('MT ANTRHA/BLK/GREY SILVER'),'value'=>'MT ANTRHA/BLK/GREY SILVER'],
                ['label'=>__('BLACK/VOLT/GREY LENS'),'value'=>'BLACK/VOLT/GREY LENS'],
                ['label'=>__('MATTE TORTOISE/BROWN LENS'),'value'=>'MATTE TORTOISE/BROWN LENS'],
                ['label'=>__('MT MIDNGHT NVY/OCEAN FOG/'),'value'=>'MT MIDNGHT NVY/OCEAN FOG/'],
                ['label'=>__('MT CR DK MAG GRY/BLK/GRY'),'value'=>'MT CR DK MAG GRY/BLK/GRY'],
                ['label'=>__('MURCY GRY/BLK/GRY SILVER'),'value'=>'MURCY GRY/BLK/GRY SILVER'],
                ['label'=>__('BLACK/MATTE BLACK/GREY LE'),'value'=>'BLACK/MATTE BLACK/GREY LE'],
                ['label'=>__('MT SQDRN BLU/BLK/GRY SILV'),'value'=>'MT SQDRN BLU/BLK/GRY SILV'],
                ['label'=>__('CRTL MRCY GRY/MTL SVR/DK'),'value'=>'CRTL MRCY GRY/MTL SVR/DK'],
                ['label'=>__('BLACK/DARK GREY LENS'),'value'=>'BLACK/DARK GREY LENS'],
                ['label'=>__('MATTE CRSTL MLTRY BRWN/GR'),'value'=>'MATTE CRSTL MLTRY BRWN/GR'],
                ['label'=>__('SATIN BLACK/BLACK'),'value'=>'SATIN BLACK/BLACK'],
                ['label'=>__('DARK GREY/DARK ARMY GREEN'),'value'=>'DARK GREY/DARK ARMY GREEN'],
                ['label'=>__('WALNUT/DARK BROWN'),'value'=>'WALNUT/DARK BROWN'],
                ['label'=>__('DARK GREY/BLACK'),'value'=>'DARK GREY/BLACK'],
                ['label'=>__('WALNUT/DARK ARMY GREEN'),'value'=>'WALNUT/DARK ARMY GREEN'],
                ['label'=>__('JET GREY'),'value'=>'JET GREY'],
                ['label'=>__('MURDERED/SMOKE'),'value'=>'MURDERED/SMOKE'],
                ['label'=>__('JET/RED ION'),'value'=>'JET/RED ION'],
                ['label'=>__('PURPLE NEBULA/PURPLE ION'),'value'=>'PURPLE NEBULA/PURPLE ION'],
                ['label'=>__('BLACK LEOPARD/BRZ GRAD'),'value'=>'BLACK LEOPARD/BRZ GRAD'],
                ['label'=>__('SHINY TORTOISE'),'value'=>'SHINY TORTOISE'],
                ['label'=>__('MATTE TORTOISE BRONZE'),'value'=>'MATTE TORTOISE BRONZE'],
                ['label'=>__('POLISHED WALNUT BROWN'),'value'=>'POLISHED WALNUT BROWN'],
                ['label'=>__('MATTE CEMENT/PEARL ION'),'value'=>'MATTE CEMENT/PEARL ION'],
                ['label'=>__('JET TEAL GREEN ION'),'value'=>'JET TEAL GREEN ION'],
                ['label'=>__('BLUE SMOKE GREY'),'value'=>'BLUE SMOKE GREY'],
                ['label'=>__('MATTE BLACK H20/GREEN ION'),'value'=>'MATTE BLACK H20/GREEN ION'],
                ['label'=>__('MATTE H2O GREY POLAR'),'value'=>'MATTE H2O GREY POLAR'],
                ['label'=>__('MATTE H2O/PLASMA P2'),'value'=>'MATTE H2O/PLASMA P2'],
                ['label'=>__('MAT TORTS/BROWN w/BRNZ FL'),'value'=>'MAT TORTS/BROWN w/BRNZ FL'],
                ['label'=>__('MILITARY BRWN/WALNT/POLAR'),'value'=>'MILITARY BRWN/WALNT/POLAR'],
                ['label'=>__('MT DP PWTR/TOT ORG/SHTR/G'),'value'=>'MT DP PWTR/TOT ORG/SHTR/G'],
                ['label'=>__('MT BLK/MLTR BL/GRY w/BLU'),'value'=>'MT BLK/MLTR BL/GRY w/BLU'],
                ['label'=>__('MAT BLK/ELE PUR/GRY/ML VI'),'value'=>'MAT BLK/ELE PUR/GRY/ML VI'],
                ['label'=>__('BLUE/WHITE/BLUE'),'value'=>'BLUE/WHITE/BLUE'],
                ['label'=>__('SHINY GUNMETAL'),'value'=>'SHINY GUNMETAL'],
                ['label'=>__('CRST MT DK GRY/UNVR RD w/'),'value'=>'CRST MT DK GRY/UNVR RD w/'],
                ['label'=>__('BLACK/VOLT w/GREY LENS'),'value'=>'BLACK/VOLT w/GREY LENS'],
                ['label'=>__('CRY GYM BL/VT GRY LEN W/S'),'value'=>'CRY GYM BL/VT GRY LEN W/S'],
                ['label'=>__('CRY/H CRM/G RD GY LEN W/S'),'value'=>'CRY/H CRM/G RD GY LEN W/S'],
                ['label'=>__('MATTE BLACK/VOLT w/GREY L'),'value'=>'MATTE BLACK/VOLT w/GREY L'],
                ['label'=>__('GUNMETAL/GYM RED'),'value'=>'GUNMETAL/GYM RED'],
                ['label'=>__('BLK/CRYSTL CLR W/GREY POL'),'value'=>'BLK/CRYSTL CLR W/GREY POL'],
                ['label'=>__('LIGHT SATIN BROWN'),'value'=>'LIGHT SATIN BROWN'],
                ['label'=>__('SHINY ROSE'),'value'=>'SHINY ROSE'],
                ['label'=>__('ANTHRA-VT W-GRY W-SIL FL'),'value'=>'ANTHRA-VT W-GRY W-SIL FL'],
                ['label'=>__('WHITE/CRY HYCRIM GREY W/S'),'value'=>'WHITE/CRY HYCRIM GREY W/S'],
                ['label'=>__('SQUAD BL-HT LAV W-SIL FL'),'value'=>'SQUAD BL-HT LAV W-SIL FL'],
                ['label'=>__('GOLD'),'value'=>'GOLD'],
                ['label'=>__('SATIN WALNUT/DARK BROWN'),'value'=>'SATIN WALNUT/DARK BROWN'],
                ['label'=>__('ST BLU/OBSID'),'value'=>'ST BLU/OBSID'],
                ['label'=>__('MATTE GREY HORN'),'value'=>'MATTE GREY HORN'],
                ['label'=>__('MATTE BROWN'),'value'=>'MATTE BROWN'],
                ['label'=>__('MATTE BLUE HORN'),'value'=>'MATTE BLUE HORN'],
                ['label'=>__('BLACK W/GREY LENS'),'value'=>'BLACK W/GREY LENS'],
                ['label'=>__('MATTE BLACK W/GREY SILVER'),'value'=>'MATTE BLACK W/GREY SILVER'],
                ['label'=>__('MERCY GRY/SIL W/GY SIL FL'),'value'=>'MERCY GRY/SIL W/GY SIL FL'],
                ['label'=>__('MAT BLK/BLU GRAD W/BL SK'),'value'=>'MAT BLK/BLU GRAD W/BL SK'],
                ['label'=>__('MAT BLK/GRN GRAD W/GY GRN'),'value'=>'MAT BLK/GRN GRAD W/GY GRN'],
                ['label'=>__('MAT BLK/VLT w/GRY ORG MIR'),'value'=>'MAT BLK/VLT w/GRY ORG MIR'],
                ['label'=>__('MATTE D P/GRN P W/GRY ML'),'value'=>'MATTE D P/GRN P W/GRY ML'],
                ['label'=>__('MATTE GREY'),'value'=>'MATTE GREY'],
                ['label'=>__('MATTE BROWN HORN'),'value'=>'MATTE BROWN HORN'],
                ['label'=>__('BLACK PURPLE'),'value'=>'BLACK PURPLE'],
                ['label'=>__('HAVANA TEAL FADE'),'value'=>'HAVANA TEAL FADE'],
                ['label'=>__('BROWN HORN LAVENDER'),'value'=>'BROWN HORN LAVENDER'],
                ['label'=>__('RUBY'),'value'=>'RUBY'],
                ['label'=>__('BLACK/HAVANA'),'value'=>'BLACK/HAVANA'],
                ['label'=>__('STRIPED BROWN/BLUE'),'value'=>'STRIPED BROWN/BLUE'],
                ['label'=>__('HAVANA/AZURE/PETROL'),'value'=>'HAVANA/AZURE/PETROL'],
                ['label'=>__('GREEN/WINE'),'value'=>'GREEN/WINE'],
                ['label'=>__('BLUE/PURPLE'),'value'=>'BLUE/PURPLE'],
                ['label'=>__('BLUE STRIPED'),'value'=>'BLUE STRIPED'],
                ['label'=>__('BLACK/LIME'),'value'=>'BLACK/LIME'],
                ['label'=>__('GREEN'),'value'=>'GREEN'],
                ['label'=>__('BLUE STEEL/ORANGE'),'value'=>'BLUE STEEL/ORANGE'],
                ['label'=>__('BLUE/GREEN'),'value'=>'BLUE/GREEN'],
                ['label'=>__('BLACK MATTE'),'value'=>'BLACK MATTE'],
                ['label'=>__('MATTE ONYX'),'value'=>'MATTE ONYX'],
                ['label'=>__('GREY MATTE'),'value'=>'GREY MATTE'],
                ['label'=>__('DARK HAVANA'),'value'=>'DARK HAVANA'],
                ['label'=>__('BLUE MATTE'),'value'=>'BLUE MATTE'],
                ['label'=>__('NAVY'),'value'=>'NAVY'],
                ['label'=>__('PLUM'),'value'=>'PLUM'],
                ['label'=>__('NATURAL'),'value'=>'NATURAL'],
                ['label'=>__('COFFEE'),'value'=>'COFFEE'],
                ['label'=>__('MIDNIGHT BLUE'),'value'=>'MIDNIGHT BLUE'],
                ['label'=>__('RED'),'value'=>'RED'],
                ['label'=>__('TURTLEDOVE'),'value'=>'TURTLEDOVE'],
                ['label'=>__('SATIN BLACK/DARK GREY'),'value'=>'SATIN BLACK/DARK GREY'],
                ['label'=>__('LIGHT GUNMETAL'),'value'=>'LIGHT GUNMETAL'],
                ['label'=>__('STEEL BLUE'),'value'=>'STEEL BLUE'],
                ['label'=>__('MATTE BLACK/GREEN ION'),'value'=>'MATTE BLACK/GREEN ION'],
                ['label'=>__('MATTE BLACK/RED ION'),'value'=>'MATTE BLACK/RED ION'],
                ['label'=>__('MATTE WOODGRAIN'),'value'=>'MATTE WOODGRAIN'],
                ['label'=>__('MATTE PURPLE/PURPLE ION'),'value'=>'MATTE PURPLE/PURPLE ION'],
                ['label'=>__('MATTE BLACK/GREY'),'value'=>'MATTE BLACK/GREY'],
                ['label'=>__('MATTE TORT/GREEN G15'),'value'=>'MATTE TORT/GREEN G15'],
                ['label'=>__('MATTE BLACK/ROSE GOLD'),'value'=>'MATTE BLACK/ROSE GOLD'],
                ['label'=>__('MATTE BLACK SILVER ION'),'value'=>'MATTE BLACK SILVER ION'],
                ['label'=>__('GREY MATTER/SKY BLUE ION'),'value'=>'GREY MATTER/SKY BLUE ION'],
                ['label'=>__('NEO GEO/GREY'),'value'=>'NEO GEO/GREY'],
                ['label'=>__('MATTE CLEAR BLUE ION'),'value'=>'MATTE CLEAR BLUE ION'],
                ['label'=>__('HAWAII GREY'),'value'=>'HAWAII GREY'],
                ['label'=>__('MATTE COPPER MARBLE SILVE'),'value'=>'MATTE COPPER MARBLE SILVE'],
                ['label'=>__('EBONY'),'value'=>'EBONY'],
                ['label'=>__('TURTLE/CREAM'),'value'=>'TURTLE/CREAM'],
                ['label'=>__('COBALT/AZURE'),'value'=>'COBALT/AZURE'],
                ['label'=>__('BURGUNDY ROSE'),'value'=>'BURGUNDY ROSE'],
                ['label'=>__('BROWN CHESTNUT'),'value'=>'BROWN CHESTNUT'],
                ['label'=>__('MAT BLACK'),'value'=>'MAT BLACK'],
                ['label'=>__('BURNT OCRE'),'value'=>'BURNT OCRE'],
                ['label'=>__('BLUE AZURE'),'value'=>'BLUE AZURE'],
                ['label'=>__('BLACK/SILVER'),'value'=>'BLACK/SILVER'],
                ['label'=>__('BRUSHED GUNMETAL/BLACK'),'value'=>'BRUSHED GUNMETAL/BLACK'],
                ['label'=>__('BLACK/WHITE/GREY LENS'),'value'=>'BLACK/WHITE/GREY LENS'],
                ['label'=>__('MT DK MAG GRY/WH/MX OUTDO'),'value'=>'MT DK MAG GRY/WH/MX OUTDO'],
                ['label'=>__('MT BLACK/WH W/GREY SIL FL'),'value'=>'MT BLACK/WH W/GREY SIL FL'],
                ['label'=>__('MT BLK/VOLT/GRY W/ SIL FL'),'value'=>'MT BLK/VOLT/GRY W/ SIL FL'],
                ['label'=>__('MATTE GREY/OUTDOOR'),'value'=>'MATTE GREY/OUTDOOR'],
                ['label'=>__('GAME ROAYL/WH W/GRY SIL F'),'value'=>'GAME ROAYL/WH W/GRY SIL F'],
                ['label'=>__('UNIVERS RD/WH W/GRY SIL L'),'value'=>'UNIVERS RD/WH W/GRY SIL L'],
                ['label'=>__('BLK/VOLT GREY LENS W/SIL'),'value'=>'BLK/VOLT GREY LENS W/SIL'],
                ['label'=>__('MT ANTHRA-CB W-GREY SI FL'),'value'=>'MT ANTHRA-CB W-GREY SI FL'],
                ['label'=>__('GAME RY/VOLT GRY LENS W/S'),'value'=>'GAME RY/VOLT GRY LENS W/S'],
                ['label'=>__('BLK/GAM RYL TORT/GRY SILV'),'value'=>'BLK/GAM RYL TORT/GRY SILV'],
                ['label'=>__('TORTOISE/BROWN'),'value'=>'TORTOISE/BROWN'],
                ['label'=>__('BLACK/BOMBER GREY'),'value'=>'BLACK/BOMBER GREY'],
                ['label'=>__('BLACK/PHOTO BLUE'),'value'=>'BLACK/PHOTO BLUE'],
                ['label'=>__('OBSIDIAN/PURE PLATINUM'),'value'=>'OBSIDIAN/PURE PLATINUM'],
                ['label'=>__('MT BLACK/VT W-GY W/SIL FL'),'value'=>'MT BLACK/VT W-GY W/SIL FL'],
                ['label'=>__('INS B/HT LV W/GRY W/SL FL'),'value'=>'INS B/HT LV W/GRY W/SL FL'],
                ['label'=>__('CLEAR/H JAD GRY LEN W/SIL'),'value'=>'CLEAR/H JAD GRY LEN W/SIL'],
                ['label'=>__('CLEAR/GRAPE GRY LEN W/SIL'),'value'=>'CLEAR/GRAPE GRY LEN W/SIL'],
                ['label'=>__('CLEAR/PUNCH GRY LEN W/SIL'),'value'=>'CLEAR/PUNCH GRY LEN W/SIL'],
                ['label'=>__('BLACK GUN'),'value'=>'BLACK GUN'],
                ['label'=>__('PALLADIUM'),'value'=>'PALLADIUM'],
                ['label'=>__('NAVY GUN'),'value'=>'NAVY GUN'],
                ['label'=>__('DARK MAGNET GREY/BLACK'),'value'=>'DARK MAGNET GREY/BLACK'],
                ['label'=>__('BLACK/VIVID PINK'),'value'=>'BLACK/VIVID PINK'],
                ['label'=>__('SATIN GUNMETAL/NIGHT FACT'),'value'=>'SATIN GUNMETAL/NIGHT FACT'],
                ['label'=>__('SHINY WALNUT/CARGO KHAKI'),'value'=>'SHINY WALNUT/CARGO KHAKI'],
                ['label'=>__('SHINY BLACK/BLACK'),'value'=>'SHINY BLACK/BLACK'],
                ['label'=>__('SATIN BLUE/MILITARY BLUE'),'value'=>'SATIN BLUE/MILITARY BLUE'],
                ['label'=>__('GRAY HORN'),'value'=>'GRAY HORN'],
                ['label'=>__('BROWN HORN'),'value'=>'BROWN HORN'],
                ['label'=>__('CAMEL BLUSH'),'value'=>'CAMEL BLUSH'],
                ['label'=>__('DARK GREY'),'value'=>'DARK GREY'],
                ['label'=>__('LIGHT HAVANA'),'value'=>'LIGHT HAVANA'],
                ['label'=>__('MEDIUM BLUE'),'value'=>'MEDIUM BLUE'],
                ['label'=>__('VIOLET'),'value'=>'VIOLET'],
                ['label'=>__('CYCLAMEN'),'value'=>'CYCLAMEN'],
                ['label'=>__('ROSE'),'value'=>'ROSE'],
                ['label'=>__('GOLD/TRANSPARENT BROWN'),'value'=>'GOLD/TRANSPARENT BROWN'],
                ['label'=>__('GOLD/TRANSP BROWN/GRAD FL'),'value'=>'GOLD/TRANSP BROWN/GRAD FL'],
                ['label'=>__('GOLD/TRANSP GREY/GRAD BLU'),'value'=>'GOLD/TRANSP GREY/GRAD BLU'],
                ['label'=>__('GOLD/TRANP BROWN/GRAD BRO'),'value'=>'GOLD/TRANP BROWN/GRAD BRO'],
                ['label'=>__('MATTE H20/BLUE ION P2'),'value'=>'MATTE H20/BLUE ION P2'],
                ['label'=>__('MATTE H2O PLASMA P2'),'value'=>'MATTE H2O PLASMA P2'],
                ['label'=>__('MATTE H20 GREEN ION P2'),'value'=>'MATTE H20 GREEN ION P2'],
                ['label'=>__('MATTE H20 BLUE P2'),'value'=>'MATTE H20 BLUE P2'],
                ['label'=>__('MATTE REDWOOD/SMOKE'),'value'=>'MATTE REDWOOD/SMOKE'],
                ['label'=>__('SMOKEY ROSE'),'value'=>'SMOKEY ROSE'],
                ['label'=>__('SHINY BORDEAUX'),'value'=>'SHINY BORDEAUX'],
                ['label'=>__('SHINY GOLD'),'value'=>'SHINY GOLD'],
                ['label'=>__('GOLDEN SAND'),'value'=>'GOLDEN SAND'],
                ['label'=>__('GOLD WHITE'),'value'=>'GOLD WHITE'],
                ['label'=>__('ESPRESSO'),'value'=>'ESPRESSO'],
                ['label'=>__('SATIN GUNMETAL-BLACK'),'value'=>'SATIN GUNMETAL-BLACK'],
                ['label'=>__('SATIN BLACK-DARK GREY'),'value'=>'SATIN BLACK-DARK GREY'],
                ['label'=>__('SEMI MATTE BLACK'),'value'=>'SEMI MATTE BLACK'],
                ['label'=>__('SATIN GUNMETAL'),'value'=>'SATIN GUNMETAL'],
                ['label'=>__('SATIN NAVY'),'value'=>'SATIN NAVY'],
                ['label'=>__('BLACK/WHITE'),'value'=>'BLACK/WHITE'],
                ['label'=>__('HAVANA/PINK/GREEN'),'value'=>'HAVANA/PINK/GREEN'],
                ['label'=>__('GREEN HAVANA/AZURE'),'value'=>'GREEN HAVANA/AZURE'],
                ['label'=>__('PETROL/VIOLET/BLUE'),'value'=>'PETROL/VIOLET/BLUE'],
                ['label'=>__('STRIPED BROWN CREAM'),'value'=>'STRIPED BROWN CREAM'],
                ['label'=>__('STRIPED NUDE'),'value'=>'STRIPED NUDE'],
                ['label'=>__('STRIPED PETROL'),'value'=>'STRIPED PETROL'],
                ['label'=>__('KHAKI'),'value'=>'KHAKI'],
                ['label'=>__('DEEP HAVANA'),'value'=>'DEEP HAVANA'],
                ['label'=>__('BLACK-CHALLENGE RED'),'value'=>'BLACK-CHALLENGE RED'],
                ['label'=>__('SATIN BLUE-MIDNIGHT NAVY'),'value'=>'SATIN BLUE-MIDNIGHT NAVY'],
                ['label'=>__('ROSE GOLD/LIGHT BROWN'),'value'=>'ROSE GOLD/LIGHT BROWN'],
                ['label'=>__('GOLD/BLUE'),'value'=>'GOLD/BLUE'],
                ['label'=>__('GOLD/PEACH'),'value'=>'GOLD/PEACH'],
                ['label'=>__('GOLD/GREEN'),'value'=>'GOLD/GREEN'],
                ['label'=>__('GOLD/LIGHT GREY'),'value'=>'GOLD/LIGHT GREY'],
                ['label'=>__('ROSE GOLD/BROWN'),'value'=>'ROSE GOLD/BROWN'],
                ['label'=>__('MATTE GUNMETAL'),'value'=>'MATTE GUNMETAL'],
                ['label'=>__('ANTIQUE GUNMETAL'),'value'=>'ANTIQUE GUNMETAL'],
                ['label'=>__('MATTE NAVY'),'value'=>'MATTE NAVY'],
                ['label'=>__('HONEY'),'value'=>'HONEY'],
                ['label'=>__('HAVANA GREY'),'value'=>'HAVANA GREY'],
                ['label'=>__('WINE'),'value'=>'WINE'],
                ['label'=>__('TRANSPARENT TURTLEDOVE'),'value'=>'TRANSPARENT TURTLEDOVE'],
                ['label'=>__('GOLDEN AMBER'),'value'=>'GOLDEN AMBER'],
                ['label'=>__('PALE GOLD'),'value'=>'PALE GOLD'],
                ['label'=>__('GOLDEN BEAUTY'),'value'=>'GOLDEN BEAUTY'],
                ['label'=>__('MATTE DARK TORTOISE'),'value'=>'MATTE DARK TORTOISE'],
                ['label'=>__('MATTE OLIVE'),'value'=>'MATTE OLIVE'],
                ['label'=>__('MT BLK/POI GRN/GRY W/ ML'),'value'=>'MT BLK/POI GRN/GRY W/ ML'],
                ['label'=>__('MT BLK/GAME RYL/GRY W/ BL'),'value'=>'MT BLK/GAME RYL/GRY W/ BL'],
                ['label'=>__('MT BLK/CHAL RD/GREY W/ ML'),'value'=>'MT BLK/CHAL RD/GREY W/ ML'],
                ['label'=>__('MT BLACK/WHITE/GREY POLAR'),'value'=>'MT BLACK/WHITE/GREY POLAR'],
                ['label'=>__('WH/DK CON/GREY W/ ML VIOL'),'value'=>'WH/DK CON/GREY W/ ML VIOL'],
                ['label'=>__('MT DP PWTR/TOTAL ORG/GRY'),'value'=>'MT DP PWTR/TOTAL ORG/GRY'],
                ['label'=>__('MT BLK/WH/GREY POLAR LENS'),'value'=>'MT BLK/WH/GREY POLAR LENS'],
                ['label'=>__('BROWN W/ANIMALIER'),'value'=>'BROWN W/ANIMALIER'],
                ['label'=>__('STRIPED GREEN'),'value'=>'STRIPED GREEN'],
                ['label'=>__('BURGUNDY W/ANIMALIER'),'value'=>'BURGUNDY W/ANIMALIER'],
                ['label'=>__('EMERALD'),'value'=>'EMERALD'],
                ['label'=>__('CORAL'),'value'=>'CORAL'],
                ['label'=>__('GREY HORN'),'value'=>'GREY HORN'],
                ['label'=>__('ROSE GOLD'),'value'=>'ROSE GOLD'],
                ['label'=>__('MATTE BLACK/TEAL'),'value'=>'MATTE BLACK/TEAL'],
                ['label'=>__('MATTE TORTOISE'),'value'=>'MATTE TORTOISE'],
                ['label'=>__('MATTE BLACK BLUE'),'value'=>'MATTE BLACK BLUE'],
                ['label'=>__('CRYSTAL NAVY'),'value'=>'CRYSTAL NAVY'],
                ['label'=>__('MATTE CRYSTAL'),'value'=>'MATTE CRYSTAL'],
                ['label'=>__('MATTE DARK GREY-BLACK'),'value'=>'MATTE DARK GREY-BLACK'],
                ['label'=>__('MT CRY CARGO KHAKI-IRON G'),'value'=>'MT CRY CARGO KHAKI-IRON G'],
                ['label'=>__('MT CRYSTAL CARGO KHAKI-CY'),'value'=>'MT CRYSTAL CARGO KHAKI-CY'],
                ['label'=>__('BLACK/CAMOUFLAGE'),'value'=>'BLACK/CAMOUFLAGE'],
                ['label'=>__('SATIN BLACK-CHALLENGE RED'),'value'=>'SATIN BLACK-CHALLENGE RED'],
                ['label'=>__('WALNUT-SPACE BLUE'),'value'=>'WALNUT-SPACE BLUE'],
                ['label'=>__('SATIN BLACK-VOLT'),'value'=>'SATIN BLACK-VOLT'],
                ['label'=>__('BRUSHED GUNMETAL-HYPER JA'),'value'=>'BRUSHED GUNMETAL-HYPER JA'],
                ['label'=>__('BLUE-BLACK'),'value'=>'BLUE-BLACK'],
                ['label'=>__('OLIVE MARBLE'),'value'=>'OLIVE MARBLE'],
                ['label'=>__('BLUE MARBLE'),'value'=>'BLUE MARBLE'],
                ['label'=>__('BLACK GUNMETAL'),'value'=>'BLACK GUNMETAL'],
                ['label'=>__('BROWN GUNMETAL'),'value'=>'BROWN GUNMETAL'],
                ['label'=>__('NAVY GUNMETAL'),'value'=>'NAVY GUNMETAL'],
                ['label'=>__('BLACK/CRYSTAL VOLT'),'value'=>'BLACK/CRYSTAL VOLT'],
                ['label'=>__('WALNUT/CARGO KHAKI'),'value'=>'WALNUT/CARGO KHAKI'],
                ['label'=>__('SATIN BLUE/BLACK/PHOTO BL'),'value'=>'SATIN BLUE/BLACK/PHOTO BL'],
                ['label'=>__('MATTE OLIVE TORTOISE'),'value'=>'MATTE OLIVE TORTOISE'],
                ['label'=>__('CORDOVAN'),'value'=>'CORDOVAN'],
                ['label'=>__('SATIN BLACK/VOLT'),'value'=>'SATIN BLACK/VOLT'],
                ['label'=>__('CRYSTAL DARK GREY/PHOTO B'),'value'=>'CRYSTAL DARK GREY/PHOTO B'],
                ['label'=>__('CRYSTAL GYM BLUE/HYPER CR'),'value'=>'CRYSTAL GYM BLUE/HYPER CR'],
                ['label'=>__('ST CRYSTAL SP BLUE/HYPER'),'value'=>'ST CRYSTAL SP BLUE/HYPER'],
                ['label'=>__('CRYSTAL GYM RED/CHALLENGE'),'value'=>'CRYSTAL GYM RED/CHALLENGE'],
                ['label'=>__('SATIN BLACK/MAGNET GREY'),'value'=>'SATIN BLACK/MAGNET GREY'],
                ['label'=>__('SATIN GUNMETAL/GREY/HYPER'),'value'=>'SATIN GUNMETAL/GREY/HYPER'],
                ['label'=>__('SATIN BLACK/CRYSTAL VOLT'),'value'=>'SATIN BLACK/CRYSTAL VOLT'],
                ['label'=>__('GUNMETAL/GREY/VOLT'),'value'=>'GUNMETAL/GREY/VOLT'],
                ['label'=>__('GREEN MATTE'),'value'=>'GREEN MATTE'],
                ['label'=>__('PETROL'),'value'=>'PETROL'],
                ['label'=>__('FOG'),'value'=>'FOG'],
                ['label'=>__('SHINY CHOCOLATE BROWN'),'value'=>'SHINY CHOCOLATE BROWN'],
                ['label'=>__('SHINY NIGHT BLUE'),'value'=>'SHINY NIGHT BLUE'],
                ['label'=>__('SHINY DEEP WINE'),'value'=>'SHINY DEEP WINE'],
                ['label'=>__('OLIVE'),'value'=>'OLIVE'],
                ['label'=>__('BROWN TEAL'),'value'=>'BROWN TEAL'],
                ['label'=>__('PURPLE PINK'),'value'=>'PURPLE PINK'],
                ['label'=>__('GRAPE LEMONADE'),'value'=>'GRAPE LEMONADE'],
                ['label'=>__('LIGHT PINK LILAC'),'value'=>'LIGHT PINK LILAC'],
                ['label'=>__('SHINY PURPLE'),'value'=>'SHINY PURPLE'],
                ['label'=>__('PURPLE HORN'),'value'=>'PURPLE HORN'],
                ['label'=>__('CABERNET'),'value'=>'CABERNET'],
                ['label'=>__('PURPLE ROSE'),'value'=>'PURPLE ROSE'],
                ['label'=>__('SATIN SILVER'),'value'=>'SATIN SILVER'],
                ['label'=>__('SATIN GOLD'),'value'=>'SATIN GOLD'],
                ['label'=>__('SATIN PLUM'),'value'=>'SATIN PLUM'],
                ['label'=>__('BLUE TORTOISE'),'value'=>'BLUE TORTOISE'],
                ['label'=>__('RED TORTOISE'),'value'=>'RED TORTOISE'],
                ['label'=>__('MAUVE'),'value'=>'MAUVE'],
                ['label'=>__('MT DK GRY/FLA LIME W/GRY'),'value'=>'MT DK GRY/FLA LIME W/GRY'],
                ['label'=>__('MATTE BLACK/VOLT W/GRY SI'),'value'=>'MATTE BLACK/VOLT W/GRY SI'],
                ['label'=>__('MT MID NAYBL LAG W/GRY BL'),'value'=>'MT MID NAYBL LAG W/GRY BL'],
                ['label'=>__('MATTE BLACK/VOLT'),'value'=>'MATTE BLACK/VOLT'],
                ['label'=>__('MATTE BLACK/ANTHRACITE/CL'),'value'=>'MATTE BLACK/ANTHRACITE/CL'],
                ['label'=>__('MT CRYSTAL DK MAGNET GRY/'),'value'=>'MT CRYSTAL DK MAGNET GRY/'],
                ['label'=>__('MT CRYSTAL MIDNIGHT NAVY/'),'value'=>'MT CRYSTAL MIDNIGHT NAVY/'],
                ['label'=>__('ROSE TORTOISE'),'value'=>'ROSE TORTOISE'],
                ['label'=>__('MATTE BLACK/MATTE CRYSTAL'),'value'=>'MATTE BLACK/MATTE CRYSTAL'],
                ['label'=>__('MATTE BLACK/CRYSTAL PHOTO'),'value'=>'MATTE BLACK/CRYSTAL PHOTO'],
                ['label'=>__('MATTE DARK GREY/CRYSTAL H'),'value'=>'MATTE DARK GREY/CRYSTAL H'],
                ['label'=>__('MATTE DARK GREY/CLEAR'),'value'=>'MATTE DARK GREY/CLEAR'],
                ['label'=>__('MAT CRYSTAL CARGO KHA/CRY'),'value'=>'MAT CRYSTAL CARGO KHA/CRY'],
                ['label'=>__('MATTE NAVY/PHOTO BLUE'),'value'=>'MATTE NAVY/PHOTO BLUE'],
                ['label'=>__('MT BLACK/MATTE CRYSTAL CL'),'value'=>'MT BLACK/MATTE CRYSTAL CL'],
                ['label'=>__('MT CRYSTAL DK GRY/CRYSTAL'),'value'=>'MT CRYSTAL DK GRY/CRYSTAL'],
                ['label'=>__('MT CRYSTAL NY/CRY BLEACH'),'value'=>'MT CRYSTAL NY/CRY BLEACH'],
                ['label'=>__('MATTE OBSIDIAN/HYPER JADE'),'value'=>'MATTE OBSIDIAN/HYPER JADE'],
                ['label'=>__('SATIN TAUPE'),'value'=>'SATIN TAUPE'],
                ['label'=>__('SATIN MINT'),'value'=>'SATIN MINT'],
                ['label'=>__('SATIN ROSE'),'value'=>'SATIN ROSE'],
                ['label'=>__('TEAL'),'value'=>'TEAL'],
                ['label'=>__('MATTE GREEN'),'value'=>'MATTE GREEN'],
                ['label'=>__('MATTE BLUE'),'value'=>'MATTE BLUE'],
                ['label'=>__('PETROLEUM'),'value'=>'PETROLEUM'],
                ['label'=>__('LAVENDER'),'value'=>'LAVENDER'],
                ['label'=>__('LILAC'),'value'=>'LILAC'],
                ['label'=>__('RUST'),'value'=>'RUST'],
                ['label'=>__('OLIVE GREEN'),'value'=>'OLIVE GREEN'],
                ['label'=>__('BLUE HORN'),'value'=>'BLUE HORN'],
                ['label'=>__('SATIN PURPLE'),'value'=>'SATIN PURPLE'],
                ['label'=>__('SHINY ROSE GOLD'),'value'=>'SHINY ROSE GOLD'],
                ['label'=>__('ANTHRACITE/BLACK'),'value'=>'ANTHRACITE/BLACK'],
                ['label'=>__('MIDNIGHT NAVY-PHOTO BLUE'),'value'=>'MIDNIGHT NAVY-PHOTO BLUE'],
                ['label'=>__('NAVY/RACER BLUE'),'value'=>'NAVY/RACER BLUE'],
                ['label'=>__('CRYSTAL PURPLE'),'value'=>'CRYSTAL PURPLE'],
                ['label'=>__('GUNMETAL-DARK GREY-CLEAR'),'value'=>'GUNMETAL-DARK GREY-CLEAR'],
                ['label'=>__('SATIN WALNUT-DEEPEST GREE'),'value'=>'SATIN WALNUT-DEEPEST GREE'],
                ['label'=>__('SATIN BLUE-MIDNIGHT NAVY-'),'value'=>'SATIN BLUE-MIDNIGHT NAVY-'],
                ['label'=>__('BLACK/ANTHRACITE'),'value'=>'BLACK/ANTHRACITE'],
                ['label'=>__('SATIN BLACK-HYPER JADE'),'value'=>'SATIN BLACK-HYPER JADE'],
                ['label'=>__('BRUSHED GUNMETAL-FLASH LI'),'value'=>'BRUSHED GUNMETAL-FLASH LI'],
                ['label'=>__('BLACK-VOLT'),'value'=>'BLACK-VOLT'],
                ['label'=>__('BLACK-HYPER PUNCH'),'value'=>'BLACK-HYPER PUNCH'],
                ['label'=>__('DARK GREY-HYPER JADE'),'value'=>'DARK GREY-HYPER JADE'],
                ['label'=>__('CARGO KHAKI-TOTAL ORANGE'),'value'=>'CARGO KHAKI-TOTAL ORANGE'],
                ['label'=>__('STRIPED SMOKE'),'value'=>'STRIPED SMOKE'],
                ['label'=>__('STRIPED BLUE'),'value'=>'STRIPED BLUE'],
                ['label'=>__('STRIPED WINE'),'value'=>'STRIPED WINE'],
                ['label'=>__('STRIPED LIGHT BROWN'),'value'=>'STRIPED LIGHT BROWN'],
                ['label'=>__('STRIPED VIOLET'),'value'=>'STRIPED VIOLET'],
                ['label'=>__('WHITE/VD PK W/GRY PK FL'),'value'=>'WHITE/VD PK W/GRY PK FL'],
                ['label'=>__('GOLD/KHAKI'),'value'=>'GOLD/KHAKI'],
                ['label'=>__('GOLD/GRADIENT GREY'),'value'=>'GOLD/GRADIENT GREY'],
                ['label'=>__('GOLD/GRADIENT KHAKI BEIGE'),'value'=>'GOLD/GRADIENT KHAKI BEIGE'],
                ['label'=>__('MATTE H2O PLASMA ION'),'value'=>'MATTE H2O PLASMA ION'],
                ['label'=>__('MATTE H2O GREEN ION'),'value'=>'MATTE H2O GREEN ION'],
                ['label'=>__('MATTE MAGNET GREY-SILVER'),'value'=>'MATTE MAGNET GREY-SILVER'],
                ['label'=>__('MATTE BLACK GREY'),'value'=>'MATTE BLACK GREY'],
                ['label'=>__('MATTE BLACK-BLUE ION'),'value'=>'MATTE BLACK-BLUE ION'],
                ['label'=>__('MATTE BLACK-GREEN ION'),'value'=>'MATTE BLACK-GREEN ION'],
                ['label'=>__('MATTE BLACK-COPPER'),'value'=>'MATTE BLACK-COPPER'],
                ['label'=>__('MATTE DEEP NAVY GREY'),'value'=>'MATTE DEEP NAVY GREY'],
                ['label'=>__('MATTE UTILITY GREEN GREY'),'value'=>'MATTE UTILITY GREEN GREY'],
                ['label'=>__('MATTE BLACK-RED ION'),'value'=>'MATTE BLACK-RED ION'],
                ['label'=>__('MATTE BLACK-SUPER SILVER'),'value'=>'MATTE BLACK-SUPER SILVER'],
                ['label'=>__('JET-DARK COPPER'),'value'=>'JET-DARK COPPER'],
                ['label'=>__('DARK SLATE BLUE'),'value'=>'DARK SLATE BLUE'],
                ['label'=>__('SLATE'),'value'=>'SLATE'],
                ['label'=>__('CRYSTAL'),'value'=>'CRYSTAL'],
                ['label'=>__('DEEP WINE'),'value'=>'DEEP WINE'],
                ['label'=>__('BRUSHED BLACK CHROME'),'value'=>'BRUSHED BLACK CHROME'],
                ['label'=>__('BRUSHED BROWN'),'value'=>'BRUSHED BROWN'],
                ['label'=>__('TURTLE DOVE'),'value'=>'TURTLE DOVE'],
                ['label'=>__('MARBLE BROWN'),'value'=>'MARBLE BROWN'],
                ['label'=>__('MARBLE VIOLET'),'value'=>'MARBLE VIOLET'],
                ['label'=>__('MARBLE RED'),'value'=>'MARBLE RED'],
                ['label'=>__('BLACK GREY PAISLEY'),'value'=>'BLACK GREY PAISLEY'],
                ['label'=>__('BLACK&WHITE PAISLEY'),'value'=>'BLACK&WHITE PAISLEY'],
                ['label'=>__('BROWN PAISLEY'),'value'=>'BROWN PAISLEY'],
                ['label'=>__('HAVANA PAISLEY'),'value'=>'HAVANA PAISLEY'],
                ['label'=>__('GREEN PAISLEY'),'value'=>'GREEN PAISLEY'],
                ['label'=>__('BLUE PAISLEY'),'value'=>'BLUE PAISLEY'],
                ['label'=>__('GOLD-BROWN'),'value'=>'GOLD-BROWN'],
                ['label'=>__('GOLD-GREY'),'value'=>'GOLD-GREY'],
                ['label'=>__('ROSE GOLD-PEACH'),'value'=>'ROSE GOLD-PEACH'],
                ['label'=>__('GOLD-KHAKI'),'value'=>'GOLD-KHAKI'],
                ['label'=>__('GOLD-TRANSPARENT PEACH'),'value'=>'GOLD-TRANSPARENT PEACH'],
                ['label'=>__('GOLD-TRANSPARENT GREY'),'value'=>'GOLD-TRANSPARENT GREY'],
                ['label'=>__('GOLD-CARAMEL'),'value'=>'GOLD-CARAMEL'],
                ['label'=>__('GOLD-TRANSPARENT LIGHT BR'),'value'=>'GOLD-TRANSPARENT LIGHT BR'],
                ['label'=>__('BLACK WITH STARPHOSPHO TE'),'value'=>'BLACK WITH STARPHOSPHO TE'],
                ['label'=>__('AZURE WITH GLITTER TEMPLE'),'value'=>'AZURE WITH GLITTER TEMPLE'],
                ['label'=>__('AQUA WITH PHOSPHO TEMPLES'),'value'=>'AQUA WITH PHOSPHO TEMPLES'],
                ['label'=>__('FUCHSIA WITH GLITTER TEMP'),'value'=>'FUCHSIA WITH GLITTER TEMP'],
                ['label'=>__('ROSE WITH PHOSPHO TEMPLES'),'value'=>'ROSE WITH PHOSPHO TEMPLES'],
                ['label'=>__('ONYX MATTE W/STAR PHOSPHO'),'value'=>'ONYX MATTE W/STAR PHOSPHO'],
                ['label'=>__('BLACK MATTE W/STAR PHOSPH'),'value'=>'BLACK MATTE W/STAR PHOSPH'],
                ['label'=>__('BROWN MATTE'),'value'=>'BROWN MATTE'],
                ['label'=>__('DARK GREEN'),'value'=>'DARK GREEN'],
                ['label'=>__('AQUA MATTE W/STAR PHOSPHO'),'value'=>'AQUA MATTE W/STAR PHOSPHO'],
                ['label'=>__('MID BLUE MATTE'),'value'=>'MID BLUE MATTE'],
                ['label'=>__('BLONDE TORTOISE'),'value'=>'BLONDE TORTOISE'],
                ['label'=>__('MATTE WHITE'),'value'=>'MATTE WHITE'],
                ['label'=>__('MATTE KHAKI'),'value'=>'MATTE KHAKI'],
                ['label'=>__('MATTE NAVY BLUE'),'value'=>'MATTE NAVY BLUE'],
                ['label'=>__('MATTE BLUE NIGHT'),'value'=>'MATTE BLUE NIGHT'],
                ['label'=>__('MATTE BURGUNDY'),'value'=>'MATTE BURGUNDY'],
                ['label'=>__('MATTE RED'),'value'=>'MATTE RED'],
                ['label'=>__('BLACK/MATTE BLACK W/GRY L'),'value'=>'BLACK/MATTE BLACK W/GRY L'],
                ['label'=>__('MT BLACK/DP PEWTER W/GRY'),'value'=>'MT BLACK/DP PEWTER W/GRY'],
                ['label'=>__('WOLF GREY/COOL GREY/TEAL'),'value'=>'WOLF GREY/COOL GREY/TEAL'],
                ['label'=>__('MT GREY TORTOISE W/DK GRY'),'value'=>'MT GREY TORTOISE W/DK GRY'],
                ['label'=>__('DARK ATOMIC TEAL/BLACK/AM'),'value'=>'DARK ATOMIC TEAL/BLACK/AM'],
                ['label'=>__('DARK TEAL/BLACK/AMBER LEN'),'value'=>'DARK TEAL/BLACK/AMBER LEN'],
                ['label'=>__('CLEAR W/GREEN LENS'),'value'=>'CLEAR W/GREEN LENS'],
                ['label'=>__('MT BLACK/CAMO W/DK LENS'),'value'=>'MT BLACK/CAMO W/DK LENS'],
                ['label'=>__('MATTE BLACK-DEEP PEWTER W'),'value'=>'MATTE BLACK-DEEP PEWTER W'],
                ['label'=>__('MATTE BLK-CINNABAR W-GRN'),'value'=>'MATTE BLK-CINNABAR W-GRN'],
                ['label'=>__('DARK GREY/GREEN'),'value'=>'DARK GREY/GREEN'],
                ['label'=>__('MT WF GREY-G RED W-GREY L'),'value'=>'MT WF GREY-G RED W-GREY L'],
                ['label'=>__('WOLF GREY/TEAL'),'value'=>'WOLF GREY/TEAL'],
                ['label'=>__('CARGO KHAKI/DARK GREY'),'value'=>'CARGO KHAKI/DARK GREY'],
                ['label'=>__('MT CRY SEAWD-CYB W-GREEN'),'value'=>'MT CRY SEAWD-CYB W-GREEN'],
                ['label'=>__('MT NAVY/CAMO W/GRY SIL FL'),'value'=>'MT NAVY/CAMO W/GRY SIL FL'],
                ['label'=>__('MT DARK OBS-WHITE W-GREY'),'value'=>'MT DARK OBS-WHITE W-GREY'],
                ['label'=>__('MT SQUAD BL-DP PEW W-GREY'),'value'=>'MT SQUAD BL-DP PEW W-GREY'],
                ['label'=>__('MIDNIGHT TEAL/DARK GREY'),'value'=>'MIDNIGHT TEAL/DARK GREY'],
                ['label'=>__('MATTE DARK GUNMETAL'),'value'=>'MATTE DARK GUNMETAL'],
                ['label'=>__('MATTE DARK BROWN'),'value'=>'MATTE DARK BROWN'],
                ['label'=>__('PAISLEY/BLACK'),'value'=>'PAISLEY/BLACK'],
                ['label'=>__('PAISLEY/GREEN'),'value'=>'PAISLEY/GREEN'],
                ['label'=>__('PAISLEY/BLUE'),'value'=>'PAISLEY/BLUE'],
                ['label'=>__('PAISLEY/VIOLET'),'value'=>'PAISLEY/VIOLET'],
                ['label'=>__('LIGHT GOLD/AZURE'),'value'=>'LIGHT GOLD/AZURE'],
                ['label'=>__('LIGHT GOLD/BLUE'),'value'=>'LIGHT GOLD/BLUE'],
                ['label'=>__('LIGHT GOLD/BLACK'),'value'=>'LIGHT GOLD/BLACK'],
                ['label'=>__('LIGHT GOLD/TAUPE'),'value'=>'LIGHT GOLD/TAUPE'],
                ['label'=>__('BLACK/BLACK VISETOS'),'value'=>'BLACK/BLACK VISETOS'],
                ['label'=>__('SHINY DARK GUN/BLACK'),'value'=>'SHINY DARK GUN/BLACK'],
                ['label'=>__('SHINY DARK GUN/KHAKI'),'value'=>'SHINY DARK GUN/KHAKI'],
                ['label'=>__('TORTOISE/COGNAC VISETOS'),'value'=>'TORTOISE/COGNAC VISETOS'],
                ['label'=>__('BLUE/PINK VISETOS'),'value'=>'BLUE/PINK VISETOS'],
                ['label'=>__('MATTE BLACK/BLACK VISETTO'),'value'=>'MATTE BLACK/BLACK VISETTO'],
                ['label'=>__('SILVER/SILVER MARBLE GLIT'),'value'=>'SILVER/SILVER MARBLE GLIT'],
                ['label'=>__('GOLD/GOLD MARBLE GLITTER'),'value'=>'GOLD/GOLD MARBLE GLITTER'],
                ['label'=>__('SHINY ROSE GOLD/BROWN VIS'),'value'=>'SHINY ROSE GOLD/BROWN VIS'],
                ['label'=>__('SHINY ROSE GOLD/NUDE VISE'),'value'=>'SHINY ROSE GOLD/NUDE VISE'],
                ['label'=>__('BLUE PEARLIZED'),'value'=>'BLUE PEARLIZED'],
                ['label'=>__('VIOLET PEARLIZED'),'value'=>'VIOLET PEARLIZED'],
                ['label'=>__('SHINY GOLD-GREY BLUE HORN'),'value'=>'SHINY GOLD-GREY BLUE HORN'],
                ['label'=>__('SHINY GOLD-HAVANA'),'value'=>'SHINY GOLD-HAVANA'],
                ['label'=>__('SHINY GOLD-BLACK'),'value'=>'SHINY GOLD-BLACK'],
                ['label'=>__('BROWN/NUDE GRADIENT'),'value'=>'BROWN/NUDE GRADIENT'],
                ['label'=>__('GRADIENT BURNT'),'value'=>'GRADIENT BURNT'],
                ['label'=>__('GRADIENT MAUVE'),'value'=>'GRADIENT MAUVE'],
                ['label'=>__('HAVANA BROWN'),'value'=>'HAVANA BROWN'],
                ['label'=>__('GUN'),'value'=>'GUN'],
                ['label'=>__('BROWN GRADIENT'),'value'=>'BROWN GRADIENT'],
                ['label'=>__('MATTE H2O-GREY'),'value'=>'MATTE H2O-GREY'],
                ['label'=>__('MATTE H2O-ROSE GOLD ION'),'value'=>'MATTE H2O-ROSE GOLD ION'],
                ['label'=>__('MATTE H2O-PLASMA ION'),'value'=>'MATTE H2O-PLASMA ION'],
                ['label'=>__('MATTE H2O-SILVER ION'),'value'=>'MATTE H2O-SILVER ION'],
                ['label'=>__('MATTE BLACK/SMOKE'),'value'=>'MATTE BLACK/SMOKE'],
                ['label'=>__('MATTE MAGNET GREY/RED'),'value'=>'MATTE MAGNET GREY/RED'],
                ['label'=>__('MATTE BLACK/SILVER'),'value'=>'MATTE BLACK/SILVER'],
                ['label'=>__('MATTE DEEP NAVY/BLUE SKY'),'value'=>'MATTE DEEP NAVY/BLUE SKY'],
                ['label'=>__('ANTIQUE GOLD'),'value'=>'ANTIQUE GOLD'],
                ['label'=>__('GRADIENT BORDEAUX'),'value'=>'GRADIENT BORDEAUX'],
                ['label'=>__('SMOKE CRYSTAL'),'value'=>'SMOKE CRYSTAL'],
                ['label'=>__('OLIVE CRYSTAL'),'value'=>'OLIVE CRYSTAL'],
                ['label'=>__('MATTE SMOKE HORN'),'value'=>'MATTE SMOKE HORN'],
                ['label'=>__('MATTE OLIVE HORN'),'value'=>'MATTE OLIVE HORN'],
                ['label'=>__('BROWN HORN GRADIENT'),'value'=>'BROWN HORN GRADIENT'],
                ['label'=>__('BLUE-BROWN HORN GRADIENT'),'value'=>'BLUE-BROWN HORN GRADIENT'],
                ['label'=>__('LILAC HORN GRADIENT'),'value'=>'LILAC HORN GRADIENT'],
                ['label'=>__('OXBLOOD'),'value'=>'OXBLOOD'],
                ['label'=>__('WHISKEY'),'value'=>'WHISKEY'],
                ['label'=>__('BROWN TORTOISE'),'value'=>'BROWN TORTOISE'],
                ['label'=>__('BERRY TORTOISE'),'value'=>'BERRY TORTOISE'],
                ['label'=>__('SHINY NAVY'),'value'=>'SHINY NAVY'],
                ['label'=>__('MATTE H2O BLACK GREY'),'value'=>'MATTE H2O BLACK GREY'],
                ['label'=>__('BLACK/MATTE BLACK W/GREY'),'value'=>'BLACK/MATTE BLACK W/GREY'],
                ['label'=>__('MATTE BLK/TUMB GRY W/GREY'),'value'=>'MATTE BLK/TUMB GRY W/GREY'],
                ['label'=>__('WOLF GREY/DP PEWT W/GREY'),'value'=>'WOLF GREY/DP PEWT W/GREY'],
                ['label'=>__('MT WF GRY/DP P W/GREY LEN'),'value'=>'MT WF GRY/DP P W/GREY LEN'],
                ['label'=>__('MT CAR KHAKI W/GRN GUN FL'),'value'=>'MT CAR KHAKI W/GRN GUN FL'],
                ['label'=>__('MT OBSIDIAN W/GRY SIL FL'),'value'=>'MT OBSIDIAN W/GRY SIL FL'],
                ['label'=>__('SKY BLUE'),'value'=>'SKY BLUE'],
                ['label'=>__('SATIN EARTH'),'value'=>'SATIN EARTH'],
                ['label'=>__('SILVER/BROWN MARBLE'),'value'=>'SILVER/BROWN MARBLE'],
                ['label'=>__('GOLD-PEACH'),'value'=>'GOLD-PEACH'],
                ['label'=>__('GOLD-BLONDE HAVANA'),'value'=>'GOLD-BLONDE HAVANA'],
                ['label'=>__('BLACK-HAVANA'),'value'=>'BLACK-HAVANA'],
                ['label'=>__('HAVANA-AMBER'),'value'=>'HAVANA-AMBER'],
                ['label'=>__('MATTE LIGHT GREY'),'value'=>'MATTE LIGHT GREY'],
                ['label'=>__('BLONDE HAVANA'),'value'=>'BLONDE HAVANA'],
                ['label'=>__('SATIN BLACK-PHOTO BLUE'),'value'=>'SATIN BLACK-PHOTO BLUE'],
                ['label'=>__('BRUSHED GUNMETAL-TOTAL OR'),'value'=>'BRUSHED GUNMETAL-TOTAL OR'],
                ['label'=>__('SATIN WALNUT-GREEN STRIKE'),'value'=>'SATIN WALNUT-GREEN STRIKE'],
                ['label'=>__('GUNMETAL-WOLF GREY'),'value'=>'GUNMETAL-WOLF GREY'],
                ['label'=>__('SATIN BLUE-PHOTO BLUE'),'value'=>'SATIN BLUE-PHOTO BLUE'],
                ['label'=>__('BRUSHED GUNMETAL-TEAM RED'),'value'=>'BRUSHED GUNMETAL-TEAM RED'],
                ['label'=>__('SATIN BLACK-WOLF GREY'),'value'=>'SATIN BLACK-WOLF GREY'],
                ['label'=>__('SATIN WALNUT'),'value'=>'SATIN WALNUT'],
                ['label'=>__('BRUSHED GUNMETAL-TIDEPOOL'),'value'=>'BRUSHED GUNMETAL-TIDEPOOL'],
                ['label'=>__('SLATE BLUE'),'value'=>'SLATE BLUE'],
                ['label'=>__('TORTOISE-BLACK'),'value'=>'TORTOISE-BLACK'],
                ['label'=>__('HAVANA/BLUE'),'value'=>'HAVANA/BLUE'],
                ['label'=>__('HAVANA/VIOLET'),'value'=>'HAVANA/VIOLET'],
                ['label'=>__('TORTOISE/BLACK'),'value'=>'TORTOISE/BLACK'],
                ['label'=>__('TRANSPARENT BORDEAUX/BORD'),'value'=>'TRANSPARENT BORDEAUX/BORD'],
                ['label'=>__('ROSE/NUDE VISETTOS'),'value'=>'ROSE/NUDE VISETTOS'],
                ['label'=>__('BLACK/COGNAC VISETTOS'),'value'=>'BLACK/COGNAC VISETTOS'],
                ['label'=>__('MT BLACK/SPEED TINT UML R'),'value'=>'MT BLACK/SPEED TINT UML R'],
                ['label'=>__('MATTE BLACK/WHITE/SPEED E'),'value'=>'MATTE BLACK/WHITE/SPEED E'],
                ['label'=>__('MATTE GREY/SPEED EXTRA WH'),'value'=>'MATTE GREY/SPEED EXTRA WH'],
                ['label'=>__('MT WHITE/SPEED TINT UML G'),'value'=>'MT WHITE/SPEED TINT UML G'],
                ['label'=>__('MATTE CARGO KHAKI/SPEED U'),'value'=>'MATTE CARGO KHAKI/SPEED U'],
                ['label'=>__('MATTE OBSIDIAN/SPEED TINT'),'value'=>'MATTE OBSIDIAN/SPEED TINT'],
                ['label'=>__('MATTE TEAM RED/SPEED TINT'),'value'=>'MATTE TEAM RED/SPEED TINT'],
                ['label'=>__('MT OBSID/DP RYL BLU/MAX S'),'value'=>'MT OBSID/DP RYL BLU/MAX S'],
                ['label'=>__('MATTE NAVY/SPEED TINT'),'value'=>'MATTE NAVY/SPEED TINT'],
                ['label'=>__('MT BLK/WOLF GRY/GRY SILVE'),'value'=>'MT BLK/WOLF GRY/GRY SILVE'],
                ['label'=>__('MT BLK/BR CRIM/GRY SIL FL'),'value'=>'MT BLK/BR CRIM/GRY SIL FL'],
                ['label'=>__('MT VOLT GREN/WH/GRY SIL F'),'value'=>'MT VOLT GREN/WH/GRY SIL F'],
                ['label'=>__('MATTE KHAKI/GREY SILVER F'),'value'=>'MATTE KHAKI/GREY SILVER F'],
                ['label'=>__('MT OBSID/RACR BLU/GRY SIL'),'value'=>'MT OBSID/RACR BLU/GRY SIL'],
                ['label'=>__('MT BR CRIMSON/WHITE/GRY S'),'value'=>'MT BR CRIMSON/WHITE/GRY S'],
                ['label'=>__('MT BLK/SILVR/GRY SUPER SI'),'value'=>'MT BLK/SILVR/GRY SUPER SI'],
                ['label'=>__('MT BLK/GOLD/GREY ML GOLD'),'value'=>'MT BLK/GOLD/GREY ML GOLD'],
                ['label'=>__('MATTE GREY/SPEED ML WHITE'),'value'=>'MATTE GREY/SPEED ML WHITE'],
                ['label'=>__('MT ANTH/GUMTL/GREY ML GRE'),'value'=>'MT ANTH/GUMTL/GREY ML GRE'],
                ['label'=>__('MT WH/BR CRIMSN/GRY SUPER'),'value'=>'MT WH/BR CRIMSN/GRY SUPER'],
                ['label'=>__('MT BK/BRGHT CRIMSON/MAX S'),'value'=>'MT BK/BRGHT CRIMSON/MAX S'],
                ['label'=>__('MT BLK/WOLF GREY/GRY SILV'),'value'=>'MT BLK/WOLF GREY/GRY SILV'],
                ['label'=>__('MT OBSID/DP RYL BLU/GRY S'),'value'=>'MT OBSID/DP RYL BLU/GRY S'],
                ['label'=>__('SMOKE'),'value'=>'SMOKE'],
                ['label'=>__('MT ANTHRA-GUNMTL-POLAR GR'),'value'=>'MT ANTHRA-GUNMTL-POLAR GR'],
                ['label'=>__('JET/GREY'),'value'=>'JET/GREY'],
                ['label'=>__('MATTE TORTOISE/BRONZE'),'value'=>'MATTE TORTOISE/BRONZE'],
                ['label'=>__('MATTE H2O MAGNET GREY'),'value'=>'MATTE H2O MAGNET GREY'],
                ['label'=>__('MATTE H2O SKY BLUE'),'value'=>'MATTE H2O SKY BLUE'],
                ['label'=>__('MATTE H2O ROSE GOLD ION'),'value'=>'MATTE H2O ROSE GOLD ION'],
                ['label'=>__('MATTE H2O BLUE ION'),'value'=>'MATTE H2O BLUE ION'],
                ['label'=>__('CRYSTAL BROWN'),'value'=>'CRYSTAL BROWN'],
                ['label'=>__('CRYSTAL OLIVE'),'value'=>'CRYSTAL OLIVE'],
                ['label'=>__('MATTE BLACK W/UNIVERSITY'),'value'=>'MATTE BLACK W/UNIVERSITY'],
                ['label'=>__('CLEAR/UNIVERSITY RED'),'value'=>'CLEAR/UNIVERSITY RED'],
                ['label'=>__('MATTE OBSIDIAN/SILVER'),'value'=>'MATTE OBSIDIAN/SILVER'],
                ['label'=>__('MATTE BLACK/UNIVERSITY RE'),'value'=>'MATTE BLACK/UNIVERSITY RE'],
                ['label'=>__('ANTHRACITE/UNIVERSITY RED'),'value'=>'ANTHRACITE/UNIVERSITY RED'],
                ['label'=>__('MATTE BLACK-GUNMETAL'),'value'=>'MATTE BLACK-GUNMETAL'],
                ['label'=>__('MATTE TUMBLED GREY-GREEN'),'value'=>'MATTE TUMBLED GREY-GREEN'],
                ['label'=>__('MATTE CRYSTAL CLEAR-BRIGH'),'value'=>'MATTE CRYSTAL CLEAR-BRIGH'],
                ['label'=>__('MATTE CRYSTAL CLEAR/BLUE'),'value'=>'MATTE CRYSTAL CLEAR/BLUE'],
                ['label'=>__('MATTE BLACK/CRYSTAL CLEAR'),'value'=>'MATTE BLACK/CRYSTAL CLEAR'],
                ['label'=>__('COBBLESTONE/SILVER'),'value'=>'COBBLESTONE/SILVER'],
                ['label'=>__('BLACK/CARGO KHAKI'),'value'=>'BLACK/CARGO KHAKI'],
                ['label'=>__('MATTE BLACK/DARK GREY'),'value'=>'MATTE BLACK/DARK GREY'],
                ['label'=>__('MATTE BLACK/WHITE'),'value'=>'MATTE BLACK/WHITE'],
                ['label'=>__('MATTE BLACK/MATTE RED'),'value'=>'MATTE BLACK/MATTE RED'],
                ['label'=>__('ANTHRACITE/SPACE BLUE'),'value'=>'ANTHRACITE/SPACE BLUE'],
                ['label'=>__('MATTE DARK GREY/VOLT'),'value'=>'MATTE DARK GREY/VOLT'],
                ['label'=>__('BLACK-DARK GREY'),'value'=>'BLACK-DARK GREY'],
                ['label'=>__('BLACK-TEAM RED'),'value'=>'BLACK-TEAM RED'],
                ['label'=>__('MATTE GREY/BLUE'),'value'=>'MATTE GREY/BLUE'],
                ['label'=>__('MATTE TORTOISE-GREEN STRI'),'value'=>'MATTE TORTOISE-GREEN STRI'],
                ['label'=>__('MATTE MIDNIGHT NAVY-DARK'),'value'=>'MATTE MIDNIGHT NAVY-DARK'],
                ['label'=>__('DARK GREY-TOTAL ORANGE'),'value'=>'DARK GREY-TOTAL ORANGE'],
                ['label'=>__('CARGO KHAKI-FLASH LIME'),'value'=>'CARGO KHAKI-FLASH LIME'],
                ['label'=>__('BLACK-FLASH LIME'),'value'=>'BLACK-FLASH LIME'],
                ['label'=>__('BLACK-PHOTO BLUE'),'value'=>'BLACK-PHOTO BLUE'],
                ['label'=>__('ANTHRACITE/RED'),'value'=>'ANTHRACITE/RED'],
                ['label'=>__('TEAM RED-BRIGHT CRIMSON'),'value'=>'TEAM RED-BRIGHT CRIMSON'],
                ['label'=>__('SATIN BLACK-TEAM RED'),'value'=>'SATIN BLACK-TEAM RED'],
                ['label'=>__('SATIN GUNMETAL-VOLT'),'value'=>'SATIN GUNMETAL-VOLT'],
                ['label'=>__('BEIGE'),'value'=>'BEIGE'],
                ['label'=>__('DARK TURQUOISE'),'value'=>'DARK TURQUOISE'],
                ['label'=>__('KHAKI BROWN GRADIENT'),'value'=>'KHAKI BROWN GRADIENT'],
                ['label'=>__('BLACK-DARK RUTHENIUM'),'value'=>'BLACK-DARK RUTHENIUM'],
                ['label'=>__('BLUE-DARK RUTHENIUM'),'value'=>'BLUE-DARK RUTHENIUM'],
                ['label'=>__('BROWN-SHINY GOLD'),'value'=>'BROWN-SHINY GOLD'],
                ['label'=>__('BLUE BARK'),'value'=>'BLUE BARK'],
                ['label'=>__('BUTTER HORN'),'value'=>'BUTTER HORN'],
                ['label'=>__('SHINY UMBER HORN'),'value'=>'SHINY UMBER HORN'],
                ['label'=>__('MATTE CRYSTAL SEA BLUE'),'value'=>'MATTE CRYSTAL SEA BLUE'],
                ['label'=>__('SHINY RED HORN'),'value'=>'SHINY RED HORN'],
                ['label'=>__('MOSS BROWN HORN'),'value'=>'MOSS BROWN HORN'],
                ['label'=>__('OLIVE HORN'),'value'=>'OLIVE HORN'],
                ['label'=>__('BLUE-YELLOW TORTOISE'),'value'=>'BLUE-YELLOW TORTOISE'],
                ['label'=>__('NUDE TORTOISE'),'value'=>'NUDE TORTOISE'],
                ['label'=>__('CRYSTAL SLATE'),'value'=>'CRYSTAL SLATE'],
                ['label'=>__('CRYSTAL JADE'),'value'=>'CRYSTAL JADE'],
                ['label'=>__('CRYSTAL SMOKE'),'value'=>'CRYSTAL SMOKE'],
                ['label'=>__('INDIGO'),'value'=>'INDIGO'],
                ['label'=>__('MATTE BLACK/DEEP PEWTER W'),'value'=>'MATTE BLACK/DEEP PEWTER W'],
                ['label'=>__('BROWN W/BROWN POLARIZED L'),'value'=>'BROWN W/BROWN POLARIZED L'],
                ['label'=>__('MUSHROOM'),'value'=>'MUSHROOM'],
                ['label'=>__('SHIMMER DARK TORTOISE'),'value'=>'SHIMMER DARK TORTOISE'],
                ['label'=>__('SKY BLUE TORTOISE'),'value'=>'SKY BLUE TORTOISE'],
                ['label'=>__('AQUAMARINE'),'value'=>'AQUAMARINE'],
                ['label'=>__('TOPAZ TORTOISE'),'value'=>'TOPAZ TORTOISE'],
                ['label'=>__('PACIFIC TORTOISE'),'value'=>'PACIFIC TORTOISE'],
                ['label'=>__('BLUSH TORTOISE'),'value'=>'BLUSH TORTOISE'],
                ['label'=>__('CYAN BLUE TORTOISE'),'value'=>'CYAN BLUE TORTOISE'],
                ['label'=>__('ROUGE TORTOISE'),'value'=>'ROUGE TORTOISE'],
                ['label'=>__('CRYSTAL TAN'),'value'=>'CRYSTAL TAN'],
                ['label'=>__('ROSE NUDE'),'value'=>'ROSE NUDE'],
                ['label'=>__('BLACK GREY CAMOUFLAGE'),'value'=>'BLACK GREY CAMOUFLAGE'],
                ['label'=>__('BROWN PEACH CAMOUFLAGE'),'value'=>'BROWN PEACH CAMOUFLAGE'],
                ['label'=>__('BLUE BROWN CAMOUFLAGE'),'value'=>'BLUE BROWN CAMOUFLAGE'],
                ['label'=>__('PURPLE VIOLET CAMOUFLAGE'),'value'=>'PURPLE VIOLET CAMOUFLAGE'],
                ['label'=>__('SATIN BLUE-DEEP ROYAL BLU'),'value'=>'SATIN BLUE-DEEP ROYAL BLU'],
                ['label'=>__('CRYSTAL BROWN W-CHEETAH'),'value'=>'CRYSTAL BROWN W-CHEETAH'],
                ['label'=>__('GOLD-NUDE'),'value'=>'GOLD-NUDE'],
                ['label'=>__('GOLD-BLACK'),'value'=>'GOLD-BLACK'],
                ['label'=>__('GRAD GREY/TURTLEDOVE'),'value'=>'GRAD GREY/TURTLEDOVE'],
                ['label'=>__('GRAD ANTIQUE ROSE/NUDE'),'value'=>'GRAD ANTIQUE ROSE/NUDE'],
                ['label'=>__('GRADIENT PETROL'),'value'=>'GRADIENT PETROL'],
                ['label'=>__('SMOKE GREY'),'value'=>'SMOKE GREY'],
                ['label'=>__('GOLDEN CHESTNUT'),'value'=>'GOLDEN CHESTNUT'],
                ['label'=>__('GRAPE'),'value'=>'GRAPE'],
                ['label'=>__('GOLD/NUDE'),'value'=>'GOLD/NUDE'],
                ['label'=>__('GOLD/BROWN'),'value'=>'GOLD/BROWN'],
                ['label'=>__('GOLD/BLACK'),'value'=>'GOLD/BLACK'],
                ['label'=>__('BURNT'),'value'=>'BURNT'],
                ['label'=>__('ONYX'),'value'=>'ONYX'],
                ['label'=>__('CRYSTAL BLUE'),'value'=>'CRYSTAL BLUE'],
                ['label'=>__('MT BLACK/SIL W/GREY POL L'),'value'=>'MT BLACK/SIL W/GREY POL L'],
                ['label'=>__('MATTE COFFEE'),'value'=>'MATTE COFFEE'],
                ['label'=>__('MATTE DEEP SEA'),'value'=>'MATTE DEEP SEA'],
                ['label'=>__('BROWN CRYSTAL'),'value'=>'BROWN CRYSTAL'],
                ['label'=>__('GREY CRYSTAL'),'value'=>'GREY CRYSTAL'],
                ['label'=>__('BLUSH CRYSTAL'),'value'=>'BLUSH CRYSTAL'],
                ['label'=>__('RED CRYSTAL'),'value'=>'RED CRYSTAL'],
                ['label'=>__('AQUA'),'value'=>'AQUA'],
                ['label'=>__('MATTE BLACK GREEN G15'),'value'=>'MATTE BLACK GREEN G15'],
                ['label'=>__('MATTE MAGNET GREY SILVER'),'value'=>'MATTE MAGNET GREY SILVER'],
                ['label'=>__('MATTE NAVY/BLUE SKY ION'),'value'=>'MATTE NAVY/BLUE SKY ION'],
                ['label'=>__('BRUSHED GUNMETAL-VOLTAGE'),'value'=>'BRUSHED GUNMETAL-VOLTAGE'],
                ['label'=>__('SATIN GUNMETAL-TEAM RED'),'value'=>'SATIN GUNMETAL-TEAM RED'],
                ['label'=>__('BLACK-WHITE'),'value'=>'BLACK-WHITE'],
                ['label'=>__('SATIN BLACK-ANTHRACITE'),'value'=>'SATIN BLACK-ANTHRACITE'],
                ['label'=>__('BRUSHED GUNMETAL-OBSIDIAN'),'value'=>'BRUSHED GUNMETAL-OBSIDIAN'],
                ['label'=>__('WALNUT-CARGO KHAKI'),'value'=>'WALNUT-CARGO KHAKI'],
                ['label'=>__('HUNTER'),'value'=>'HUNTER'],
                ['label'=>__('HAVANA/TURQUOISE'),'value'=>'HAVANA/TURQUOISE'],
                ['label'=>__('BROWN/STRAWBERRY'),'value'=>'BROWN/STRAWBERRY'],
                ['label'=>__('BLUE/NUDE'),'value'=>'BLUE/NUDE'],
                ['label'=>__('BLACK/NUT'),'value'=>'BLACK/NUT'],
                ['label'=>__('GEOMETRIC FUCHSIA AZURE'),'value'=>'GEOMETRIC FUCHSIA AZURE'],
                ['label'=>__('GEOMETRIC BLUE RED'),'value'=>'GEOMETRIC BLUE RED'],
                ['label'=>__('GEOMETRIC BROWN GREY'),'value'=>'GEOMETRIC BROWN GREY'],
                ['label'=>__('SATIN GOLDEN'),'value'=>'SATIN GOLDEN'],
                ['label'=>__('GREYBROWN'),'value'=>'GREYBROWN'],
                ['label'=>__('AMETHYST'),'value'=>'AMETHYST'],
                ['label'=>__('MERLOT'),'value'=>'MERLOT'],
                ['label'=>__('BROWN/TEAL'),'value'=>'BROWN/TEAL'],
                ['label'=>__('NAVY/LAVENDER'),'value'=>'NAVY/LAVENDER'],
                ['label'=>__('PLUM/ROSE'),'value'=>'PLUM/ROSE'],
                ['label'=>__('BROWN/LAVENDER'),'value'=>'BROWN/LAVENDER'],
                ['label'=>__('NAVY/TEAL'),'value'=>'NAVY/TEAL'],
                ['label'=>__('PLUM/LILAC'),'value'=>'PLUM/LILAC'],
                ['label'=>__('PLUM TORTOISE'),'value'=>'PLUM TORTOISE'],
                ['label'=>__('SMOKEY GRAPE'),'value'=>'SMOKEY GRAPE'],
                ['label'=>__('VIOLET TORTOISE'),'value'=>'VIOLET TORTOISE'],
                ['label'=>__('PURPLE BLUE'),'value'=>'PURPLE BLUE'],
                ['label'=>__('MT BLACK/SIL W/GRY SIL FL'),'value'=>'MT BLACK/SIL W/GRY SIL FL'],
                ['label'=>__('MT TORTOISE W/GRN GUN FL'),'value'=>'MT TORTOISE W/GRN GUN FL'],
                ['label'=>__('BLACK/VOLT W/GRY W/SIL FL'),'value'=>'BLACK/VOLT W/GRY W/SIL FL'],
                ['label'=>__('MT WF GRY/OBS W/GY SIL FL'),'value'=>'MT WF GRY/OBS W/GY SIL FL'],
                ['label'=>__('MATTE A/BLACK W/ DK GREY'),'value'=>'MATTE A/BLACK W/ DK GREY'],
                ['label'=>__('WHITE W/GREY SILVER FLASH'),'value'=>'WHITE W/GREY SILVER FLASH'],
                ['label'=>__('MATTE CAR KHA/BLK W/ OUT'),'value'=>'MATTE CAR KHA/BLK W/ OUT'],
                ['label'=>__('MATTE OBS/OC FG W/GRY SIL'),'value'=>'MATTE OBS/OC FG W/GRY SIL'],
                ['label'=>__('BROWN ROSE'),'value'=>'BROWN ROSE'],
                ['label'=>__('PEACH'),'value'=>'PEACH'],
                ['label'=>__('LIGHT TURTLEDOVE'),'value'=>'LIGHT TURTLEDOVE'],
                ['label'=>__('DARK BLUE CHROME'),'value'=>'DARK BLUE CHROME'],
                ['label'=>__('BROWN/JADEITE'),'value'=>'BROWN/JADEITE'],
                ['label'=>__('TEAL/GOLD'),'value'=>'TEAL/GOLD'],
                ['label'=>__('NAVY/MAGENTA'),'value'=>'NAVY/MAGENTA'],
                ['label'=>__('BURGUNDY/PLUM'),'value'=>'BURGUNDY/PLUM'],
                ['label'=>__('BLACK GOLD'),'value'=>'BLACK GOLD'],
                ['label'=>__('NAVY/CORAL'),'value'=>'NAVY/CORAL'],
                ['label'=>__('BURGUNDY/ASH ROSE'),'value'=>'BURGUNDY/ASH ROSE'],
                ['label'=>__('MATTE BLUE NAVY'),'value'=>'MATTE BLUE NAVY'],
                ['label'=>__('TURQUOISE'),'value'=>'TURQUOISE'],
                ['label'=>__('MATTE EGGPLANT'),'value'=>'MATTE EGGPLANT'],
                ['label'=>__('TEAL HORN'),'value'=>'TEAL HORN'],
                ['label'=>__('MATTE TOKYO TORTOISE'),'value'=>'MATTE TOKYO TORTOISE'],
                ['label'=>__('SHINY DEEP RED'),'value'=>'SHINY DEEP RED'],
                ['label'=>__('SHINY BLACK/SMOKE'),'value'=>'SHINY BLACK/SMOKE'],
                ['label'=>__('MATTE DARK TORTOISE/BROWN'),'value'=>'MATTE DARK TORTOISE/BROWN'],
                ['label'=>__('MATTE CRYSTAL NAVY/BLUE'),'value'=>'MATTE CRYSTAL NAVY/BLUE'],
                ['label'=>__('MATTE CRYSTAL/RED ION'),'value'=>'MATTE CRYSTAL/RED ION'],
                ['label'=>__('MATTE BLACK/BLUE'),'value'=>'MATTE BLACK/BLUE'],
                ['label'=>__('SHINY TORTOISE/BROWN'),'value'=>'SHINY TORTOISE/BROWN'],
                ['label'=>__('SHINY KHAKI/GOLDEN'),'value'=>'SHINY KHAKI/GOLDEN'],
                ['label'=>__('MATTE CRYSTAL GREY/SMOKE'),'value'=>'MATTE CRYSTAL GREY/SMOKE'],
                ['label'=>__('TOKYO TORTOISE/GREEN'),'value'=>'TOKYO TORTOISE/GREEN'],
                ['label'=>__('SHINY RED/RED ION'),'value'=>'SHINY RED/RED ION'],
                ['label'=>__('CHROME'),'value'=>'CHROME'],
                ['label'=>__('BLACK/SHINY GOLD'),'value'=>'BLACK/SHINY GOLD'],
                ['label'=>__('BROWN/SHINY GOLD'),'value'=>'BROWN/SHINY GOLD'],
                ['label'=>__('ORCHID/SHINY GOLD'),'value'=>'ORCHID/SHINY GOLD'],
                ['label'=>__('STRIPED GREY/BROWN/GREEN'),'value'=>'STRIPED GREY/BROWN/GREEN'],
                ['label'=>__('HAVANA/HONEY'),'value'=>'HAVANA/HONEY'],
                ['label'=>__('BLUE/LILAC'),'value'=>'BLUE/LILAC'],
                ['label'=>__('BURGUNDY/ROSE'),'value'=>'BURGUNDY/ROSE'],
                ['label'=>__('BLACK/MILK'),'value'=>'BLACK/MILK'],
                ['label'=>__('CRYSTAL BURGUNDY'),'value'=>'CRYSTAL BURGUNDY'],
                ['label'=>__('SHINY GOLD/BLACK'),'value'=>'SHINY GOLD/BLACK'],
                ['label'=>__('SHINY LIGHT GOLD/BURGUNDY'),'value'=>'SHINY LIGHT GOLD/BURGUNDY'],
                ['label'=>__('SHINY GOLD/BROWN'),'value'=>'SHINY GOLD/BROWN'],
                ['label'=>__('SHINY GOLD/CREAM'),'value'=>'SHINY GOLD/CREAM'],
                ['label'=>__('MARBLE GREY'),'value'=>'MARBLE GREY'],
                ['label'=>__('GREEN HORN'),'value'=>'GREEN HORN'],
                ['label'=>__('GREY BLUE HORN'),'value'=>'GREY BLUE HORN'],
                ['label'=>__('SHINY PALLADIUM/MARBLE GR'),'value'=>'SHINY PALLADIUM/MARBLE GR'],
                ['label'=>__('SHINY GOLD/MARBLE BROWN'),'value'=>'SHINY GOLD/MARBLE BROWN'],
                ['label'=>__('MATTE TORTOISE/BLACK'),'value'=>'MATTE TORTOISE/BLACK'],
                ['label'=>__('BAROQUE BROWN/ORANGE QUAR'),'value'=>'BAROQUE BROWN/ORANGE QUAR'],
                ['label'=>__('MATTE SQUADRON BLUE/GUNME'),'value'=>'MATTE SQUADRON BLUE/GUNME'],
                ['label'=>__('MAGENTA'),'value'=>'MAGENTA'],
                ['label'=>__('PINK'),'value'=>'PINK'],
                ['label'=>__('MATTE UTILITY GREEN/GREY'),'value'=>'MATTE UTILITY GREEN/GREY'],
                ['label'=>__('MATTE NAVY/GREY'),'value'=>'MATTE NAVY/GREY'],
                ['label'=>__('BLACK VISETOS/BLACK'),'value'=>'BLACK VISETOS/BLACK'],
                ['label'=>__('COGNAC VISETOS/BLACK'),'value'=>'COGNAC VISETOS/BLACK'],
                ['label'=>__('NUDE VISETOS'),'value'=>'NUDE VISETOS'],
                ['label'=>__('GREEN VISETOS'),'value'=>'GREEN VISETOS'],
                ['label'=>__('BROWN/COGNAC VISETOS'),'value'=>'BROWN/COGNAC VISETOS'],
                ['label'=>__('CORAL VISETOS'),'value'=>'CORAL VISETOS'],
                ['label'=>__('BLACK/BLACK VISETTOS'),'value'=>'BLACK/BLACK VISETTOS'],
                ['label'=>__('BROWN/COGNAC VISETTOS'),'value'=>'BROWN/COGNAC VISETTOS'],
                ['label'=>__('BLUE/COGNAC VISETTOS'),'value'=>'BLUE/COGNAC VISETTOS'],
                ['label'=>__('BLACK MARBLE'),'value'=>'BLACK MARBLE'],
                ['label'=>__('TORTOISE BLACK'),'value'=>'TORTOISE BLACK'],
                ['label'=>__('BROWN MARBLE'),'value'=>'BROWN MARBLE'],
                ['label'=>__('RED MARBLE'),'value'=>'RED MARBLE'],
                ['label'=>__('CRYSTAL SAPPHIRE'),'value'=>'CRYSTAL SAPPHIRE'],
                ['label'=>__('CRYSTAL RUBY'),'value'=>'CRYSTAL RUBY'],
                ['label'=>__('BLACK/ACID GREEN'),'value'=>'BLACK/ACID GREEN'],
                ['label'=>__('DARK HAVANA/ORANGE'),'value'=>'DARK HAVANA/ORANGE'],
                ['label'=>__('LIGHT HAVANA/PURPLE'),'value'=>'LIGHT HAVANA/PURPLE'],
                ['label'=>__('GREEN/OCHRE'),'value'=>'GREEN/OCHRE'],
                ['label'=>__('DARK BLUE/ELECTRIC BLUE'),'value'=>'DARK BLUE/ELECTRIC BLUE'],
                ['label'=>__('LAPIS CRYSTAL'),'value'=>'LAPIS CRYSTAL'],
                ['label'=>__('AZURE'),'value'=>'AZURE'],
                ['label'=>__('ORCHID'),'value'=>'ORCHID'],
                ['label'=>__('SATIN DENIM'),'value'=>'SATIN DENIM'],
                ['label'=>__('SEMIMATTE RED'),'value'=>'SEMIMATTE RED'],
                ['label'=>__('MINT BROWN CAMOUFLAGE'),'value'=>'MINT BROWN CAMOUFLAGE'],
                ['label'=>__('ORCHID BROWN CAMOUFLAGE'),'value'=>'ORCHID BROWN CAMOUFLAGE'],
                ['label'=>__('COBALT GREEN PURPLE CAMOU'),'value'=>'COBALT GREEN PURPLE CAMOU'],
                ['label'=>__('COSMETIC PINK'),'value'=>'COSMETIC PINK'],
                ['label'=>__('GREY HAVANA'),'value'=>'GREY HAVANA'],
                ['label'=>__('SAND'),'value'=>'SAND'],
                ['label'=>__('STRIPED BORDEAUX'),'value'=>'STRIPED BORDEAUX'],
                ['label'=>__('BROWN HAVANA'),'value'=>'BROWN HAVANA'],
                ['label'=>__('PETROL HAVANA'),'value'=>'PETROL HAVANA'],
                ['label'=>__('PURPLE HAVANA'),'value'=>'PURPLE HAVANA'],
                ['label'=>__('HAVANA/PURPLE'),'value'=>'HAVANA/PURPLE'],
                ['label'=>__('STRIPED BLUE/FUCHSIA'),'value'=>'STRIPED BLUE/FUCHSIA'],
                ['label'=>__('VIOLET/ORANGE'),'value'=>'VIOLET/ORANGE'],
                ['label'=>__('STRIPED BORDEAUX/AZURE'),'value'=>'STRIPED BORDEAUX/AZURE'],
                ['label'=>__('BLACK PAISLEY'),'value'=>'BLACK PAISLEY'],
                ['label'=>__('TURTLE DOVE PAISLEY'),'value'=>'TURTLE DOVE PAISLEY'],
                ['label'=>__('BORDEAUX PAISLEY'),'value'=>'BORDEAUX PAISLEY'],
                ['label'=>__('POUDRE'),'value'=>'POUDRE'],
                ['label'=>__('MAT BLACK PAISLEY'),'value'=>'MAT BLACK PAISLEY'],
                ['label'=>__('MAT PURPLE PAISLEY'),'value'=>'MAT PURPLE PAISLEY'],
                ['label'=>__('GREY HORN/OCHRE'),'value'=>'GREY HORN/OCHRE'],
                ['label'=>__('LIGHT BROWN HORN/ORANGE'),'value'=>'LIGHT BROWN HORN/ORANGE'],
                ['label'=>__('BLACK/TURTLE DOVE'),'value'=>'BLACK/TURTLE DOVE'],
                ['label'=>__('DARK HAVANA/BURNT'),'value'=>'DARK HAVANA/BURNT'],
                ['label'=>__('TURTLE DOVE/CYCLAMEN'),'value'=>'TURTLE DOVE/CYCLAMEN'],
                ['label'=>__('STRIPED HONEY/DARK BLUE'),'value'=>'STRIPED HONEY/DARK BLUE'],
                ['label'=>__('STRIPED GREY/CYCLAMEN'),'value'=>'STRIPED GREY/CYCLAMEN'],
                ['label'=>__('STRIPED GREEN/ORANGE'),'value'=>'STRIPED GREEN/ORANGE'],
                ['label'=>__('BLACK TORTOISE'),'value'=>'BLACK TORTOISE'],
                ['label'=>__('ANTIQUE ASH BROWN'),'value'=>'ANTIQUE ASH BROWN'],
                ['label'=>__('JET/BLACK'),'value'=>'JET/BLACK'],
                ['label'=>__('BLACK/VACHETTA'),'value'=>'BLACK/VACHETTA'],
                ['label'=>__('MILKY BONE'),'value'=>'MILKY BONE'],
                ['label'=>__('MILKY NAVY'),'value'=>'MILKY NAVY'],
                ['label'=>__('BLACK/ CREAM'),'value'=>'BLACK/ CREAM'],
                ['label'=>__('CREAM TORTOISE'),'value'=>'CREAM TORTOISE'],
                ['label'=>__('BEIGE MARBLE'),'value'=>'BEIGE MARBLE'],
                ['label'=>__('ROSE MARBLE'),'value'=>'ROSE MARBLE'],
                ['label'=>__('MINK TORTOISE'),'value'=>'MINK TORTOISE'],
                ['label'=>__('BONE'),'value'=>'BONE'],
                ['label'=>__('SATIN TITANIUM'),'value'=>'SATIN TITANIUM'],
                ['label'=>__('GOLD/TRANSPARENT PEACH'),'value'=>'GOLD/TRANSPARENT PEACH'],
                ['label'=>__('GOLD/TRANSPARENT LIGHT GR'),'value'=>'GOLD/TRANSPARENT LIGHT GR'],
                ['label'=>__('GOLD/HAVANA'),'value'=>'GOLD/HAVANA'],
                ['label'=>__('GOLD/TRANSPARENT KHAKI'),'value'=>'GOLD/TRANSPARENT KHAKI'],
                ['label'=>__('GOLD/TRANSPARENT GREEN'),'value'=>'GOLD/TRANSPARENT GREEN'],
                ['label'=>__('ROSE GOLD/TRANSPARENT BRO'),'value'=>'ROSE GOLD/TRANSPARENT BRO'],
                ['label'=>__('GOLD/HAVANA/GRAD YELLOW L'),'value'=>'GOLD/HAVANA/GRAD YELLOW L'],
                ['label'=>__('GOLD/HAVANA/GRAD CYCLAMEN'),'value'=>'GOLD/HAVANA/GRAD CYCLAMEN'],
                ['label'=>__('SILVER/LIGHT GREY'),'value'=>'SILVER/LIGHT GREY'],
                ['label'=>__('GOLD/ROSE'),'value'=>'GOLD/ROSE'],
                ['label'=>__('LIGHT BURNT'),'value'=>'LIGHT BURNT'],
                ['label'=>__('CARAMEL'),'value'=>'CARAMEL'],
                ['label'=>__('GOLD/LIGHT BROWN'),'value'=>'GOLD/LIGHT BROWN'],
                ['label'=>__('GOLD/GREY'),'value'=>'GOLD/GREY'],
                ['label'=>__('GOLD/ROSE PEACH'),'value'=>'GOLD/ROSE PEACH'],
                ['label'=>__('GOLD/BROWN PEACH'),'value'=>'GOLD/BROWN PEACH'],
                ['label'=>__('GOLD/HAVANA/GRAD ROSE HON'),'value'=>'GOLD/HAVANA/GRAD ROSE HON'],
                ['label'=>__('GOLD/HAVANA/GRAD GREEN/RO'),'value'=>'GOLD/HAVANA/GRAD GREEN/RO'],
                ['label'=>__('BURGUNDY TORTOISE'),'value'=>'BURGUNDY TORTOISE'],
                ['label'=>__('MATTE ANTHRA/BRIGHT CRIMS'),'value'=>'MATTE ANTHRA/BRIGHT CRIMS'],
                ['label'=>__('TOBACCO'),'value'=>'TOBACCO'],
                ['label'=>__('AUTUMN BLUE'),'value'=>'AUTUMN BLUE'],
                ['label'=>__('MUSTARD TORTOISE'),'value'=>'MUSTARD TORTOISE'],
                ['label'=>__('PETROL TORTOISE'),'value'=>'PETROL TORTOISE'],
                ['label'=>__('TURTLE GRADIENT'),'value'=>'TURTLE GRADIENT'],
                ['label'=>__('BLUE GRADIENT'),'value'=>'BLUE GRADIENT'],
                ['label'=>__('BURGUNDY GRADIENT'),'value'=>'BURGUNDY GRADIENT'],
                ['label'=>__('LIGHT SHINY GOLD/BLACK'),'value'=>'LIGHT SHINY GOLD/BLACK'],
                ['label'=>__('SHINY GOLD/IVORY'),'value'=>'SHINY GOLD/IVORY'],
                ['label'=>__('SHINY GOLD/RED'),'value'=>'SHINY GOLD/RED'],
                ['label'=>__('IVORY/PLUM'),'value'=>'IVORY/PLUM'],
                ['label'=>__('TORTOISE/PETROL'),'value'=>'TORTOISE/PETROL'],
                ['label'=>__('BLACK/TORTOISE'),'value'=>'BLACK/TORTOISE'],
                ['label'=>__('TORTOISE/IVORY'),'value'=>'TORTOISE/IVORY'],
                ['label'=>__('WOOD/BROWN'),'value'=>'WOOD/BROWN'],
                ['label'=>__('WOOD/TAUPE'),'value'=>'WOOD/TAUPE'],
                ['label'=>__('WOOD/SMOKE'),'value'=>'WOOD/SMOKE'],
                ['label'=>__('COPPER'),'value'=>'COPPER'],
                ['label'=>__('TORTOISE/GREEN'),'value'=>'TORTOISE/GREEN'],
                ['label'=>__('TOKYO TORTOISE/RUBY'),'value'=>'TOKYO TORTOISE/RUBY'],
                ['label'=>__('BLUE/GREY'),'value'=>'BLUE/GREY'],
                ['label'=>__('SLATE VISETOS'),'value'=>'SLATE VISETOS'],
                ['label'=>__('BURNT VISETOS'),'value'=>'BURNT VISETOS'],
                ['label'=>__('SPARKLY IVORY'),'value'=>'SPARKLY IVORY'],
                ['label'=>__('SPARKLY JEANS'),'value'=>'SPARKLY JEANS'],
                ['label'=>__('SPARKLY PINK'),'value'=>'SPARKLY PINK'],
                ['label'=>__('BLACK VISETOS'),'value'=>'BLACK VISETOS'],
                ['label'=>__('STRIPED BLUE/BLUE VISETOS'),'value'=>'STRIPED BLUE/BLUE VISETOS'],
                ['label'=>__('STRIPED ORCHID/VIOLET VIS'),'value'=>'STRIPED ORCHID/VIOLET VIS'],
                ['label'=>__('HAVANA BLUE'),'value'=>'HAVANA BLUE'],
                ['label'=>__('HAVANA VIOLET'),'value'=>'HAVANA VIOLET'],
                ['label'=>__('STRIPED KHAKI'),'value'=>'STRIPED KHAKI'],
                ['label'=>__('BUTTERSCOTCH'),'value'=>'BUTTERSCOTCH'],
                ['label'=>__('BLACK/CORAL'),'value'=>'BLACK/CORAL'],
                ['label'=>__('BRUSHED GUNMTL/TIDE POOL'),'value'=>'BRUSHED GUNMTL/TIDE POOL'],
                ['label'=>__('SATIN WALNUT/TUMBLED GREY'),'value'=>'SATIN WALNUT/TUMBLED GREY'],
                ['label'=>__('MATTE BLACK/PURE PLATINUM'),'value'=>'MATTE BLACK/PURE PLATINUM'],
                ['label'=>__('SATIN WALNUT/GREEN STRIKE'),'value'=>'SATIN WALNUT/GREEN STRIKE'],
                ['label'=>__('BLUE/PURE PLATINUM'),'value'=>'BLUE/PURE PLATINUM'],
                ['label'=>__('SATIN BLUE/PHOTO BLUE'),'value'=>'SATIN BLUE/PHOTO BLUE'],
                ['label'=>__('GREY MARBLE'),'value'=>'GREY MARBLE'],
                ['label'=>__('BLONDE HAVANA/SHINY TEAL'),'value'=>'BLONDE HAVANA/SHINY TEAL'],
                ['label'=>__('WHITE'),'value'=>'WHITE'],
                ['label'=>__('MEDIUM HAVANA'),'value'=>'MEDIUM HAVANA'],
                ['label'=>__('ROSEGOLD'),'value'=>'ROSEGOLD'],
                ['label'=>__('YELLOW GOLD'),'value'=>'YELLOW GOLD'],
                ['label'=>__('MATTE LIGHT BROWN'),'value'=>'MATTE LIGHT BROWN'],
                ['label'=>__('STRIPED BROWN'),'value'=>'STRIPED BROWN'],
                ['label'=>__('MATTE ANTH W/GRN MIRR LEN'),'value'=>'MATTE ANTH W/GRN MIRR LEN'],
                ['label'=>__('MT MLK/P WI W/GRY W BL PC'),'value'=>'MT MLK/P WI W/GRY W BL PC'],
                ['label'=>__('MATTE SQUAD BLU W/BL MIRR'),'value'=>'MATTE SQUAD BLU W/BL MIRR'],
                ['label'=>__('CRY CL/MED OL W/TRI COP L'),'value'=>'CRY CL/MED OL W/TRI COP L'],
                ['label'=>__('BLACK W/GREY POL LENS'),'value'=>'BLACK W/GREY POL LENS'],
                ['label'=>__('MT BROWN W/BRWN POL LENS'),'value'=>'MT BROWN W/BRWN POL LENS'],
                ['label'=>__('BLACK W/GREY POLARIZED LE'),'value'=>'BLACK W/GREY POLARIZED LE'],
                ['label'=>__('CRYSTAL YELLOW'),'value'=>'CRYSTAL YELLOW'],
                ['label'=>__('GRADIENT BLACK'),'value'=>'GRADIENT BLACK'],
                ['label'=>__('GRADIENT GREY/TURTLEDOVE'),'value'=>'GRADIENT GREY/TURTLEDOVE'],
                ['label'=>__('GRADIENT CARAMEL/CHAMPAGN'),'value'=>'GRADIENT CARAMEL/CHAMPAGN'],
                ['label'=>__('GRADIENT TURTLEDOVE/LIGHT'),'value'=>'GRADIENT TURTLEDOVE/LIGHT'],
                ['label'=>__('MT PLATNM/VOLT/SPED WHITE'),'value'=>'MT PLATNM/VOLT/SPED WHITE'],
                ['label'=>__('BLACK/GUNMETAL'),'value'=>'BLACK/GUNMETAL'],
                ['label'=>__('MARINA BLUE'),'value'=>'MARINA BLUE'],
                ['label'=>__('DARK PLUM'),'value'=>'DARK PLUM'],
                ['label'=>__('FUCHSIA'),'value'=>'FUCHSIA'],
                ['label'=>__('DARK NAVY'),'value'=>'DARK NAVY'],
                ['label'=>__('MATTE BLACK/GREY SILVER F'),'value'=>'MATTE BLACK/GREY SILVER F'],
                ['label'=>__('MATTE ANTHRACITE/DARK GRE'),'value'=>'MATTE ANTHRACITE/DARK GRE'],
                ['label'=>__('MATTE OBSIDIAN/GREY SILVE'),'value'=>'MATTE OBSIDIAN/GREY SILVE'],
                ['label'=>__('MATTE GREY/SPEED TINT'),'value'=>'MATTE GREY/SPEED TINT'],
                ['label'=>__('MATTE BLACK/VOLT/SPED ML'),'value'=>'MATTE BLACK/VOLT/SPED ML'],
                ['label'=>__('BLUE/GREY BLUE FLASH'),'value'=>'BLUE/GREY BLUE FLASH'],
                ['label'=>__('MATTE SHARK'),'value'=>'MATTE SHARK'],
                ['label'=>__('GREY TORTOISE'),'value'=>'GREY TORTOISE'],
                ['label'=>__('MATTE DEEP GREEN'),'value'=>'MATTE DEEP GREEN'],
                ['label'=>__('MATTE COLLEGIATE NAVY'),'value'=>'MATTE COLLEGIATE NAVY'],
                ['label'=>__('MATTE ANTHRACITE W/GREY L'),'value'=>'MATTE ANTHRACITE W/GREY L'],
                ['label'=>__('MATTE BLACK W/GREY LENS'),'value'=>'MATTE BLACK W/GREY LENS'],
                ['label'=>__('MATTE GRAVEL/FIRE'),'value'=>'MATTE GRAVEL/FIRE'],
                ['label'=>__('CRYSTAL SHARK/SMOKE FLASH'),'value'=>'CRYSTAL SHARK/SMOKE FLASH'],
                ['label'=>__('MATTE SHARK/BLUE'),'value'=>'MATTE SHARK/BLUE'],
                ['label'=>__('MATTE DARK COMPASS/COBALT'),'value'=>'MATTE DARK COMPASS/COBALT'],
                ['label'=>__('MATTE NIGHT SHADOW'),'value'=>'MATTE NIGHT SHADOW'],
                ['label'=>__('MATTE MERCURY/BLUE SKY FL'),'value'=>'MATTE MERCURY/BLUE SKY FL'],
                ['label'=>__('MATTE COLLEGIATE NAVY/SMO'),'value'=>'MATTE COLLEGIATE NAVY/SMO'],
                ['label'=>__('MATTE DARK COMPASS/AMBER'),'value'=>'MATTE DARK COMPASS/AMBER'],
                ['label'=>__('BLACK/SMOKE'),'value'=>'BLACK/SMOKE'],
                ['label'=>__('MATTE SHARK/SMOKE'),'value'=>'MATTE SHARK/SMOKE'],
                ['label'=>__('SATIN BLACK/SMOKE'),'value'=>'SATIN BLACK/SMOKE'],
                ['label'=>__('GUNMETAL/SMOKE'),'value'=>'GUNMETAL/SMOKE'],
                ['label'=>__('BLUE COMPASS/ORANGE FLASH'),'value'=>'BLUE COMPASS/ORANGE FLASH'],
                ['label'=>__('SATIN GUNMETAL/GREEN'),'value'=>'SATIN GUNMETAL/GREEN'],
                ['label'=>__('SATIN GUNMETAL/SMOKE'),'value'=>'SATIN GUNMETAL/SMOKE'],
                ['label'=>__('MATTE BLACK/GREEN'),'value'=>'MATTE BLACK/GREEN'],
                ['label'=>__('MATTE CINDER/BROWN'),'value'=>'MATTE CINDER/BROWN'],
                ['label'=>__('SATIN GREY MOSS'),'value'=>'SATIN GREY MOSS'],
                ['label'=>__('SATIN SAND'),'value'=>'SATIN SAND'],
                ['label'=>__('HAVANA/BEIGE'),'value'=>'HAVANA/BEIGE'],
                ['label'=>__('TOKYO HAVANA/PURPLE'),'value'=>'TOKYO HAVANA/PURPLE'],
                ['label'=>__('BLUE/HAVANA'),'value'=>'BLUE/HAVANA'],
                ['label'=>__('PETROL GREEN'),'value'=>'PETROL GREEN'],
                ['label'=>__('BLACK/DARK RUTHENIUM'),'value'=>'BLACK/DARK RUTHENIUM'],
                ['label'=>__('BLUE AVIATION'),'value'=>'BLUE AVIATION'],
                ['label'=>__('SHINY SILVER'),'value'=>'SHINY SILVER'],
                ['label'=>__('WOLF GREY'),'value'=>'WOLF GREY'],
                ['label'=>__('MATTE ANTHRACITE'),'value'=>'MATTE ANTHRACITE'],
                ['label'=>__('MATTE SEAWEED'),'value'=>'MATTE SEAWEED'],
                ['label'=>__('MATTE OBSIDIAN'),'value'=>'MATTE OBSIDIAN'],
                ['label'=>__('MATTE TORTOISE/G15'),'value'=>'MATTE TORTOISE/G15'],
                ['label'=>__('MATTE BLUE CHALK/SMOKE'),'value'=>'MATTE BLUE CHALK/SMOKE'],
                ['label'=>__('MATTE MAGNET GREY/SMOKE'),'value'=>'MATTE MAGNET GREY/SMOKE'],
                ['label'=>__('MATTE TORTOISE/GREEN'),'value'=>'MATTE TORTOISE/GREEN'],
                ['label'=>__('MATTE DEEP SEA/SMOKE'),'value'=>'MATTE DEEP SEA/SMOKE'],
                ['label'=>__('BROWN/MINT'),'value'=>'BROWN/MINT'],
                ['label'=>__('BROWN/ROSE'),'value'=>'BROWN/ROSE'],
                ['label'=>__('GREEN TORTOISE'),'value'=>'GREEN TORTOISE'],
                ['label'=>__('BLACK W/GREY BLACK MIRROR'),'value'=>'BLACK W/GREY BLACK MIRROR'],
                ['label'=>__('MATTE TORTOISE W/BROWN LE'),'value'=>'MATTE TORTOISE W/BROWN LE'],
                ['label'=>__('MT PURPLE W/GRY SILVER FL'),'value'=>'MT PURPLE W/GRY SILVER FL'],
                ['label'=>__('MATTE BLACK/SOLAR RED'),'value'=>'MATTE BLACK/SOLAR RED'],
                ['label'=>__('SPACE BLUE'),'value'=>'SPACE BLUE'],
                ['label'=>__('MATTE CARGO'),'value'=>'MATTE CARGO'],
                ['label'=>__('MATTE CARGO KHAKI'),'value'=>'MATTE CARGO KHAKI'],
                ['label'=>__('MT BLACK/GRN GRA W/GRN FL'),'value'=>'MT BLACK/GRN GRA W/GRN FL'],
                ['label'=>__('MT BLACK/R GRAD W/ML RD L'),'value'=>'MT BLACK/R GRAD W/ML RD L'],
                ['label'=>__('MT BLACK/S RD W/GRN SUN L'),'value'=>'MT BLACK/S RD W/GRN SUN L'],
                ['label'=>__('MT BLACK/HY CRI W/GR AMAR'),'value'=>'MT BLACK/HY CRI W/GR AMAR'],
                ['label'=>__('WH/TUR GRN W/GRN FL LENS'),'value'=>'WH/TUR GRN W/GRN FL LENS'],
                ['label'=>__('MT TORT W/GRN GUN FLASH L'),'value'=>'MT TORT W/GRN GUN FLASH L'],
                ['label'=>__('MT GREEN W/GRN TRIFLECT L'),'value'=>'MT GREEN W/GRN TRIFLECT L'],
                ['label'=>__('MT RED W/GRY TRIFLECT COP'),'value'=>'MT RED W/GRY TRIFLECT COP'],
                ['label'=>__('CLEAR W/GREY SILVER FLASH'),'value'=>'CLEAR W/GREY SILVER FLASH'],
                ['label'=>__('MT BLACK W/DARK GREY LENS'),'value'=>'MT BLACK W/DARK GREY LENS'],
                ['label'=>__('BLACK W/DARK GREY LENS'),'value'=>'BLACK W/DARK GREY LENS'],
                ['label'=>__('MT BLK/CL GR W/GRY SIL FL'),'value'=>'MT BLK/CL GR W/GRY SIL FL'],
                ['label'=>__('WOLF GREY/GREY LENS'),'value'=>'WOLF GREY/GREY LENS'],
                ['label'=>__('MT GREY W/GREY SILVER FL'),'value'=>'MT GREY W/GREY SILVER FL'],
                ['label'=>__('MT BLACK/RA GRN W/DK GRY'),'value'=>'MT BLACK/RA GRN W/DK GRY'],
                ['label'=>__('MT BLACK/CLEAR F W/DK GRY'),'value'=>'MT BLACK/CLEAR F W/DK GRY'],
                ['label'=>__('MT BLK/R GRN PRI W GR ML'),'value'=>'MT BLK/R GRN PRI W GR ML'],
                ['label'=>__('MT BLK/CR F W/GY ML AMA L'),'value'=>'MT BLK/CR F W/GY ML AMA L'],
                ['label'=>__('MT CARGO KHAKI W/GRY COP'),'value'=>'MT CARGO KHAKI W/GRY COP'],
                ['label'=>__('MT BLUE W/GREEN TRI PET L'),'value'=>'MT BLUE W/GREEN TRI PET L'],
                ['label'=>__('CLEAR W/GREY SUPER FLASH'),'value'=>'CLEAR W/GREY SUPER FLASH'],
                ['label'=>__('MT BLACK W/HY VIO F DK GY'),'value'=>'MT BLACK W/HY VIO F DK GY'],
                ['label'=>__('MT GREY W/DARK GREY LENS'),'value'=>'MT GREY W/DARK GREY LENS'],
                ['label'=>__('MT BLACK W/GRY ML RD FL L'),'value'=>'MT BLACK W/GRY ML RD FL L'],
                ['label'=>__('BLACK W/GRY ML GOLD FLASH'),'value'=>'BLACK W/GRY ML GOLD FLASH'],
                ['label'=>__('MT W GRY W/GRY BLU NI FL'),'value'=>'MT W GRY W/GRY BLU NI FL'],
                ['label'=>__('MT BLACK/AUR GRN PR W/AM'),'value'=>'MT BLACK/AUR GRN PR W/AM'],
                ['label'=>__('MT ANTHRA W/GRY VIO FL LE'),'value'=>'MT ANTHRA W/GRY VIO FL LE'],
                ['label'=>__('MT BLACK/CL F W/GRN ML SU'),'value'=>'MT BLACK/CL F W/GRN ML SU'],
                ['label'=>__('MT GRN W/GRN TRI PT LENS'),'value'=>'MT GRN W/GRN TRI PT LENS'],
                ['label'=>__('MT CARGO K W/GRY W/ML GRN'),'value'=>'MT CARGO K W/GRY W/ML GRN'],
                ['label'=>__('MT BLACK/S RED FA W/DK GY'),'value'=>'MT BLACK/S RED FA W/DK GY'],
                ['label'=>__('ANTHRACITE W/GRY SIL FL L'),'value'=>'ANTHRACITE W/GRY SIL FL L'],
                ['label'=>__('CARGO KHAKI W/OUTDOOR TIN'),'value'=>'CARGO KHAKI W/OUTDOOR TIN'],
                ['label'=>__('MT SQUAD BL W/GRY SIL FL'),'value'=>'MT SQUAD BL W/GRY SIL FL'],
                ['label'=>__('MATTE RED W/DARK GREY LEN'),'value'=>'MATTE RED W/DARK GREY LEN'],
                ['label'=>__('MT TORT W/GRN GUN FL LENS'),'value'=>'MT TORT W/GRN GUN FL LENS'],
                ['label'=>__('MATTE BACK W/DARK GREY LE'),'value'=>'MATTE BACK W/DARK GREY LE'],
                ['label'=>__('MT BLACK W/GREY POL LENS'),'value'=>'MT BLACK W/GREY POL LENS'],
                ['label'=>__('BLACK W/GREY SILVER FL LE'),'value'=>'BLACK W/GREY SILVER FL LE'],
                ['label'=>__('MATTE ANTH/WF GY W/OUT LE'),'value'=>'MATTE ANTH/WF GY W/OUT LE'],
                ['label'=>__('MT GREY W/GREY W/SILVER F'),'value'=>'MT GREY W/GREY W/SILVER F'],
                ['label'=>__('MT ANTHRACITE W/DARK GREY'),'value'=>'MT ANTHRACITE W/DARK GREY'],
                ['label'=>__('MT SP BL/RA GRN W/DK GY L'),'value'=>'MT SP BL/RA GRN W/DK GY L'],
                ['label'=>__('MT CAR KHAKI W/GRY TRI CO'),'value'=>'MT CAR KHAKI W/GRY TRI CO'],
                ['label'=>__('MT NAVY W/GRN STAN MIRR L'),'value'=>'MT NAVY W/GRN STAN MIRR L'],
                ['label'=>__('MATTE BLACK CKARK LITTLE'),'value'=>'MATTE BLACK CKARK LITTLE'],
                ['label'=>__('BORDEAUX HORN'),'value'=>'BORDEAUX HORN'],
                ['label'=>__('BLACK HORN'),'value'=>'BLACK HORN'],
                ['label'=>__('AQUA HORN'),'value'=>'AQUA HORN'],
                ['label'=>__('ROSE HORN'),'value'=>'ROSE HORN'],
                ['label'=>__('HAVANA/CRYSTAL'),'value'=>'HAVANA/CRYSTAL'],
                ['label'=>__('BORDEAUX/CRYSTAL'),'value'=>'BORDEAUX/CRYSTAL'],
                ['label'=>__('TURQUOISE PAISLEY'),'value'=>'TURQUOISE PAISLEY'],
                ['label'=>__('RED PAISLEY'),'value'=>'RED PAISLEY'],
                ['label'=>__('ORANGE PAISLEY'),'value'=>'ORANGE PAISLEY'],
                ['label'=>__('MATTE AMBER TORTOISE'),'value'=>'MATTE AMBER TORTOISE'],
                ['label'=>__('MATTE BLACK/GUNMETAL'),'value'=>'MATTE BLACK/GUNMETAL'],
                ['label'=>__('MATTE GUNMETAL/GUNMETAL'),'value'=>'MATTE GUNMETAL/GUNMETAL'],
                ['label'=>__('MATTE BROWN/GUNMETAL'),'value'=>'MATTE BROWN/GUNMETAL'],
                ['label'=>__('MATTE OLIVE/GUNMETAL'),'value'=>'MATTE OLIVE/GUNMETAL'],
                ['label'=>__('MATTE TAUPE'),'value'=>'MATTE TAUPE'],
                ['label'=>__('MATTE BRICK'),'value'=>'MATTE BRICK'],
                ['label'=>__('MATTE ANTIQUE GOLD'),'value'=>'MATTE ANTIQUE GOLD'],
                ['label'=>__('ANTHRACITE/GREY SUPER IVO'),'value'=>'ANTHRACITE/GREY SUPER IVO'],
                ['label'=>__('DK TEAL/GREY ML AMARANTHI'),'value'=>'DK TEAL/GREY ML AMARANTHI'],
                ['label'=>__('PORT WINE/GREEN ML SUNSHI'),'value'=>'PORT WINE/GREEN ML SUNSHI'],
                ['label'=>__('MT BLACK/ANTHRA W/GRY SIL'),'value'=>'MT BLACK/ANTHRA W/GRY SIL'],
                ['label'=>__('MT BLUE W/GRN TRI PET LEN'),'value'=>'MT BLUE W/GRN TRI PET LEN'],
                ['label'=>__('RED MATTE'),'value'=>'RED MATTE'],
                ['label'=>__('SAGE'),'value'=>'SAGE'],
                ['label'=>__('MATTE BLACK W/DARK GREY L'),'value'=>'MATTE BLACK W/DARK GREY L'],
                ['label'=>__('MT TOK TORT W/GRN GUN FL'),'value'=>'MT TOK TORT W/GRN GUN FL'],
                ['label'=>__('MATTE CRYSTAL CLEAR/SILVE'),'value'=>'MATTE CRYSTAL CLEAR/SILVE'],
                ['label'=>__('JAPANESE GOLD'),'value'=>'JAPANESE GOLD'],
                ['label'=>__('SOFT TORTOISE/HORN'),'value'=>'SOFT TORTOISE/HORN'],
                ['label'=>__('CRYSTAL OXBLOOD'),'value'=>'CRYSTAL OXBLOOD'],
                ['label'=>__('TITANIUM'),'value'=>'TITANIUM'],
                ['label'=>__('MILKY CLOUDY BLUE'),'value'=>'MILKY CLOUDY BLUE'],
                ['label'=>__('MILKY ROSE'),'value'=>'MILKY ROSE'],
                ['label'=>__('SATIN NICKEL'),'value'=>'SATIN NICKEL'],
                ['label'=>__('MATTE CRYSTAL GREY/BLUE S'),'value'=>'MATTE CRYSTAL GREY/BLUE S'],
                ['label'=>__('MATTE MAGNET GREY/BLUE IO'),'value'=>'MATTE MAGNET GREY/BLUE IO'],
                ['label'=>__('BLACK LEOPARD'),'value'=>'BLACK LEOPARD'],
                ['label'=>__('BROWN LEOPARD'),'value'=>'BROWN LEOPARD'],
                ['label'=>__('CRYSTAL CHARCOAL'),'value'=>'CRYSTAL CHARCOAL'],
                ['label'=>__('MILKY TAUPE'),'value'=>'MILKY TAUPE'],
                ['label'=>__('MATTE CRYSTAL CLEAR'),'value'=>'MATTE CRYSTAL CLEAR'],
                ['label'=>__('BROWN/TORTOISE'),'value'=>'BROWN/TORTOISE'],
                ['label'=>__('PURPLE/TORTOISE'),'value'=>'PURPLE/TORTOISE'],
                ['label'=>__('RED/TORTOISE'),'value'=>'RED/TORTOISE'],
                ['label'=>__('CRYSTAL TEAL'),'value'=>'CRYSTAL TEAL'],
                ['label'=>__('TEAL LEOPARD'),'value'=>'TEAL LEOPARD'],
                ['label'=>__('RED LEOPARD'),'value'=>'RED LEOPARD'],
                ['label'=>__('CHARCOAL LEOPARD'),'value'=>'CHARCOAL LEOPARD'],
                ['label'=>__('CYRSTAL GRAY'),'value'=>'CYRSTAL GRAY'],
                ['label'=>__('CRYSTAL BLUSH'),'value'=>'CRYSTAL BLUSH'],
                ['label'=>__('CRYSTAL BEIGE'),'value'=>'CRYSTAL BEIGE'],
                ['label'=>__('CYRSTAL GREEN'),'value'=>'CYRSTAL GREEN'],
                ['label'=>__('MILKY CHARCOAL'),'value'=>'MILKY CHARCOAL'],
                ['label'=>__('BLACK/GLITTER'),'value'=>'BLACK/GLITTER'],
                ['label'=>__('BROWN/GLITTER'),'value'=>'BROWN/GLITTER'],
                ['label'=>__('PURPLE/GLITTER'),'value'=>'PURPLE/GLITTER'],
                ['label'=>__('MAGENTA/GLITTER'),'value'=>'MAGENTA/GLITTER'],
                ['label'=>__('TEAL/PURPLE TORTOISE'),'value'=>'TEAL/PURPLE TORTOISE'],
                ['label'=>__('LAVENDER/BLUSH TORTOISE'),'value'=>'LAVENDER/BLUSH TORTOISE'],
                ['label'=>__('PEACH/BLUSH TORTOISE'),'value'=>'PEACH/BLUSH TORTOISE'],
                ['label'=>__('MARBLE BEIGE'),'value'=>'MARBLE BEIGE'],
                ['label'=>__('GOLD/POUDRE'),'value'=>'GOLD/POUDRE'],
                ['label'=>__('BLACK HAVANA'),'value'=>'BLACK HAVANA'],
                ['label'=>__('BLUE VISETOS'),'value'=>'BLUE VISETOS'],
                ['label'=>__('BROWN LUREX'),'value'=>'BROWN LUREX'],
                ['label'=>__('BLUE LUREX'),'value'=>'BLUE LUREX'],
                ['label'=>__('RED LUREX'),'value'=>'RED LUREX'],
                ['label'=>__('SHINY GOLD/WHITE'),'value'=>'SHINY GOLD/WHITE'],
                ['label'=>__('SHINY GOLD/ROSE'),'value'=>'SHINY GOLD/ROSE'],
                ['label'=>__('BROWN/ACID VISETOS'),'value'=>'BROWN/ACID VISETOS'],
                ['label'=>__('NAVY/SILVER GLITTER VISET'),'value'=>'NAVY/SILVER GLITTER VISET'],
                ['label'=>__('PETROL/ROSE VISETOS'),'value'=>'PETROL/ROSE VISETOS'],
                ['label'=>__('AZURE VISETOS'),'value'=>'AZURE VISETOS'],
                ['label'=>__('VIOLET VISETOS'),'value'=>'VIOLET VISETOS'],
                ['label'=>__('TURTLEDOVE VISETOS'),'value'=>'TURTLEDOVE VISETOS'],
                ['label'=>__('ROSE BOUQUET'),'value'=>'ROSE BOUQUET'],
                ['label'=>__('MARBLE BLUE'),'value'=>'MARBLE BLUE'],
                ['label'=>__('MARBLE PINK'),'value'=>'MARBLE PINK'],
                ['label'=>__('DARK RUTHENIUM'),'value'=>'DARK RUTHENIUM'],
                ['label'=>__('AMBER'),'value'=>'AMBER'],
                ['label'=>__('MATTE BLACK/SKY BLUE'),'value'=>'MATTE BLACK/SKY BLUE'],
                ['label'=>__('MATTE CRYSTAL SLATE/BLUE'),'value'=>'MATTE CRYSTAL SLATE/BLUE'],
                ['label'=>__('MATTE WOODGRAIN/COPPER IO'),'value'=>'MATTE WOODGRAIN/COPPER IO'],
                ['label'=>__('MATTE REDWOOD/BLUE SKY'),'value'=>'MATTE REDWOOD/BLUE SKY'],
                ['label'=>__('BROWN FADE'),'value'=>'BROWN FADE'],
                ['label'=>__('SHINY GUNMETAL/BLUE'),'value'=>'SHINY GUNMETAL/BLUE'],
                ['label'=>__('STRIPED BLACK/WHITE'),'value'=>'STRIPED BLACK/WHITE'],
                ['label'=>__('STRIPED GREY'),'value'=>'STRIPED GREY'],
                ['label'=>__('BROWN/STRIPED GREY'),'value'=>'BROWN/STRIPED GREY'],
                ['label'=>__('STRIPED BLUE/RED'),'value'=>'STRIPED BLUE/RED'],
                ['label'=>__('RED SNAKE'),'value'=>'RED SNAKE'],
                ['label'=>__('ROSE SNAKE'),'value'=>'ROSE SNAKE'],
                ['label'=>__('TORTOISE/RED'),'value'=>'TORTOISE/RED'],
                ['label'=>__('TORTOISE/PURPLE'),'value'=>'TORTOISE/PURPLE'],
                ['label'=>__('HAVANA/SHINY GOLD'),'value'=>'HAVANA/SHINY GOLD'],
                ['label'=>__('PETROL/SHINY GOLD'),'value'=>'PETROL/SHINY GOLD'],
                ['label'=>__('BLACK/PETROL'),'value'=>'BLACK/PETROL'],
                ['label'=>__('ANTHRACITE/GREY SILVER FL'),'value'=>'ANTHRACITE/GREY SILVER FL'],
                ['label'=>__('BLACK/SMOKE GRADIENT'),'value'=>'BLACK/SMOKE GRADIENT'],
                ['label'=>__('WALNUT/BROWN'),'value'=>'WALNUT/BROWN'],
                ['label'=>__('MATTE GOLD/G15'),'value'=>'MATTE GOLD/G15'],
                ['label'=>__('SHINY BLACK/SILVER FLASH'),'value'=>'SHINY BLACK/SILVER FLASH'],
                ['label'=>__('MATTE NAVY/BLUE FLASH'),'value'=>'MATTE NAVY/BLUE FLASH'],
                ['label'=>__('BLUE GREY/BLUE FLASH'),'value'=>'BLUE GREY/BLUE FLASH'],
                ['label'=>__('GREY CRYSTAL/SMOKE'),'value'=>'GREY CRYSTAL/SMOKE'],
                ['label'=>__('BROWN CRYSTAL/BROWN'),'value'=>'BROWN CRYSTAL/BROWN'],
                ['label'=>__('NAVY CRYSTAL/BLUE'),'value'=>'NAVY CRYSTAL/BLUE'],
                ['label'=>__('SHINY GUNMETAL/SMOKE'),'value'=>'SHINY GUNMETAL/SMOKE'],
                ['label'=>__('MATTE BLACK/SILVER FLASH'),'value'=>'MATTE BLACK/SILVER FLASH'],
                ['label'=>__('MATTE BLUE/GOLF'),'value'=>'MATTE BLUE/GOLF'],
                ['label'=>__('BLACK/BLACK PAISLEY'),'value'=>'BLACK/BLACK PAISLEY'],
                ['label'=>__('DARK BROWN/ORANGE PAISLEY'),'value'=>'DARK BROWN/ORANGE PAISLEY'],
                ['label'=>__('DARK BLUE/TURQUOISE PAISL'),'value'=>'DARK BLUE/TURQUOISE PAISL'],
                ['label'=>__('BORDEAUX/RED PAISLEY'),'value'=>'BORDEAUX/RED PAISLEY'],
                ['label'=>__('BROWN/ORANGE PAISLEY'),'value'=>'BROWN/ORANGE PAISLEY'],
                ['label'=>__('RED HAVANA'),'value'=>'RED HAVANA'],
                ['label'=>__('BLUE/TURQUOISE PAISLEY'),'value'=>'BLUE/TURQUOISE PAISLEY'],
                ['label'=>__('STRIPED HONEY'),'value'=>'STRIPED HONEY'],
                ['label'=>__('PURPLE PAISLEY'),'value'=>'PURPLE PAISLEY'],
                ['label'=>__('TURTLEDOVE PAISLEY/BORDEA'),'value'=>'TURTLEDOVE PAISLEY/BORDEA'],
                ['label'=>__('DARK BROWN PAISLEY'),'value'=>'DARK BROWN PAISLEY'],
                ['label'=>__('FOG PAISLEY/PETROL'),'value'=>'FOG PAISLEY/PETROL'],
                ['label'=>__('TURQUOISE BLUE MARBLE'),'value'=>'TURQUOISE BLUE MARBLE'],
                ['label'=>__('VIOLET MARBLE'),'value'=>'VIOLET MARBLE'],
                ['label'=>__('STRIPED ANTIQUE ROSE'),'value'=>'STRIPED ANTIQUE ROSE'],
                ['label'=>__('STRIPED CYCLAMINE'),'value'=>'STRIPED CYCLAMINE'],
                ['label'=>__('STRIPED PURPLE'),'value'=>'STRIPED PURPLE'],
                ['label'=>__('LIGHT BLUE'),'value'=>'LIGHT BLUE'],
                ['label'=>__('STRIPED RED'),'value'=>'STRIPED RED'],
                ['label'=>__('STRIPED GREY/PEACH'),'value'=>'STRIPED GREY/PEACH'],
                ['label'=>__('STRIPED BROWN/MINT'),'value'=>'STRIPED BROWN/MINT'],
                ['label'=>__('STRIPED PURPLE/LILAC'),'value'=>'STRIPED PURPLE/LILAC'],
                ['label'=>__('TAUPE MARBLE'),'value'=>'TAUPE MARBLE'],
                ['label'=>__('GREEN MARBLE'),'value'=>'GREEN MARBLE'],
                ['label'=>__('PURPLE MARBLE'),'value'=>'PURPLE MARBLE'],
                ['label'=>__('MATTE PETROL'),'value'=>'MATTE PETROL'],
                ['label'=>__('CHARCOAL TORTOISE'),'value'=>'CHARCOAL TORTOISE'],
                ['label'=>__('CRYSTAL PETROL'),'value'=>'CRYSTAL PETROL'],
                ['label'=>__('CRYSTAL VIOLET'),'value'=>'CRYSTAL VIOLET'],
                ['label'=>__('CRYSTAL RED'),'value'=>'CRYSTAL RED'],
                ['label'=>__('BLUE VIOLET'),'value'=>'BLUE VIOLET'],
                ['label'=>__('SAGE HORN'),'value'=>'SAGE HORN'],
                ['label'=>__('LAVENDAR'),'value'=>'LAVENDAR'],
                ['label'=>__('BLACK/JET'),'value'=>'BLACK/JET'],
                ['label'=>__('MILKY FOREST GREEN'),'value'=>'MILKY FOREST GREEN'],
                ['label'=>__('NAVY HORN'),'value'=>'NAVY HORN'],
                ['label'=>__('HONEY TORTOISE'),'value'=>'HONEY TORTOISE'],
                ['label'=>__('STRIPED AZURE BROWN'),'value'=>'STRIPED AZURE BROWN'],
                ['label'=>__('STRIPED PURPLE AZURE'),'value'=>'STRIPED PURPLE AZURE'],
                ['label'=>__('STRIPED RED TORTOISE'),'value'=>'STRIPED RED TORTOISE'],
                ['label'=>__('GREY SNAKE'),'value'=>'GREY SNAKE'],
                ['label'=>__('BLUE SNAKE'),'value'=>'BLUE SNAKE'],
                ['label'=>__('BLACK/ICE'),'value'=>'BLACK/ICE'],
                ['label'=>__('TORTOISE/NUDE'),'value'=>'TORTOISE/NUDE'],
                ['label'=>__('LIGHT GREEN'),'value'=>'LIGHT GREEN'],
                ['label'=>__('MATTE AZURE'),'value'=>'MATTE AZURE'],
                ['label'=>__('MATTE CYCLAMEN'),'value'=>'MATTE CYCLAMEN'],
                ['label'=>__('LIGHT ROSE'),'value'=>'LIGHT ROSE'],
                ['label'=>__('MATTE REDWOOD'),'value'=>'MATTE REDWOOD'],
                ['label'=>__('MATTE MAGNET GREY'),'value'=>'MATTE MAGNET GREY'],
                ['label'=>__('MATTE DEEP NAVY'),'value'=>'MATTE DEEP NAVY'],
                ['label'=>__('PONY BROWN'),'value'=>'PONY BROWN'],
                ['label'=>__('MATTE BEET RED'),'value'=>'MATTE BEET RED'],
                ['label'=>__('GREEN BOULDER TORTOISE'),'value'=>'GREEN BOULDER TORTOISE'],
                ['label'=>__('GLASS BLUE'),'value'=>'GLASS BLUE'],
                ['label'=>__('MATTE BLACK/BLUE SKY ION'),'value'=>'MATTE BLACK/BLUE SKY ION'],
                ['label'=>__('MATTE DEEP NAVY/BLUE ION'),'value'=>'MATTE DEEP NAVY/BLUE ION'],
                ['label'=>__('DEEP MOCHA'),'value'=>'DEEP MOCHA'],
                ['label'=>__('SUNRISE RED'),'value'=>'SUNRISE RED'],
                ['label'=>__('STRIPED GREY BROWN'),'value'=>'STRIPED GREY BROWN'],
                ['label'=>__('STRIPED AZURE TORTOISE'),'value'=>'STRIPED AZURE TORTOISE'],
                ['label'=>__('GOLD/TURTLEDOVE'),'value'=>'GOLD/TURTLEDOVE'],
                ['label'=>__('ROSE GOLD/TURTLEDOVE'),'value'=>'ROSE GOLD/TURTLEDOVE'],
                ['label'=>__('GRADIENT BLACK/GREY'),'value'=>'GRADIENT BLACK/GREY'],
                ['label'=>__('GREDIENT GREY/ROSE'),'value'=>'GREDIENT GREY/ROSE'],
                ['label'=>__('GRADIENT BROWN/HONEY'),'value'=>'GRADIENT BROWN/HONEY'],
                ['label'=>__('GRADIENT TURTLEDOVE/PEACH'),'value'=>'GRADIENT TURTLEDOVE/PEACH'],
                ['label'=>__('GRADIENT BORDEAUX/GREY'),'value'=>'GRADIENT BORDEAUX/GREY'],
                ['label'=>__('MIDNIGHT NAVY'),'value'=>'MIDNIGHT NAVY'],
                ['label'=>__('MILITARY HAVANA'),'value'=>'MILITARY HAVANA'],
                ['label'=>__('MATTE BLACK/GREEN ML INFR'),'value'=>'MATTE BLACK/GREEN ML INFR'],
                ['label'=>__('MT PLATINUM/GREEN IVORY/G'),'value'=>'MT PLATINUM/GREEN IVORY/G'],
                ['label'=>__('MT WINE/GRN FLSH INFRARED'),'value'=>'MT WINE/GRN FLSH INFRARED'],
                ['label'=>__('MT BLACK/GRN ML INFRARED/'),'value'=>'MT BLACK/GRN ML INFRARED/'],
                ['label'=>__('MT GREY/GRN SUPER IVORY/G'),'value'=>'MT GREY/GRN SUPER IVORY/G'],
                ['label'=>__('MT OBSIDIAN/GREY ML BLUE/'),'value'=>'MT OBSIDIAN/GREY ML BLUE/'],
                ['label'=>__('MATTE BLACK/GREY ML INFRA'),'value'=>'MATTE BLACK/GREY ML INFRA'],
                ['label'=>__('MT BLACK/GRN/GREY ML INFR'),'value'=>'MT BLACK/GRN/GREY ML INFR'],
                ['label'=>__('MATTE BLACK/GREY ML BLUE'),'value'=>'MATTE BLACK/GREY ML BLUE'],
                ['label'=>__('MATTE SEAWEED/GREY ML GRE'),'value'=>'MATTE SEAWEED/GREY ML GRE'],
                ['label'=>__('MATTE BLACK/GREY W/ BLACK'),'value'=>'MATTE BLACK/GREY W/ BLACK'],
                ['label'=>__('OBSIDIAN/BROWN SUPER IVOR'),'value'=>'OBSIDIAN/BROWN SUPER IVOR'],
                ['label'=>__('MATTE BLACK/POLARIZED GRE'),'value'=>'MATTE BLACK/POLARIZED GRE'],
                ['label'=>__('BLONDE HAVAN'),'value'=>'BLONDE HAVAN'],
                ['label'=>__('TORTOISE BLUE'),'value'=>'TORTOISE BLUE'],
                ['label'=>__('STRIPED AQUA'),'value'=>'STRIPED AQUA'],
                ['label'=>__('STRIPED CYCLAMEN'),'value'=>'STRIPED CYCLAMEN'],
                ['label'=>__('GREY/HAVANA'),'value'=>'GREY/HAVANA'],
                ['label'=>__('SPOTTED HAVANA'),'value'=>'SPOTTED HAVANA'],
                ['label'=>__('BURNT HAVANA'),'value'=>'BURNT HAVANA'],
                ['label'=>__('GRAPHITE'),'value'=>'GRAPHITE'],
                ['label'=>__('GOLD SAND'),'value'=>'GOLD SAND'],
                ['label'=>__('EMERALD/TEAL GRADIENT'),'value'=>'EMERALD/TEAL GRADIENT'],
                ['label'=>__('FUSCHIA/ORANGE GRADIENT'),'value'=>'FUSCHIA/ORANGE GRADIENT'],
                ['label'=>__('REAL TREE/BROWN'),'value'=>'REAL TREE/BROWN'],
                ['label'=>__('MILKY BLUE'),'value'=>'MILKY BLUE'],
                ['label'=>__('MILKY PURPLE'),'value'=>'MILKY PURPLE'],
                ['label'=>__('BLACK/CARGO'),'value'=>'BLACK/CARGO'],
                ['label'=>__('ALMOND BROWN'),'value'=>'ALMOND BROWN'],
                ['label'=>__('BLACK/MT BLACK/ W/GRY POL'),'value'=>'BLACK/MT BLACK/ W/GRY POL'],
                ['label'=>__('MATTE BLACK W/GREY POLARI'),'value'=>'MATTE BLACK W/GREY POLARI'],
                ['label'=>__('TOBACCO HORN'),'value'=>'TOBACCO HORN'],
                ['label'=>__('BLUE HORN DUSK'),'value'=>'BLUE HORN DUSK'],
                ['label'=>__('GUNMETAL/BLACK'),'value'=>'GUNMETAL/BLACK'],
                ['label'=>__('BROWN/NAVY'),'value'=>'BROWN/NAVY'],
                ['label'=>__('BLACK/BLACK'),'value'=>'BLACK/BLACK'],
                ['label'=>__('MATTE ABYSS'),'value'=>'MATTE ABYSS'],
                ['label'=>__('MULTI TORTOISE'),'value'=>'MULTI TORTOISE'],
                ['label'=>__('MATTE FOREST'),'value'=>'MATTE FOREST'],
                ['label'=>__('MATTE GREY/MELON'),'value'=>'MATTE GREY/MELON'],
                ['label'=>__('MATTE TEAL'),'value'=>'MATTE TEAL'],
                ['label'=>__('DARK SATIN BROWN'),'value'=>'DARK SATIN BROWN'],
                ['label'=>__('SATIN OLIVE'),'value'=>'SATIN OLIVE'],
                ['label'=>__('TORTOISE/GOLDEN'),'value'=>'TORTOISE/GOLDEN'],
                ['label'=>__('GREEN BOULDER TORTOISE/BL'),'value'=>'GREEN BOULDER TORTOISE/BL'],
                ['label'=>__('BLUE GREY/SILVER FLASH'),'value'=>'BLUE GREY/SILVER FLASH'],
                ['label'=>__('GREEN BOULDER TORTOISE/G1'),'value'=>'GREEN BOULDER TORTOISE/G1'],
                ['label'=>__('STRAWBERRY'),'value'=>'STRAWBERRY'],
                ['label'=>__('DUST'),'value'=>'DUST'],
                ['label'=>__('MAPLE RED'),'value'=>'MAPLE RED'],
                ['label'=>__('CRYSTAL CLEAR'),'value'=>'CRYSTAL CLEAR'],
                ['label'=>__('BLACK W/GREY FLBL NIGHT L'),'value'=>'BLACK W/GREY FLBL NIGHT L'],
                ['label'=>__('WO GRY W/GRY ML VIOLET FL'),'value'=>'WO GRY W/GRY ML VIOLET FL'],
                ['label'=>__('MT WOLF GREY W/GRY ML RED'),'value'=>'MT WOLF GREY W/GRY ML RED'],
                ['label'=>__('MT SEAWEED W/GRY FL ML GR'),'value'=>'MT SEAWEED W/GRY FL ML GR'],
                ['label'=>__('MATTE BLACK/SILVER W/GREY'),'value'=>'MATTE BLACK/SILVER W/GREY'],
                ['label'=>__('CHARCOAL LAMINATE'),'value'=>'CHARCOAL LAMINATE'],
                ['label'=>__('BROWN/BLUSH LAMINATE'),'value'=>'BROWN/BLUSH LAMINATE'],
                ['label'=>__('BLUE LAMINATE'),'value'=>'BLUE LAMINATE'],
                ['label'=>__('PURPLE/MAUVE LAMINATE'),'value'=>'PURPLE/MAUVE LAMINATE'],
                ['label'=>__('BROWN / BLUSH LAMINATE'),'value'=>'BROWN / BLUSH LAMINATE'],
                ['label'=>__('GREEN LAMINATE'),'value'=>'GREEN LAMINATE'],
                ['label'=>__('BLUEGREY'),'value'=>'BLUEGREY'],
                ['label'=>__('LILAC HAVANA'),'value'=>'LILAC HAVANA'],
                ['label'=>__('PLUM LAMINATE'),'value'=>'PLUM LAMINATE'],
                ['label'=>__('GRAY GRADIENT'),'value'=>'GRAY GRADIENT'],
                ['label'=>__('GREEN GRADIENT'),'value'=>'GREEN GRADIENT'],
                ['label'=>__('PLUM GRADIENT'),'value'=>'PLUM GRADIENT'],
                ['label'=>__('MILKY CREAM'),'value'=>'MILKY CREAM'],
                ['label'=>__('SHINY LIGHT GUNMETAL'),'value'=>'SHINY LIGHT GUNMETAL'],
                ['label'=>__('CARGO KHAKI/VOLT'),'value'=>'CARGO KHAKI/VOLT'],
                ['label'=>__('ANTHRACITE/PURE PLATINUM'),'value'=>'ANTHRACITE/PURE PLATINUM'],
                ['label'=>__('OBSIDIAN/TIDE POOL BLUE'),'value'=>'OBSIDIAN/TIDE POOL BLUE'],
                ['label'=>__('MIDNIGHT TURQUOISE'),'value'=>'MIDNIGHT TURQUOISE'],
                ['label'=>__('MARBLE BLACK'),'value'=>'MARBLE BLACK'],
                ['label'=>__('SAND/AZURE IRIDESCENT VIS'),'value'=>'SAND/AZURE IRIDESCENT VIS'],
                ['label'=>__('PEACH/ROSE IRIDESCENT VIS'),'value'=>'PEACH/ROSE IRIDESCENT VIS'],
                ['label'=>__('PURPLE/SAND IRIDESCENT VI'),'value'=>'PURPLE/SAND IRIDESCENT VI'],
                ['label'=>__('ROSE/HONEY IRIDESCENT VIS'),'value'=>'ROSE/HONEY IRIDESCENT VIS'],
                ['label'=>__('BLACK/CAMO LION'),'value'=>'BLACK/CAMO LION'],
                ['label'=>__('HAVANA/CAMO LION'),'value'=>'HAVANA/CAMO LION'],
                ['label'=>__('SHINY DARK GUNMETAL/BLACK'),'value'=>'SHINY DARK GUNMETAL/BLACK'],
                ['label'=>__('SHINY GOLD/KHAKI'),'value'=>'SHINY GOLD/KHAKI'],
                ['label'=>__('SHINY GOLD/TORTOISE'),'value'=>'SHINY GOLD/TORTOISE'],
                ['label'=>__('BEIGE / BLACK LAMINATE'),'value'=>'BEIGE / BLACK LAMINATE'],
                ['label'=>__('BLACK/GOLD'),'value'=>'BLACK/GOLD'],
                ['label'=>__('IVORY/GOLD'),'value'=>'IVORY/GOLD'],
                ['label'=>__('OLIVE GREEN/AMBER GOLD'),'value'=>'OLIVE GREEN/AMBER GOLD'],
                ['label'=>__('BORDEAUX/GOLD'),'value'=>'BORDEAUX/GOLD'],
                ['label'=>__('BLACK GREY STONE'),'value'=>'BLACK GREY STONE'],
                ['label'=>__('CRYSTAL QUARTZ'),'value'=>'CRYSTAL QUARTZ'],
                ['label'=>__('BROWN GREIGE STONE'),'value'=>'BROWN GREIGE STONE'],
                ['label'=>__('BROWN QUARTZ'),'value'=>'BROWN QUARTZ'],
                ['label'=>__('MATTE SILVER'),'value'=>'MATTE SILVER'],
                ['label'=>__('SHINY DARK GUNMETAL'),'value'=>'SHINY DARK GUNMETAL'],
                ['label'=>__('SHINY YELLOW GOLD'),'value'=>'SHINY YELLOW GOLD'],
                ['label'=>__('BLACK/G15'),'value'=>'BLACK/G15'],
                ['label'=>__('BLACK ANIMALIER'),'value'=>'BLACK ANIMALIER'],
                ['label'=>__('ANTIQUE ROSE ANIMALIER'),'value'=>'ANTIQUE ROSE ANIMALIER'],
                ['label'=>__('HAVANA/BRONZE'),'value'=>'HAVANA/BRONZE'],
                ['label'=>__('HAVANA/PEACH'),'value'=>'HAVANA/PEACH'],
                ['label'=>__('HAVANA/BROWN'),'value'=>'HAVANA/BROWN'],
                ['label'=>__('HAVANA/GREEN ROSE'),'value'=>'HAVANA/GREEN ROSE'],
                ['label'=>__('AMBER/WHITE MARBLE'),'value'=>'AMBER/WHITE MARBLE'],
                ['label'=>__('GREEN/HAVANA'),'value'=>'GREEN/HAVANA'],
                ['label'=>__('PEARL/CHAMPAGNE'),'value'=>'PEARL/CHAMPAGNE'],
                ['label'=>__('PETROL/HAVANA'),'value'=>'PETROL/HAVANA'],
                ['label'=>__('GOLD/HAVANA/BROWN LENS'),'value'=>'GOLD/HAVANA/BROWN LENS'],
                ['label'=>__('GOLD/HAVANA/GREEN LENS'),'value'=>'GOLD/HAVANA/GREEN LENS'],
                ['label'=>__('GOLD/HAVANA/FLASH BLUE LE'),'value'=>'GOLD/HAVANA/FLASH BLUE LE'],
                ['label'=>__('GOLD/MARBLE/REVO ROSE PEA'),'value'=>'GOLD/MARBLE/REVO ROSE PEA'],
                ['label'=>__('HAVANA TURQUOISE'),'value'=>'HAVANA TURQUOISE'],
                ['label'=>__('HAVANA FUCHSIA'),'value'=>'HAVANA FUCHSIA'],
                ['label'=>__('MEDIUM HORN'),'value'=>'MEDIUM HORN'],
                ['label'=>__('LIGHT HORN'),'value'=>'LIGHT HORN'],
                ['label'=>__('WOLF GREY/RACER BLUE'),'value'=>'WOLF GREY/RACER BLUE'],
                ['label'=>__('MIDNIGHT NAVY/ELECTRO GRE'),'value'=>'MIDNIGHT NAVY/ELECTRO GRE'],
                ['label'=>__('SQUADRON BLUE/MAX ORANGE'),'value'=>'SQUADRON BLUE/MAX ORANGE'],
                ['label'=>__('BLACK/GREY PAISLEY'),'value'=>'BLACK/GREY PAISLEY'],
                ['label'=>__('BROWN/TAUPE PAISLEY'),'value'=>'BROWN/TAUPE PAISLEY'],
                ['label'=>__('BORDEAUX/BORDEAUX PAISLEY'),'value'=>'BORDEAUX/BORDEAUX PAISLEY'],
                ['label'=>__('HAVANA/BLACK'),'value'=>'HAVANA/BLACK'],
                ['label'=>__('PURPLE/GREY HAVANA'),'value'=>'PURPLE/GREY HAVANA'],
                ['label'=>__('PLUMBERRY'),'value'=>'PLUMBERRY'],
                ['label'=>__('BLUE-VIOLET/TEAL'),'value'=>'BLUE-VIOLET/TEAL'],
                ['label'=>__('BROWN PEWTER'),'value'=>'BROWN PEWTER'],
                ['label'=>__('MATTE BLACK RED/GREY'),'value'=>'MATTE BLACK RED/GREY'],
                ['label'=>__('TORTOISE/G15'),'value'=>'TORTOISE/G15'],
                ['label'=>__('ANTHRACITE BLACK/W/GREEN'),'value'=>'ANTHRACITE BLACK/W/GREEN'],
                ['label'=>__('MATTE BLACK/GUNMETAL W/GR'),'value'=>'MATTE BLACK/GUNMETAL W/GR'],
                ['label'=>__('DARK ATOMIC TEAL/BLACK W/'),'value'=>'DARK ATOMIC TEAL/BLACK W/'],
                ['label'=>__('JET BLACK/GREY'),'value'=>'JET BLACK/GREY'],
                ['label'=>__('MATTE BLACK/ORANGE ION'),'value'=>'MATTE BLACK/ORANGE ION'],
                ['label'=>__('MATTE BLACK/SILVER ION'),'value'=>'MATTE BLACK/SILVER ION'],
                ['label'=>__('MATTE BLACK/BLUE ION'),'value'=>'MATTE BLACK/BLUE ION'],
                ['label'=>__('MATTE CRYSTAL SHADOW/BLUE'),'value'=>'MATTE CRYSTAL SHADOW/BLUE'],
                ['label'=>__('BROWN/BROWN PAISLEY'),'value'=>'BROWN/BROWN PAISLEY'],
                ['label'=>__('BLUE/BLUE PAISELY'),'value'=>'BLUE/BLUE PAISELY'],
                ['label'=>__('BROWN/TURTLE DOVE PAISLEY'),'value'=>'BROWN/TURTLE DOVE PAISLEY'],
                ['label'=>__('PETROL/AZURE PAISLEY'),'value'=>'PETROL/AZURE PAISLEY'],
                ['label'=>__('VIOLET PAISLEY'),'value'=>'VIOLET PAISLEY'],
                ['label'=>__('STRIPED BLUE GREY'),'value'=>'STRIPED BLUE GREY'],
                ['label'=>__('MATTE BLACK/PEW W/AMBER L'),'value'=>'MATTE BLACK/PEW W/AMBER L'],
                ['label'=>__('ANTHRA/GUNMETAL W/GRN LEN'),'value'=>'ANTHRA/GUNMETAL W/GRN LEN'],
                ['label'=>__('ANTHRACITE/COOL GREY W/GR'),'value'=>'ANTHRACITE/COOL GREY W/GR'],
                ['label'=>__('WOLF GREY/COOL GREY W/TEA'),'value'=>'WOLF GREY/COOL GREY W/TEA'],
                ['label'=>__('MT BLACK/DK AT TL W/AM LE'),'value'=>'MT BLACK/DK AT TL W/AM LE'],
                ['label'=>__('CAR KHAKI/MED OLI W/GRN L'),'value'=>'CAR KHAKI/MED OLI W/GRN L'],
                ['label'=>__('MT DK TM RD/BLACK W/DK GR'),'value'=>'MT DK TM RD/BLACK W/DK GR'],
                ['label'=>__('MATTE DK TEAL/GREY GREEN'),'value'=>'MATTE DK TEAL/GREY GREEN'],
                ['label'=>__('MATTE ANTHR BLK W/DK GRY'),'value'=>'MATTE ANTHR BLK W/DK GRY'],
                ['label'=>__('BLACK W/GREY SILVER FLASH'),'value'=>'BLACK W/GREY SILVER FLASH'],
                ['label'=>__('S SO WH W/GREY W SIL FL L'),'value'=>'S SO WH W/GREY W SIL FL L'],
                ['label'=>__('MATTE S VO W/GRY W/ SIL F'),'value'=>'MATTE S VO W/GRY W/ SIL F'],
                ['label'=>__('MATTE SOLAR RED/GREY PINK'),'value'=>'MATTE SOLAR RED/GREY PINK'],
                ['label'=>__('MATTE GREY/GREY ML INFRAR'),'value'=>'MATTE GREY/GREY ML INFRAR'],
                ['label'=>__('MT BLACK W/GREY STA GRN F'),'value'=>'MT BLACK W/GREY STA GRN F'],
                ['label'=>__('SHINY BLACK/GREY'),'value'=>'SHINY BLACK/GREY'],
                ['label'=>__('WOODGRAIN/COPPER ION'),'value'=>'WOODGRAIN/COPPER ION'],
                ['label'=>__('TORTOISE PURPLE'),'value'=>'TORTOISE PURPLE'],
                ['label'=>__('MATTE MAGNET GREY/SILVER'),'value'=>'MATTE MAGNET GREY/SILVER'],
                ['label'=>__('MATTE SHADOW/COPPER'),'value'=>'MATTE SHADOW/COPPER'],
                ['label'=>__('STRIPED BURNT BLUE'),'value'=>'STRIPED BURNT BLUE'],
                ['label'=>__('TORTOISE TURQUOISE'),'value'=>'TORTOISE TURQUOISE'],
                ['label'=>__('TORTOISE ROSE'),'value'=>'TORTOISE ROSE'],
                ['label'=>__('SPOTTED BLUE'),'value'=>'SPOTTED BLUE'],
                ['label'=>__('SPOTTED WINE'),'value'=>'SPOTTED WINE'],
                ['label'=>__('SHINY TORTOISE/G15'),'value'=>'SHINY TORTOISE/G15'],
                ['label'=>__('MATTE SOFT ICE/PEARL ION'),'value'=>'MATTE SOFT ICE/PEARL ION'],
                ['label'=>__('CRYSTAL SHADOW/PEARL ION'),'value'=>'CRYSTAL SHADOW/PEARL ION'],
                ['label'=>__('CHARCOAL HORN/MARBLE'),'value'=>'CHARCOAL HORN/MARBLE'],
                ['label'=>__('BROWN HORN / MARBLE LAMIN'),'value'=>'BROWN HORN / MARBLE LAMIN'],
                ['label'=>__('NAVY HORN / MARBLE LAMINA'),'value'=>'NAVY HORN / MARBLE LAMINA'],
                ['label'=>__('OPALINE BLUE'),'value'=>'OPALINE BLUE'],
                ['label'=>__('WINE/OCHRE'),'value'=>'WINE/OCHRE'],
                ['label'=>__('TURTLEDOVE/BRICK'),'value'=>'TURTLEDOVE/BRICK'],
                ['label'=>__('BURGUNDY/GREY'),'value'=>'BURGUNDY/GREY'],
                ['label'=>__('HAVANA MULTICOLOR'),'value'=>'HAVANA MULTICOLOR'],
                ['label'=>__('BLU TORTOISE'),'value'=>'BLU TORTOISE'],
                ['label'=>__('GOLD BOURBON'),'value'=>'GOLD BOURBON'],
                ['label'=>__('GOLD/DARK BROWN'),'value'=>'GOLD/DARK BROWN'],
                ['label'=>__('ROSE GOLD/NUDE'),'value'=>'ROSE GOLD/NUDE'],
                ['label'=>__('ROSE GOLD/BURGUNDY'),'value'=>'ROSE GOLD/BURGUNDY'],
                ['label'=>__('BRIGHT GOLD'),'value'=>'BRIGHT GOLD'],
                ['label'=>__('HAVANA/BURGUNDY'),'value'=>'HAVANA/BURGUNDY'],
                ['label'=>__('MILITARY/OCHRE'),'value'=>'MILITARY/OCHRE'],
                ['label'=>__('PETROL/BERRY'),'value'=>'PETROL/BERRY'],
                ['label'=>__('RUBY/PETROL'),'value'=>'RUBY/PETROL'],
                ['label'=>__('TURTLEDOVE/VIOLET'),'value'=>'TURTLEDOVE/VIOLET'],
                ['label'=>__('HAVANA/GREEN'),'value'=>'HAVANA/GREEN'],
                ['label'=>__('VIOLET/BERRY'),'value'=>'VIOLET/BERRY'],
                ['label'=>__('CHERRY/RED'),'value'=>'CHERRY/RED'],
                ['label'=>__('PEACH/BRICK'),'value'=>'PEACH/BRICK'],
                ['label'=>__('AMBER/OCHRE'),'value'=>'AMBER/OCHRE'],
                ['label'=>__('BEIGE TORTOISE'),'value'=>'BEIGE TORTOISE'],
                ['label'=>__('GEOMETRIC SAND'),'value'=>'GEOMETRIC SAND'],
                ['label'=>__('GEOMETRIC FOREST'),'value'=>'GEOMETRIC FOREST'],
                ['label'=>__('GEOMETRIC BLUE'),'value'=>'GEOMETRIC BLUE'],
                ['label'=>__('PINK TORTOISE'),'value'=>'PINK TORTOISE'],
                ['label'=>__('STRIPED SKY'),'value'=>'STRIPED SKY'],
                ['label'=>__('STRIPED BOUQUET'),'value'=>'STRIPED BOUQUET'],
                ['label'=>__('BROWN/PINK TORTOISE'),'value'=>'BROWN/PINK TORTOISE'],
                ['label'=>__('IRIDESCENT BLUE TORTOISE'),'value'=>'IRIDESCENT BLUE TORTOISE'],
                ['label'=>__('IRIDESCENT PURPLE TORTOIS'),'value'=>'IRIDESCENT PURPLE TORTOIS'],
                ['label'=>__('GOLD/BEIGE'),'value'=>'GOLD/BEIGE'],
                ['label'=>__('YELLOW'),'value'=>'YELLOW'],
                ['label'=>__('BLK/SIL/GREY POLARIZED LE'),'value'=>'BLK/SIL/GREY POLARIZED LE'],
                ['label'=>__('CAMEL'),'value'=>'CAMEL'],
                ['label'=>__('MATTE BLACK/DARK GREY LEN'),'value'=>'MATTE BLACK/DARK GREY LEN'],
                ['label'=>__('MATTE TORTOISE/DARK BROWN'),'value'=>'MATTE TORTOISE/DARK BROWN'],
                ['label'=>__('MATTE RUNWAY GOLD'),'value'=>'MATTE RUNWAY GOLD'],
                ['label'=>__('MIDNIGHT TEAL'),'value'=>'MIDNIGHT TEAL'],
                ['label'=>__('OPALINE WINE'),'value'=>'OPALINE WINE'],
                ['label'=>__('OPALINE PEACH'),'value'=>'OPALINE PEACH'],
                ['label'=>__('GOLD/TORTOISE'),'value'=>'GOLD/TORTOISE'],
                ['label'=>__('ROSE GOLD/BORDEAUX'),'value'=>'ROSE GOLD/BORDEAUX'],
                ['label'=>__('CRYSTAL PEACH'),'value'=>'CRYSTAL PEACH'],
                ['label'=>__('CRYSTAL KHAKI'),'value'=>'CRYSTAL KHAKI'],
                ['label'=>__('CRYSTAL GREY'),'value'=>'CRYSTAL GREY'],
                ['label'=>__('MATTE CHAPARRAL/BLUE'),'value'=>'MATTE CHAPARRAL/BLUE'],
                ['label'=>__('ALPINE TUNDRA/ORANGE'),'value'=>'ALPINE TUNDRA/ORANGE'],
                ['label'=>__('MATTE TORTOISE/AMBER'),'value'=>'MATTE TORTOISE/AMBER'],
                ['label'=>__('MATTE BLACK/ORANGE'),'value'=>'MATTE BLACK/ORANGE'],
                ['label'=>__('MATTE COLLEGIATE NAVY/BLU'),'value'=>'MATTE COLLEGIATE NAVY/BLU'],
                ['label'=>__('MATTE GRILL/RED'),'value'=>'MATTE GRILL/RED'],
                ['label'=>__('MATTE GRILL/ SMOKE'),'value'=>'MATTE GRILL/ SMOKE'],
                ['label'=>__('MATTE TORTOISE/AMBER FLAS'),'value'=>'MATTE TORTOISE/AMBER FLAS'],
                ['label'=>__('MATTE EVER BLUE/ BLUE'),'value'=>'MATTE EVER BLUE/ BLUE'],
                ['label'=>__('MATTE PIXEL/GREEN'),'value'=>'MATTE PIXEL/GREEN'],
                ['label'=>__('MATTE SAGE/SILVER'),'value'=>'MATTE SAGE/SILVER'],
                ['label'=>__('GREY DUST'),'value'=>'GREY DUST'],
                ['label'=>__('MATTE PIXEL/SILVER'),'value'=>'MATTE PIXEL/SILVER'],
                ['label'=>__('SHINY BURGUNDY'),'value'=>'SHINY BURGUNDY'],
                ['label'=>__('SHINY CRYSTAL'),'value'=>'SHINY CRYSTAL'],
                ['label'=>__('CRYSTAL BORDEAUX'),'value'=>'CRYSTAL BORDEAUX'],
                ['label'=>__('SATIN WALNUT/AMBER'),'value'=>'SATIN WALNUT/AMBER'],
                ['label'=>__('SATIN BLACK/BLUE'),'value'=>'SATIN BLACK/BLUE'],
                ['label'=>__('SATIN GUNMETAL/ORANGE'),'value'=>'SATIN GUNMETAL/ORANGE'],
                ['label'=>__('SATIN WALNUT/FIRE'),'value'=>'SATIN WALNUT/FIRE'],
                ['label'=>__('MATTE EVER BLUE/SILVER'),'value'=>'MATTE EVER BLUE/SILVER'],
                ['label'=>__('ARMY GREEN'),'value'=>'ARMY GREEN'],
                ['label'=>__('MATTE ARMY GREEN'),'value'=>'MATTE ARMY GREEN'],
                ['label'=>__('SHINY GREEN'),'value'=>'SHINY GREEN'],
                ['label'=>__('SHINY RED FUCHSIA'),'value'=>'SHINY RED FUCHSIA'],
                ['label'=>__('MATTE BLACK/SCOPH'),'value'=>'MATTE BLACK/SCOPH'],
                ['label'=>__('MATTE BLACK/JAIME'),'value'=>'MATTE BLACK/JAIME'],
                ['label'=>__('MATTE BLACK/IUNA'),'value'=>'MATTE BLACK/IUNA'],
                ['label'=>__('MATTE BLACK/BRIAN'),'value'=>'MATTE BLACK/BRIAN'],
                ['label'=>__('MATTE BLACK/ROSE GOLD ION'),'value'=>'MATTE BLACK/ROSE GOLD ION'],
                ['label'=>__('SEQUIOA'),'value'=>'SEQUIOA'],
                ['label'=>__('MATTE ATOMIC TEAL'),'value'=>'MATTE ATOMIC TEAL'],
                ['label'=>__('MATTE PORT WINE'),'value'=>'MATTE PORT WINE'],
                ['label'=>__('CLEAR'),'value'=>'CLEAR'],
                ['label'=>__('MATTE TEAM RED'),'value'=>'MATTE TEAM RED'],
                ['label'=>__('HONEY WALNUT'),'value'=>'HONEY WALNUT'],
                ['label'=>__('AGAVE BLUE'),'value'=>'AGAVE BLUE'],
                ['label'=>__('SMOKE CRYSTAL/MULTI'),'value'=>'SMOKE CRYSTAL/MULTI'],
                ['label'=>__('MATTE CRYSTAL/BLACK'),'value'=>'MATTE CRYSTAL/BLACK'],
                ['label'=>__('SMOKE CRYSTAL/BLACK'),'value'=>'SMOKE CRYSTAL/BLACK'],
                ['label'=>__('CRYSTAL/BLACK'),'value'=>'CRYSTAL/BLACK'],
                ['label'=>__('MATTE SHADOW/RESIN'),'value'=>'MATTE SHADOW/RESIN'],
                ['label'=>__('BLACK/RESIN'),'value'=>'BLACK/RESIN'],
                ['label'=>__('SATIN GUNMETAL/BARK'),'value'=>'SATIN GUNMETAL/BARK'],
                ['label'=>__('SATIN GUNMETAL/TORTOISE'),'value'=>'SATIN GUNMETAL/TORTOISE'],
                ['label'=>__('MATTE BLACK/CRYSTAL'),'value'=>'MATTE BLACK/CRYSTAL'],
                ['label'=>__('GREY HORN/BOULDER'),'value'=>'GREY HORN/BOULDER'],
                ['label'=>__('SHINY TORTOISE/OLIVE'),'value'=>'SHINY TORTOISE/OLIVE'],
                ['label'=>__('MATTE NAVY/SPACE BLUE'),'value'=>'MATTE NAVY/SPACE BLUE'],
                ['label'=>__('MATTE ABYSS BLUE'),'value'=>'MATTE ABYSS BLUE'],
                ['label'=>__('SATIN BLACK/RESIN'),'value'=>'SATIN BLACK/RESIN'],
                ['label'=>__('SATIN NAVY AGAVE'),'value'=>'SATIN NAVY AGAVE'],
                ['label'=>__('SATIN EGGPLANT'),'value'=>'SATIN EGGPLANT'],
                ['label'=>__('SATIN BLACK/GUNMETAL'),'value'=>'SATIN BLACK/GUNMETAL'],
                ['label'=>__('SATIN BLACK/TEAL'),'value'=>'SATIN BLACK/TEAL'],
                ['label'=>__('SATIN GUNMETAL/GREY'),'value'=>'SATIN GUNMETAL/GREY'],
                ['label'=>__('SATIN WALNUT/GUNMETAL'),'value'=>'SATIN WALNUT/GUNMETAL'],
                ['label'=>__('SATIN ACAI/POMEGRANATE'),'value'=>'SATIN ACAI/POMEGRANATE'],
                ['label'=>__('SATIN NAVY/GUNMETAL'),'value'=>'SATIN NAVY/GUNMETAL'],
                ['label'=>__('SHINY NICKEL'),'value'=>'SHINY NICKEL'],
                ['label'=>__('SHINY LIGHT GOLD'),'value'=>'SHINY LIGHT GOLD'],
                ['label'=>__('WHITE/CRYSTAL'),'value'=>'WHITE/CRYSTAL'],
                ['label'=>__('CREAM TORTOISE/BLACK HORN'),'value'=>'CREAM TORTOISE/BLACK HORN'],
                ['label'=>__('BROWN TORTOISE/CRYSTAL BR'),'value'=>'BROWN TORTOISE/CRYSTAL BR'],
                ['label'=>__('ROSE TORTOISE/CRYSTAL ROS'),'value'=>'ROSE TORTOISE/CRYSTAL ROS'],
                ['label'=>__('BROWN HORN/SOFT TORTOISE'),'value'=>'BROWN HORN/SOFT TORTOISE'],
                ['label'=>__('OLIVE HORN/TORTOISE'),'value'=>'OLIVE HORN/TORTOISE'],
                ['label'=>__('BLACK/CREAM HORN'),'value'=>'BLACK/CREAM HORN'],
                ['label'=>__('SOFT TORTOISE/CREAM TORTO'),'value'=>'SOFT TORTOISE/CREAM TORTO'],
                ['label'=>__('BLUE/LIGHT BLUE HORN'),'value'=>'BLUE/LIGHT BLUE HORN'],
                ['label'=>__('BLUSH/BLUSH HORN'),'value'=>'BLUSH/BLUSH HORN'],
                ['label'=>__('DARK TORTOISE/HORN/CRYSTA'),'value'=>'DARK TORTOISE/HORN/CRYSTA'],
                ['label'=>__('GREEN/TOKYO TORTOISE'),'value'=>'GREEN/TOKYO TORTOISE'],
                ['label'=>__('SHINY TITANIUM'),'value'=>'SHINY TITANIUM'],
                ['label'=>__('SATIN ROSE GOLD'),'value'=>'SATIN ROSE GOLD'],
                ['label'=>__('TEAL TORTOISE/CRYSTAL TEA'),'value'=>'TEAL TORTOISE/CRYSTAL TEA'],
                ['label'=>__('LIGHT BROWN HORN'),'value'=>'LIGHT BROWN HORN'],
                ['label'=>__('LIGHT BLUE HORN'),'value'=>'LIGHT BLUE HORN'],
                ['label'=>__('BLUSH HORN'),'value'=>'BLUSH HORN'],
                ['label'=>__('MATTE WOLF GREY/GREY SILV'),'value'=>'MATTE WOLF GREY/GREY SILV'],
                ['label'=>__('BLACK/GREY SILVER FLASH'),'value'=>'BLACK/GREY SILVER FLASH'],
                ['label'=>__('MATTE VOLT/GREY SILVER FL'),'value'=>'MATTE VOLT/GREY SILVER FL'],
                ['label'=>__('STRIPED TRNSPARENT BLUE P'),'value'=>'STRIPED TRNSPARENT BLUE P'],
                ['label'=>__('STRIPED TRANSPARENT RED'),'value'=>'STRIPED TRANSPARENT RED'],
                ['label'=>__('BLACK/CLEAR LENS'),'value'=>'BLACK/CLEAR LENS'],
                ['label'=>__('SOLID BLACK'),'value'=>'SOLID BLACK'],
                ['label'=>__('CRYSTAL NUDE'),'value'=>'CRYSTAL NUDE'],
                ['label'=>__('COOL GREY/GREY ML INFRARE'),'value'=>'COOL GREY/GREY ML INFRARE'],
                ['label'=>__('MATTE BLUE/GREY BLUE FLAS'),'value'=>'MATTE BLUE/GREY BLUE FLAS'],
                ['label'=>__('MATTE BLACK/MATTE RACER B'),'value'=>'MATTE BLACK/MATTE RACER B'],
                ['label'=>__('BRUSHED GUNMETAL/MATTE AN'),'value'=>'BRUSHED GUNMETAL/MATTE AN'],
                ['label'=>__('GUNMETAL/SOLAR RED'),'value'=>'GUNMETAL/SOLAR RED'],
                ['label'=>__('MATTE CRYSTAL GREY'),'value'=>'MATTE CRYSTAL GREY'],
                ['label'=>__('MATTE TORTOISE/HONEY'),'value'=>'MATTE TORTOISE/HONEY'],
                ['label'=>__('WALNUT/VOLT'),'value'=>'WALNUT/VOLT'],
                ['label'=>__('GUNMETAL/WOLF GREY'),'value'=>'GUNMETAL/WOLF GREY'],
                ['label'=>__('SATIN GUNMETAL/BLACK'),'value'=>'SATIN GUNMETAL/BLACK'],
                ['label'=>__('BLACK/CRYSTAL CLEAR GUNME'),'value'=>'BLACK/CRYSTAL CLEAR GUNME'],
                ['label'=>__('BLACK/TOTAL CRIMSON'),'value'=>'BLACK/TOTAL CRIMSON'],
                ['label'=>__('MATTE TORTOISE/ANTHRACITE'),'value'=>'MATTE TORTOISE/ANTHRACITE'],
                ['label'=>__('MATTE SPACE BLUE'),'value'=>'MATTE SPACE BLUE'],
                ['label'=>__('BLACK/NEPTUNE GREEN'),'value'=>'BLACK/NEPTUNE GREEN'],
                ['label'=>__('MATTE TORTOISE/PORT WINE'),'value'=>'MATTE TORTOISE/PORT WINE'],
                ['label'=>__('MATTE WOLF GREY/GREEN'),'value'=>'MATTE WOLF GREY/GREEN'],
                ['label'=>__('GOLD/GREEN LENS'),'value'=>'GOLD/GREEN LENS'],
                ['label'=>__('GOLD/BROWN LENS'),'value'=>'GOLD/BROWN LENS'],
                ['label'=>__('GOLD/PETROL LENS'),'value'=>'GOLD/PETROL LENS'],
                ['label'=>__('GOLD/LIGHT WINE LENS'),'value'=>'GOLD/LIGHT WINE LENS'],
                ['label'=>__('GREIGE'),'value'=>'GREIGE'],
                ['label'=>__('AVIO'),'value'=>'AVIO'],
                ['label'=>__('GOLD/BLONDE HAVANA'),'value'=>'GOLD/BLONDE HAVANA'],
                ['label'=>__('GOLD/IVORY'),'value'=>'GOLD/IVORY'],
                ['label'=>__('GOLD/MARBLE BEIGE'),'value'=>'GOLD/MARBLE BEIGE'],
                ['label'=>__('GOLD/PEACH LENS'),'value'=>'GOLD/PEACH LENS'],
                ['label'=>__('GOLD/RAINBOW LENS'),'value'=>'GOLD/RAINBOW LENS'],
                ['label'=>__('GOLD/FLASH BROWN LENS'),'value'=>'GOLD/FLASH BROWN LENS'],
                ['label'=>__('SATIN GRAPHITE'),'value'=>'SATIN GRAPHITE'],
                ['label'=>__('HAVANA BLACK'),'value'=>'HAVANA BLACK'],
                ['label'=>__('TORTOISE/TURTLEDOVE VISET'),'value'=>'TORTOISE/TURTLEDOVE VISET'],
                ['label'=>__('BLUE/BLUE VISETOS'),'value'=>'BLUE/BLUE VISETOS'],
                ['label'=>__('HAVANA/BURNT VISETOS'),'value'=>'HAVANA/BURNT VISETOS'],
                ['label'=>__('MILKY GREEN'),'value'=>'MILKY GREEN'],
                ['label'=>__('MILKY PLUM'),'value'=>'MILKY PLUM'],
                ['label'=>__('STRIPED GREY/BLUE'),'value'=>'STRIPED GREY/BLUE'],
                ['label'=>__('STRIPED BROWN/YELLOW'),'value'=>'STRIPED BROWN/YELLOW'],
                ['label'=>__('STRIPED SMOKE ROSE'),'value'=>'STRIPED SMOKE ROSE'],
                ['label'=>__('STRIPED PURPLE BROWN'),'value'=>'STRIPED PURPLE BROWN'],
                ['label'=>__('BLACK/BROWN TORTOISE'),'value'=>'BLACK/BROWN TORTOISE'],
                ['label'=>__('BLUE/GREY TORTOISE'),'value'=>'BLUE/GREY TORTOISE'],
                ['label'=>__('DARK BROWN HORN'),'value'=>'DARK BROWN HORN'],
                ['label'=>__('BLACK/IGUCHI'),'value'=>'BLACK/IGUCHI'],
                ['label'=>__('BLACK/SCOPH'),'value'=>'BLACK/SCOPH'],
                ['label'=>__('BLACK/JAMIE LYNN'),'value'=>'BLACK/JAMIE LYNN'],
                ['label'=>__('BLACK/IUNA TANTI'),'value'=>'BLACK/IUNA TANTI'],
                ['label'=>__('KHAKI MARBLE'),'value'=>'KHAKI MARBLE'],
                ['label'=>__('SATIN GUNMETAL/DARK GREY'),'value'=>'SATIN GUNMETAL/DARK GREY'],
                ['label'=>__('SATIN NAVY/WOLF GREY'),'value'=>'SATIN NAVY/WOLF GREY'],
                ['label'=>__('AUTUMN'),'value'=>'AUTUMN'],
                ['label'=>__('MINT GREEN'),'value'=>'MINT GREEN'],
                ['label'=>__('MATTE PALLADIUM'),'value'=>'MATTE PALLADIUM'],
                ['label'=>__('MATTE DARK BLUE'),'value'=>'MATTE DARK BLUE'],
                ['label'=>__('MATTE ELECTRIC BLUE'),'value'=>'MATTE ELECTRIC BLUE'],
                ['label'=>__('CRYSTAL CHAMPAGNE'),'value'=>'CRYSTAL CHAMPAGNE'],
                ['label'=>__('MATTE GREEN/GREY'),'value'=>'MATTE GREEN/GREY'],
                ['label'=>__('MATTE BLUE/GREY'),'value'=>'MATTE BLUE/GREY'],
                ['label'=>__('MATTE RED/GREY'),'value'=>'MATTE RED/GREY'],
                ['label'=>__('JET /G15'),'value'=>'JET /G15'],
                ['label'=>__('CHERRY'),'value'=>'CHERRY'],
                ['label'=>__('BLACK/POLAR'),'value'=>'BLACK/POLAR'],
                ['label'=>__('MATTE BLACK/GOLF FE'),'value'=>'MATTE BLACK/GOLF FE'],
                ['label'=>__('MT THUNDER BLUE/GOLF MLKY'),'value'=>'MT THUNDER BLUE/GOLF MLKY'],
                ['label'=>__('PEWTER/GOLF MILKY BLUE'),'value'=>'PEWTER/GOLF MILKY BLUE'],
                ['label'=>__('WALNUT/GOLF FLASH ELECTRI'),'value'=>'WALNUT/GOLF FLASH ELECTRI'],
                ['label'=>__('TOTAL CRIMSON/GOLF FE'),'value'=>'TOTAL CRIMSON/GOLF FE'],
                ['label'=>__('COPPER/BRN BRONZE FLSH GO'),'value'=>'COPPER/BRN BRONZE FLSH GO'],
                ['label'=>__('SATIN PEWTER/TERRAIN TINT'),'value'=>'SATIN PEWTER/TERRAIN TINT'],
                ['label'=>__('SATIN BLACK/GREY'),'value'=>'SATIN BLACK/GREY'],
                ['label'=>__('MT DRK GRY/COURSE TINT/GR'),'value'=>'MT DRK GRY/COURSE TINT/GR'],
                ['label'=>__('MT GRY/TERRAIN TINT/GRY S'),'value'=>'MT GRY/TERRAIN TINT/GRY S'],
                ['label'=>__('MATTE DARK GREY/GOLF FE'),'value'=>'MATTE DARK GREY/GOLF FE'],
                ['label'=>__('MATTE WOLF GREY/TERRAIN T'),'value'=>'MATTE WOLF GREY/TERRAIN T'],
                ['label'=>__('COGNAC'),'value'=>'COGNAC'],
                ['label'=>__('BLUSH/BROWN GRADIENT'),'value'=>'BLUSH/BROWN GRADIENT'],
                ['label'=>__('OLIVE/BLUE GRADIENT'),'value'=>'OLIVE/BLUE GRADIENT'],
                ['label'=>__('MILKY BROWN'),'value'=>'MILKY BROWN'],
                ['label'=>__('MILKY BURGUNDY'),'value'=>'MILKY BURGUNDY'],
                ['label'=>__('NICKEL / GOLD'),'value'=>'NICKEL / GOLD'],
                ['label'=>__('SMOKE LAMINATE'),'value'=>'SMOKE LAMINATE'],
                ['label'=>__('BROWN LAMINATE'),'value'=>'BROWN LAMINATE'],
                ['label'=>__('NAVY PEACH LAMINATE'),'value'=>'NAVY PEACH LAMINATE'],
                ['label'=>__('RED BLUE LAMINATE'),'value'=>'RED BLUE LAMINATE'],
                ['label'=>__('BLUSH LAMINATE'),'value'=>'BLUSH LAMINATE'],
                ['label'=>__('MATTE GOLD'),'value'=>'MATTE GOLD'],
                ['label'=>__('MARBLE GREEN'),'value'=>'MARBLE GREEN'],
                ['label'=>__('MARBLE ROSE'),'value'=>'MARBLE ROSE'],
                ['label'=>__('BLACK/PURPLE PAISLEY'),'value'=>'BLACK/PURPLE PAISLEY'],
                ['label'=>__('PINK PAISLEY'),'value'=>'PINK PAISLEY'],
                ['label'=>__('YELLOW/BROWN PAISLEY'),'value'=>'YELLOW/BROWN PAISLEY'],
                ['label'=>__('GREY/PETROL PAISLEY'),'value'=>'GREY/PETROL PAISLEY'],
                ['label'=>__('LILAC PAISLEY'),'value'=>'LILAC PAISLEY'],
                ['label'=>__('ORANGE/BURNT PAISLEY'),'value'=>'ORANGE/BURNT PAISLEY'],
                ['label'=>__('GREEN HAVANA'),'value'=>'GREEN HAVANA'],
                ['label'=>__('HAVANA BURGUNDY'),'value'=>'HAVANA BURGUNDY'],
                ['label'=>__('HAVANA PURPLE'),'value'=>'HAVANA PURPLE'],
                ['label'=>__('BROWN/NUDE'),'value'=>'BROWN/NUDE'],
                ['label'=>__('MARBLE ROUGE'),'value'=>'MARBLE ROUGE'],
                ['label'=>__('MARBLE BROWN AZURE'),'value'=>'MARBLE BROWN AZURE'],
                ['label'=>__('MARBLE BROWN RED'),'value'=>'MARBLE BROWN RED'],
                ['label'=>__('GOLD ROSE'),'value'=>'GOLD ROSE'],
                ['label'=>__('GOLD BLUE'),'value'=>'GOLD BLUE'],
                ['label'=>__('MARBLE PURPLE'),'value'=>'MARBLE PURPLE'],
                ['label'=>__('MARBLE BROWN-AZURE'),'value'=>'MARBLE BROWN-AZURE'],
                ['label'=>__('MARBLE BROWN-PURPLE'),'value'=>'MARBLE BROWN-PURPLE'],
                ['label'=>__('WALNUT/AMBER'),'value'=>'WALNUT/AMBER'],
                ['label'=>__('SATIN NAVY/SILVER'),'value'=>'SATIN NAVY/SILVER'],
                ['label'=>__('GREY/GREY'),'value'=>'GREY/GREY'],
                ['label'=>__('KHAKI/TURTLE DOVE'),'value'=>'KHAKI/TURTLE DOVE'],
                ['label'=>__('PEACH/PEACH'),'value'=>'PEACH/PEACH'],
                ['label'=>__('ROSE/ROSE'),'value'=>'ROSE/ROSE'],
                ['label'=>__('MARBLE BLACK ROSE'),'value'=>'MARBLE BLACK ROSE'],
                ['label'=>__('STRIPED AQUA CYCLAMEN'),'value'=>'STRIPED AQUA CYCLAMEN'],
                ['label'=>__('STEELGREY'),'value'=>'STEELGREY'],
                ['label'=>__('BLUE WITH AZURE PHOSPHO T'),'value'=>'BLUE WITH AZURE PHOSPHO T'],
                ['label'=>__('SHINY GUMETAL/MAPLE'),'value'=>'SHINY GUMETAL/MAPLE'],
                ['label'=>__('SHINY GOLD/BIRCH'),'value'=>'SHINY GOLD/BIRCH'],
                ['label'=>__('SHINY GOLD/PUMPKIN'),'value'=>'SHINY GOLD/PUMPKIN'],
                ['label'=>__('BLACK/MARBLE'),'value'=>'BLACK/MARBLE'],
                ['label'=>__('MINT FLOWERS'),'value'=>'MINT FLOWERS'],
                ['label'=>__('LILAC FLOWERS'),'value'=>'LILAC FLOWERS'],
                ['label'=>__('ROSE FLOWERS'),'value'=>'ROSE FLOWERS'],
                ['label'=>__('GREY/PEACH'),'value'=>'GREY/PEACH'],
                ['label'=>__('CHOCOLATE/SKYBLUE'),'value'=>'CHOCOLATE/SKYBLUE'],
                ['label'=>__('GREEN/ROSE'),'value'=>'GREEN/ROSE'],
                ['label'=>__('CHERRY/PEACH'),'value'=>'CHERRY/PEACH'],
                ['label'=>__('SHINY GOLD/MINT'),'value'=>'SHINY GOLD/MINT'],
                ['label'=>__('SHINY ROSE GOLD/BORDEAUX'),'value'=>'SHINY ROSE GOLD/BORDEAUX'],
                ['label'=>__('BLACK/RUTHENIUM'),'value'=>'BLACK/RUTHENIUM'],
                ['label'=>__('HAVANA/DARK RUTHENIUM'),'value'=>'HAVANA/DARK RUTHENIUM'],
                ['label'=>__('HAVANA/GOLD'),'value'=>'HAVANA/GOLD'],
                ['label'=>__('SHINY ROSE GOLD/NUDE'),'value'=>'SHINY ROSE GOLD/NUDE'],
                ['label'=>__('SHINY GOLD/BURGUNDY'),'value'=>'SHINY GOLD/BURGUNDY'],
                ['label'=>__('HAVANA/KHAKI'),'value'=>'HAVANA/KHAKI'],
                ['label'=>__('STRIPED MILITARY GREEN'),'value'=>'STRIPED MILITARY GREEN'],
                ['label'=>__('BLACK/THUNDER BLUE'),'value'=>'BLACK/THUNDER BLUE'],
                ['label'=>__('BLACK/WOLF GREY'),'value'=>'BLACK/WOLF GREY'],
                ['label'=>__('BLUE BARK/GREY'),'value'=>'BLUE BARK/GREY'],
                ['label'=>__('MATTE BLUE TORTOISE/SMOKE'),'value'=>'MATTE BLUE TORTOISE/SMOKE'],
                ['label'=>__('BLACK BARK/SMOKE'),'value'=>'BLACK BARK/SMOKE'],
                ['label'=>__('MATTE TOKYO TORTOISE/G15'),'value'=>'MATTE TOKYO TORTOISE/G15'],
                ['label'=>__('SATIN BLACK/CRYSTAL'),'value'=>'SATIN BLACK/CRYSTAL'],
                ['label'=>__('SATIN EARTH/HONEY'),'value'=>'SATIN EARTH/HONEY'],
                ['label'=>__('SATIN NAVY/SPACE BLUE'),'value'=>'SATIN NAVY/SPACE BLUE'],
                ['label'=>__('BLUE HORN DUSK/SMOKE'),'value'=>'BLUE HORN DUSK/SMOKE'],
                ['label'=>__('TOBACCO HORN/SMOKE'),'value'=>'TOBACCO HORN/SMOKE'],
                ['label'=>__('TOKYO TORTOISE/G15'),'value'=>'TOKYO TORTOISE/G15'],
                ['label'=>__('BLACK/BURGUNDY LAMINATE'),'value'=>'BLACK/BURGUNDY LAMINATE'],
                ['label'=>__('DARK ROSE'),'value'=>'DARK ROSE'],
                ['label'=>__('BLUSH/BURUGNDY LAMINATE'),'value'=>'BLUSH/BURUGNDY LAMINATE'],
                ['label'=>__('VINTAGE TORTOISE'),'value'=>'VINTAGE TORTOISE'],
                ['label'=>__('LIGHT GREY'),'value'=>'LIGHT GREY'],
                ['label'=>__('BLACK/WHITE/BLUE'),'value'=>'BLACK/WHITE/BLUE'],
                ['label'=>__('BLUE STORM'),'value'=>'BLUE STORM'],
                ['label'=>__('PEWTER'),'value'=>'PEWTER'],
                ['label'=>__('SATIN WALNUT/CARGO KHAKI'),'value'=>'SATIN WALNUT/CARGO KHAKI'],
                ['label'=>__('BLACK/CHARCOAL TORTOISE'),'value'=>'BLACK/CHARCOAL TORTOISE'],
                ['label'=>__('MATTE BLACK/CHARCOAL TORT'),'value'=>'MATTE BLACK/CHARCOAL TORT'],
                ['label'=>__('SATIN TITANIUM/BROWN HORN'),'value'=>'SATIN TITANIUM/BROWN HORN'],
                ['label'=>__('SATIN TITANIUM/NAVY HORN'),'value'=>'SATIN TITANIUM/NAVY HORN'],
                ['label'=>__('SATIN NICKEL/AMBER TORTOI'),'value'=>'SATIN NICKEL/AMBER TORTOI'],
                ['label'=>__('CHARCOAL TORTOISE/TITANIU'),'value'=>'CHARCOAL TORTOISE/TITANIU'],
                ['label'=>__('CREAM TORTOISE/NICKEL'),'value'=>'CREAM TORTOISE/NICKEL'],
                ['label'=>__('ROSE TORTOISE/ROSE GOLD'),'value'=>'ROSE TORTOISE/ROSE GOLD'],
                ['label'=>__('BLACK/GREY HORN'),'value'=>'BLACK/GREY HORN'],
                ['label'=>__('CHARCOAL TORTOISE/CRYSTAL'),'value'=>'CHARCOAL TORTOISE/CRYSTAL'],
                ['label'=>__('CHARCOAL HORN'),'value'=>'CHARCOAL HORN'],
                ['label'=>__('SLATE HORN'),'value'=>'SLATE HORN'],
                ['label'=>__('CHARCOAL HORN GRADIENT'),'value'=>'CHARCOAL HORN GRADIENT'],
                ['label'=>__('AMBER HORN GRADIENT'),'value'=>'AMBER HORN GRADIENT'],
                ['label'=>__('BLUSH HORN GRADIENT'),'value'=>'BLUSH HORN GRADIENT'],
                ['label'=>__('MATTE BLACK/GOLD'),'value'=>'MATTE BLACK/GOLD'],
                ['label'=>__('MATTE BROWN/GOLD'),'value'=>'MATTE BROWN/GOLD'],
                ['label'=>__('MATTE TAUPE/ROSE GOLD'),'value'=>'MATTE TAUPE/ROSE GOLD'],
                ['label'=>__('MATTE TEAL/NICKEL'),'value'=>'MATTE TEAL/NICKEL'],
                ['label'=>__('BLACK/NICKEL'),'value'=>'BLACK/NICKEL'],
                ['label'=>__('GREY HORN/ROSE GOLD'),'value'=>'GREY HORN/ROSE GOLD'],
                ['label'=>__('SOFT TORTOISE/GOLD'),'value'=>'SOFT TORTOISE/GOLD'],
                ['label'=>__('LIGHT BLUE HORN/GOLD'),'value'=>'LIGHT BLUE HORN/GOLD'],
                ['label'=>__('BLUE BROWN'),'value'=>'BLUE BROWN'],
                ['label'=>__('RED GRADIENT'),'value'=>'RED GRADIENT'],
                ['label'=>__('NAVY/CREAM LAMINATE'),'value'=>'NAVY/CREAM LAMINATE'],
                ['label'=>__('BURGUNDY/BLACK LAMINATE'),'value'=>'BURGUNDY/BLACK LAMINATE'],
                ['label'=>__('BLACK/WHITE CONFETTI'),'value'=>'BLACK/WHITE CONFETTI'],
                ['label'=>__('TEAL GRADIENT'),'value'=>'TEAL GRADIENT'],
                ['label'=>__('BLACK/WHITE LAMINATE'),'value'=>'BLACK/WHITE LAMINATE'],
                ['label'=>__('BLUSH/BURGUNDY LAMINATE'),'value'=>'BLUSH/BURGUNDY LAMINATE'],
                ['label'=>__('MATTE TEAL HORN'),'value'=>'MATTE TEAL HORN'],
                ['label'=>__('MATTE RED HORN'),'value'=>'MATTE RED HORN'],
                ['label'=>__('ANTIQUE RUTHENIUM/HAVANA'),'value'=>'ANTIQUE RUTHENIUM/HAVANA'],
                ['label'=>__('ANTIQUE RUTHENIUM/BLACK'),'value'=>'ANTIQUE RUTHENIUM/BLACK'],
                ['label'=>__('MATTE MUSTARD'),'value'=>'MATTE MUSTARD'],
                ['label'=>__('SMOKE HORN'),'value'=>'SMOKE HORN'],
                ['label'=>__('RED HORN'),'value'=>'RED HORN'],
                ['label'=>__('BLUSH CAMEL'),'value'=>'BLUSH CAMEL'],
                ['label'=>__('TAUPE BROWN'),'value'=>'TAUPE BROWN'],
                ['label'=>__('MINT'),'value'=>'MINT'],
                ['label'=>__('BEIGE/NAVY GRADIENT'),'value'=>'BEIGE/NAVY GRADIENT'],
                ['label'=>__('PINK GRADIENT'),'value'=>'PINK GRADIENT'],
                ['label'=>__('BROWN WOOD'),'value'=>'BROWN WOOD'],
                ['label'=>__('CRYSTAL SMOKE/BLACK'),'value'=>'CRYSTAL SMOKE/BLACK'],
                ['label'=>__('CRYSTAL LIGHT GREEN/MINT'),'value'=>'CRYSTAL LIGHT GREEN/MINT'],
                ['label'=>__('CRYSTAL LIGHT BLUE/SKY'),'value'=>'CRYSTAL LIGHT BLUE/SKY'],
                ['label'=>__('CRYSTAL/STRIPES'),'value'=>'CRYSTAL/STRIPES'],
                ['label'=>__('KHAKI TORTOISE'),'value'=>'KHAKI TORTOISE'],
                ['label'=>__('MILKY PINK'),'value'=>'MILKY PINK'],
                ['label'=>__('MILKY PALE YELLOW'),'value'=>'MILKY PALE YELLOW'],
                ['label'=>__('MILKY ORANGE'),'value'=>'MILKY ORANGE'],
                ['label'=>__('CRYSTAL/WHITE'),'value'=>'CRYSTAL/WHITE'],
                ['label'=>__('CRYSTAL/GREEN'),'value'=>'CRYSTAL/GREEN'],
                ['label'=>__('CARGO GREEN'),'value'=>'CARGO GREEN'],
                ['label'=>__('SMOKE TORTOISE'),'value'=>'SMOKE TORTOISE'],
                ['label'=>__('WHITE/BLACK STRIPES'),'value'=>'WHITE/BLACK STRIPES'],
                ['label'=>__('SILVER/GREEN'),'value'=>'SILVER/GREEN'],
                ['label'=>__('SILVER/VIOLET'),'value'=>'SILVER/VIOLET'],
                ['label'=>__('ROSE GOLD/PINK'),'value'=>'ROSE GOLD/PINK'],
                ['label'=>__('GUNMETAL/OXBLOOD'),'value'=>'GUNMETAL/OXBLOOD'],
                ['label'=>__('SILVER/BLUE'),'value'=>'SILVER/BLUE'],
                ['label'=>__('GOLD/MAIZE'),'value'=>'GOLD/MAIZE'],
                ['label'=>__('BLACK GRADIENT'),'value'=>'BLACK GRADIENT'],
                ['label'=>__('BLUSH GRADIENT'),'value'=>'BLUSH GRADIENT'],
                ['label'=>__('SILVER/GOLD'),'value'=>'SILVER/GOLD'],
                ['label'=>__('CRYSTAL BEIGE/BROWN'),'value'=>'CRYSTAL BEIGE/BROWN'],
                ['label'=>__('CRYSTAL PALE YELLOW/CREAM'),'value'=>'CRYSTAL PALE YELLOW/CREAM'],
                ['label'=>__('CRYSTAL SMOKE/STRIPES'),'value'=>'CRYSTAL SMOKE/STRIPES'],
                ['label'=>__('CRYSTAL TAUPE/BROWN'),'value'=>'CRYSTAL TAUPE/BROWN'],
                ['label'=>__('CRYSTAL LIGHT BLUE/NAVY'),'value'=>'CRYSTAL LIGHT BLUE/NAVY'],
                ['label'=>__('CHARCOAL/GREY GRADIENT'),'value'=>'CHARCOAL/GREY GRADIENT'],
                ['label'=>__('GREEN/TEAL GRADIENT'),'value'=>'GREEN/TEAL GRADIENT'],
                ['label'=>__('PLUM/CORAL GRADIENT'),'value'=>'PLUM/CORAL GRADIENT'],
                ['label'=>__('CRYSTAL TEAL LAMINATE'),'value'=>'CRYSTAL TEAL LAMINATE'],
                ['label'=>__('CRYSTAL MAUVE LAMINATE'),'value'=>'CRYSTAL MAUVE LAMINATE'],
                ['label'=>__('CRYSTAL BRICK RED'),'value'=>'CRYSTAL BRICK RED'],
                ['label'=>__('TORTOISE/YELLOW'),'value'=>'TORTOISE/YELLOW'],
                ['label'=>__('TEAL/LIGHT BLUE'),'value'=>'TEAL/LIGHT BLUE'],
                ['label'=>__('RED/BLUSH'),'value'=>'RED/BLUSH'],
                ['label'=>__('GOLD/HAVANA/GRAD AZURE RO'),'value'=>'GOLD/HAVANA/GRAD AZURE RO'],
                ['label'=>__('GOLD/OPALINE BEIGE'),'value'=>'GOLD/OPALINE BEIGE'],
                ['label'=>__('GOLD/TRANSPARENT MUD'),'value'=>'GOLD/TRANSPARENT MUD'],
                ['label'=>__('GOLD/GREY ORANGE YELLOW L'),'value'=>'GOLD/GREY ORANGE YELLOW L'],
                ['label'=>__('GOLD/BROWN ROSE SAND LENS'),'value'=>'GOLD/BROWN ROSE SAND LENS'],
                ['label'=>__('GOLD/PURPLE AZURE CRYSTAL'),'value'=>'GOLD/PURPLE AZURE CRYSTAL'],
                ['label'=>__('GOLD/TRANSPARENT GREY'),'value'=>'GOLD/TRANSPARENT GREY'],
                ['label'=>__('GOLD/TRANSP.GREY/GRAD.GRE'),'value'=>'GOLD/TRANSP.GREY/GRAD.GRE'],
                ['label'=>__('GOLD/HAVANA/GRAD GREEN RO'),'value'=>'GOLD/HAVANA/GRAD GREEN RO'],
                ['label'=>__('GOLD/CARAMEL'),'value'=>'GOLD/CARAMEL'],
                ['label'=>__('GOLD/GRADIENT BROWN'),'value'=>'GOLD/GRADIENT BROWN'],
                ['label'=>__('GUNMETAL/BRICK'),'value'=>'GUNMETAL/BRICK'],
                ['label'=>__('SILVER/NAVY'),'value'=>'SILVER/NAVY'],
                ['label'=>__('LIGHT BLUE TORTOISE'),'value'=>'LIGHT BLUE TORTOISE'],
                ['label'=>__('PEACH TORTOISE'),'value'=>'PEACH TORTOISE'],
                ['label'=>__('CHARCOAL HAVANA'),'value'=>'CHARCOAL HAVANA'],
                ['label'=>__('AMBER HAVANA'),'value'=>'AMBER HAVANA'],
                ['label'=>__('NAVY HAVANA'),'value'=>'NAVY HAVANA'],
                ['label'=>__('TAUPE TORTOISE'),'value'=>'TAUPE TORTOISE'],
                ['label'=>__('BLACK/SLATE'),'value'=>'BLACK/SLATE'],
                ['label'=>__('CARGO/YELLOW'),'value'=>'CARGO/YELLOW'],
                ['label'=>__('OXBLOOD/BLUE'),'value'=>'OXBLOOD/BLUE'],
                ['label'=>__('MAIZE'),'value'=>'MAIZE'],
                ['label'=>__('MILKY LIGHT BLUE'),'value'=>'MILKY LIGHT BLUE'],
                ['label'=>__('WHITE/BLUE/BEIGE LAMINATE'),'value'=>'WHITE/BLUE/BEIGE LAMINATE'],
                ['label'=>__('NAVY/TEAL/PEACH LAMINATE'),'value'=>'NAVY/TEAL/PEACH LAMINATE'],
                ['label'=>__('TEAL LAMINATE'),'value'=>'TEAL LAMINATE'],
                ['label'=>__('BLACK/BROWN LAMINATE'),'value'=>'BLACK/BROWN LAMINATE'],
                ['label'=>__('BURGUNDY/PINK LAMINATE'),'value'=>'BURGUNDY/PINK LAMINATE'],
                ['label'=>__('BLACK/STRIPES'),'value'=>'BLACK/STRIPES'],
                ['label'=>__('GREY/BLUE'),'value'=>'GREY/BLUE'],
                ['label'=>__('KHAKI TORTOISE/BLACK'),'value'=>'KHAKI TORTOISE/BLACK'],
                ['label'=>__('PINE/LIGHT GREEN'),'value'=>'PINE/LIGHT GREEN'],
                ['label'=>__('BLACK/COBALT'),'value'=>'BLACK/COBALT'],
                ['label'=>__('TORTOISE/AMBER'),'value'=>'TORTOISE/AMBER'],
                ['label'=>__('NAVY/ORANGE'),'value'=>'NAVY/ORANGE'],
                ['label'=>__('NAVY TEAL HORN'),'value'=>'NAVY TEAL HORN'],
                ['label'=>__('LAVENDER HORN'),'value'=>'LAVENDER HORN'],
                ['label'=>__('HAVANA/PEWTER'),'value'=>'HAVANA/PEWTER'],
                ['label'=>__('ANTIQUE BROWN'),'value'=>'ANTIQUE BROWN'],
                ['label'=>__('(015) DARK GUNMETAL'),'value'=>'(015) DARK GUNMETAL'],
                ['label'=>__('(249) CAFE'),'value'=>'(249) CAFE'],
                ['label'=>__('LILAC MIST'),'value'=>'LILAC MIST'],
                ['label'=>__('PINK MIST'),'value'=>'PINK MIST'],
                ['label'=>__('SOFT SATIN PURPLE'),'value'=>'SOFT SATIN PURPLE'],
                ['label'=>__('ROSE 601'),'value'=>'ROSE 601'],
                ['label'=>__('BLACK/GOLDEN'),'value'=>'BLACK/GOLDEN'],
                ['label'=>__('MOCHA MARBLE'),'value'=>'MOCHA MARBLE'],
                ['label'=>__('SKY'),'value'=>'SKY'],
                ['label'=>__('PURPLE FANTASY'),'value'=>'PURPLE FANTASY'],
                ['label'=>__('SPRING ROSE'),'value'=>'SPRING ROSE'],
                ['label'=>__('MISTY ROSE'),'value'=>'MISTY ROSE'],
                ['label'=>__('CAFE'),'value'=>'CAFE'],

            ];
        }
        return $this->_options;
    }
}
/*

TAN (SHINY)
SILVER (SHINY)
BLUE STEEL (SHINY)
GUNMETAL (SHINY)
TAN
SILVER
BLUE STEEL
LIGHT GOLD
GUNMETAL
BLACK CHROME
COFFEE 218
TORTOISE/NATURAL
GEP
SILVER ROSE
BURGUNDY
CHOCO LATTE
CHAMPAGNE
BURGUNDY WINE
GUNMETAL
MATTE BROWN
GEP
DARK SILVER 040
TORTOISE/BRONZE
COFFEE 249
SHINY BROWN
DENIM
STEEL
LIGHT BRONZE
SATIN BURGUNDY
BLUSH
SATIN BROWN
MAT BARK
BLACK/GREY LENS
BLACK CHROME SMOKE
MAT BARK ESPRESSO
CHARCOAL/MATTE BLUE
WALNUT
SATIN BLACK CHROME
GUNMETAL/SATIN BLACK
WALNUT/BLACK CHROME
NEW BLUE/CHARCOAL
JET
SATIN GUN
LIGHT GOLDEN
COFFEE 246
SHINY GUN
TORTOISE/GREEN LENS
MATTE BLACK/GREY MAX POLA
SHINY DARK BROWN
CHARCOAL/DEEP GREEN
CHARCOAL/SATIN BLACK
MATTE BLACK/ANTHRACITE
MATTE BLACK/RED
STEALTH
MATTE DARK GREY
BLACK GOLDEN
TOFFEE
SPARKLING ROSE
GLOSS BLACK
ANTHRACITE
DARK METALLIC GREY
MATTE BLACK
DARK GREY/ORANGE
MATTE PLATINUM/SLATE BLUE
BLACK/WHITE BLACK
SATIN BLACK / GREY
BLACK/COOL GREY
BLACK/VOLT
GREY/BLUE/CYBER GREEN
GAME ROYAL
MATTE BLCK/GREY/SLVR FLSH
ANTHRACITE/OUTDOOR LENS
MT DP PWTR/TOT ORG/GRY SI
CRG KHAKI/GRN PULSE/OTDR
CRYS MD NVY/PHO BL/GRY W/
MT CRYSL MERCRY GRY/VLT/G
MATTE BLACK/GREY POLARIZE
STEALTH/GREY LENS
MATTE BLK/GRY W/SLVR FLSH
MERCURY GREY/SILVER/GREY
MATTE ANTHRACITE/ BLACK/
MATTE TORTOISE/BROWN
CRGO KHAK/GRN STRK/OUTDR/
DEEP RYAL BLUE/SILVR GREY
MATTE BLACK/GREY POLARIZD
MAT TORT/CARGOKHAKI/POL B
BLACK/GRY ORNG BLAZE LENS
MT BLCK/WHT W/GRY W/SLVR
BLACK/VOLT/GRY SIL FL OUT
WOLF/WHT W/GRY W/SLVR FLS
CLEAR/CLEAR
BLACK/CRYSTAL CLEAR
GREY W/SILVER FLASH LENS
GUNMETAL/GREY LENS
WALNUT/BROWN LENS
GUNMETAL/GREY MAX POLARIZ
BLACK
HAVANA
HAVANA AMBER
BLUE/AZURE
HAVANA/CARAMEL
BORDEAUX RED
ANTIQUE ROSE/ROSE
BLACK/GRAD GREY LENS
HAVANA/GRAD KHAKI LENS
SATIN BLACK
BLACK/VOLT/GRY SIL FL LEN
FOREST
DARK TORTOISE
OCEAN
BLUE HAVANA
YELLOW HAVANA
BLUE
MATTE PETROLEUM
BRUSHED DARK GUNMETAL
SHINY WALNUT
TOKYO TORTOISE
SOFT TORTOISE
NUDE
BLACK/GREEN/CRYSTAL
NAVY/HYPER JADE
CONCORD/FUCHSIA
BLACK/BROWN
BLACK/BLUE
BLACK/GREEN
BLACK/BLUE/BLACK
DARK GREEN/GREEN
BLUE/YELLOW/BLUE
BLUE/TURQUOISE
BURGUNDY/WHITE/PURPLE
MATTE BLACK/PHOTO BLUE
MATTE BLACK/SONIC YELLOW
OBSIDIAN
MATTE BLACK/POISON GREEN
MATTE BLACK/CHALLENGE RED
NAUTICAL NAVY
DARK GUN
DARK BROWN
DARK GUNMETAL
MATTE HAVANA GEP
PURPLE TORTOISE
SHINY BLACK
STRIPED GREY/PETROL
STRIPED BROWN/GREY
CHARCOAL
CHOCOLATE
MIDNIGHT
EGGPLANT
BLACK SILVER
BROWN SIERRA
BORDEAUX
BRUSHED GUNMETAL
BRUSHED NAVY
LIGHT BROWN
IVORY
STRIPED TOBACCO
TORTOISE
BORDEAUX/RED
PEACH/NUDE
STRIPED BROWN GREY
STRIPED BLUE GREEN
SATIN BLUE
LIGHT TORTOISE
BLUE NAVY
CYCLAMINE
ANTIQUE ROSE
CRYSTAL KHAKI GREEN
BROWNFADE
DEMIGREY
GREYFADE
OLIVE TORTOISE
TEAL TORTOISE
BLUEROSE
BROWNROSE
CRYSTALPINK
GREYROSE
TRANSPARENT GREY
BLUE/YELLOW
TRANSPARENT BLUE
BROWN MOSAIC
BLUE MIST
LILAC MOSAIC
BROWN
AZURE HAVANA
ROSE HAVANA
PURPLE
BLACK/CRYSTAL
MISTY BLUE
GREY BLUE H0RN
BROWN PINK HORN
BLACK/GREY
GREY
BROWN/BLUE
DARK BLUE
DARK BLUE/BLUE
MT ANTRHA/BLK/GREY SILVER
BLACK/VOLT/GREY LENS
MATTE TORTOISE/BROWN LENS
MT MIDNGHT NVY/OCEAN FOG/
MT CR DK MAG GRY/BLK/GRY
MURCY GRY/BLK/GRY SILVER
BLACK/MATTE BLACK/GREY LE
MT SQDRN BLU/BLK/GRY SILV
CRTL MRCY GRY/MTL SVR/DK
BLACK/DARK GREY LENS
MATTE CRSTL MLTRY BRWN/GR
SATIN BLACK/BLACK
DARK GREY/DARK ARMY GREEN
WALNUT/DARK BROWN
DARK GREY/BLACK
WALNUT/DARK ARMY GREEN
JET GREY
MURDERED/SMOKE
JET/RED ION
PURPLE NEBULA/PURPLE ION
BLACK LEOPARD/BRZ GRAD
SHINY TORTOISE
MATTE TORTOISE BRONZE
POLISHED WALNUT BROWN
MATTE CEMENT/PEARL ION
JET TEAL GREEN ION
BLUE SMOKE GREY
MATTE BLACK H20/GREEN ION
MATTE H2O GREY POLAR
MATTE H2O/PLASMA P2
MAT TORTS/BROWN w/BRNZ FL
MILITARY BRWN/WALNT/POLAR
MT DP PWTR/TOT ORG/SHTR/G
MT BLK/MLTR BL/GRY w/BLU
MAT BLK/ELE PUR/GRY/ML VI
BLUE/WHITE/BLUE
SHINY GUNMETAL
CRST MT DK GRY/UNVR RD w/
BLACK/VOLT w/GREY LENS
CRY GYM BL/VT GRY LEN W/S
CRY/H CRM/G RD GY LEN W/S
MATTE BLACK/VOLT w/GREY L
GUNMETAL/GYM RED
BLK/CRYSTL CLR W/GREY POL
LIGHT SATIN BROWN
SHINY ROSE
ANTHRA-VT W-GRY W-SIL FL
WHITE/CRY HYCRIM GREY W/S
SQUAD BL-HT LAV W-SIL FL
GOLD
SATIN WALNUT/DARK BROWN
ST BLU/OBSID
MATTE GREY HORN
MATTE BROWN
MATTE BLUE HORN
BLACK W/GREY LENS
MATTE BLACK W/GREY SILVER
MERCY GRY/SIL W/GY SIL FL
MAT BLK/BLU GRAD W/BL SK
MAT BLK/GRN GRAD W/GY GRN
MAT BLK/VLT w/GRY ORG MIR
MATTE D P/GRN P W/GRY ML
MATTE GREY
MATTE BROWN HORN
BLACK PURPLE
HAVANA TEAL FADE
BROWN HORN LAVENDER
RUBY
BLACK/HAVANA
STRIPED BROWN/BLUE
HAVANA/AZURE/PETROL
GREEN/WINE
BLUE/PURPLE
BLUE STRIPED
BLACK/LIME
GREEN
BLUE STEEL/ORANGE
BLUE/GREEN
BLACK MATTE
MATTE ONYX
GREY MATTE
DARK HAVANA
BLUE MATTE
NAVY
PLUM
NATURAL
COFFEE
MIDNIGHT BLUE
RED
TURTLEDOVE
SATIN BLACK/DARK GREY
LIGHT GUNMETAL
STEEL BLUE
MATTE BLACK/GREEN ION
MATTE BLACK/RED ION
MATTE WOODGRAIN
MATTE PURPLE/PURPLE ION
MATTE BLACK/GREY
MATTE TORT/GREEN G15
MATTE BLACK/ROSE GOLD
MATTE BLACK SILVER ION
GREY MATTER/SKY BLUE ION
NEO GEO/GREY
MATTE CLEAR BLUE ION
HAWAII GREY
MATTE COPPER MARBLE SILVE
EBONY
TURTLE/CREAM
COBALT/AZURE
BURGUNDY ROSE
BROWN CHESTNUT
MAT BLACK
BURNT OCRE
BLUE AZURE
BLACK/SILVER
BRUSHED GUNMETAL/BLACK
BLACK/WHITE/GREY LENS
MT DK MAG GRY/WH/MX OUTDO
MT BLACK/WH W/GREY SIL FL
MT BLK/VOLT/GRY W/ SIL FL
MATTE GREY/OUTDOOR
GAME ROAYL/WH W/GRY SIL F
UNIVERS RD/WH W/GRY SIL L
BLK/VOLT GREY LENS W/SIL
MT ANTHRA-CB W-GREY SI FL
GAME RY/VOLT GRY LENS W/S
BLK/GAM RYL TORT/GRY SILV
TORTOISE/BROWN
BLACK/BOMBER GREY
BLACK/PHOTO BLUE
OBSIDIAN/PURE PLATINUM
MT BLACK/VT W-GY W/SIL FL
INS B/HT LV W/GRY W/SL FL
CLEAR/H JAD GRY LEN W/SIL
CLEAR/GRAPE GRY LEN W/SIL
CLEAR/PUNCH GRY LEN W/SIL
BLACK GUN
PALLADIUM
NAVY GUN
DARK MAGNET GREY/BLACK
BLACK/VIVID PINK
SATIN GUNMETAL/NIGHT FACT
SHINY WALNUT/CARGO KHAKI
SHINY BLACK/BLACK
SATIN BLUE/MILITARY BLUE
GRAY HORN
BROWN HORN
CAMEL BLUSH
DARK GREY
LIGHT HAVANA
MEDIUM BLUE
VIOLET
CYCLAMEN
ROSE
GOLD/TRANSPARENT BROWN
GOLD/TRANSP BROWN/GRAD FL
GOLD/TRANSP GREY/GRAD BLU
GOLD/TRANP BROWN/GRAD BRO
MATTE H20/BLUE ION P2
MATTE H2O PLASMA P2
MATTE H20 GREEN ION P2
MATTE H20 BLUE P2
MATTE REDWOOD/SMOKE
SMOKEY ROSE
SHINY BORDEAUX
SHINY GOLD
GOLDEN SAND
GOLD WHITE
ESPRESSO
SATIN GUNMETAL-BLACK
SATIN BLACK-DARK GREY
SEMI MATTE BLACK
SATIN GUNMETAL
SATIN NAVY
BLACK/WHITE
HAVANA/PINK/GREEN
GREEN HAVANA/AZURE
PETROL/VIOLET/BLUE
STRIPED BROWN CREAM
STRIPED NUDE
STRIPED PETROL
KHAKI
DEEP HAVANA
BLACK-CHALLENGE RED
SATIN BLUE-MIDNIGHT NAVY
ROSE GOLD/LIGHT BROWN
GOLD/BLUE
GOLD/PEACH
GOLD/GREEN
GOLD/LIGHT GREY
ROSE GOLD/BROWN
MATTE GUNMETAL
ANTIQUE GUNMETAL
MATTE NAVY
HONEY
HAVANA GREY
WINE
TRANSPARENT TURTLEDOVE
GOLDEN AMBER
PALE GOLD
GOLDEN BEAUTY
MATTE DARK TORTOISE
MATTE OLIVE
MT BLK/POI GRN/GRY W/ ML
MT BLK/GAME RYL/GRY W/ BL
MT BLK/CHAL RD/GREY W/ ML
MT BLACK/WHITE/GREY POLAR
WH/DK CON/GREY W/ ML VIOL
MT DP PWTR/TOTAL ORG/GRY
MT BLK/WH/GREY POLAR LENS
BROWN W/ANIMALIER
STRIPED GREEN
BURGUNDY W/ANIMALIER
EMERALD
CORAL
GREY HORN
ROSE GOLD
MATTE BLACK/TEAL
MATTE TORTOISE
MATTE BLACK BLUE
CRYSTAL NAVY
MATTE CRYSTAL
MATTE DARK GREY-BLACK
MT CRY CARGO KHAKI-IRON G
MT CRYSTAL CARGO KHAKI-CY
BLACK/CAMOUFLAGE
SATIN BLACK-CHALLENGE RED
WALNUT-SPACE BLUE
SATIN BLACK-VOLT
BRUSHED GUNMETAL-HYPER JA
BLUE-BLACK
OLIVE MARBLE
BLUE MARBLE
BLACK GUNMETAL
BROWN GUNMETAL
NAVY GUNMETAL
BLACK/CRYSTAL VOLT
WALNUT/CARGO KHAKI
SATIN BLUE/BLACK/PHOTO BL
MATTE OLIVE TORTOISE
CORDOVAN
SATIN BLACK/VOLT
CRYSTAL DARK GREY/PHOTO B
CRYSTAL GYM BLUE/HYPER CR
ST CRYSTAL SP BLUE/HYPER
CRYSTAL GYM RED/CHALLENGE
SATIN BLACK/MAGNET GREY
SATIN GUNMETAL/GREY/HYPER
SATIN BLACK/CRYSTAL VOLT
GUNMETAL/GREY/VOLT
GREEN MATTE
PETROL
FOG
SHINY CHOCOLATE BROWN
SHINY NIGHT BLUE
SHINY DEEP WINE
OLIVE
BROWN TEAL
PURPLE PINK
GRAPE LEMONADE
LIGHT PINK LILAC
SHINY PURPLE
PURPLE HORN
CABERNET
PURPLE ROSE
SATIN SILVER
SATIN GOLD
SATIN PLUM
BLUE TORTOISE
RED TORTOISE
MAUVE
MT DK GRY/FLA LIME W/GRY
MATTE BLACK/VOLT W/GRY SI
MT MID NAYBL LAG W/GRY BL
MATTE BLACK/VOLT
MATTE BLACK/ANTHRACITE/CL
MT CRYSTAL DK MAGNET GRY/
MT CRYSTAL MIDNIGHT NAVY/
ROSE TORTOISE
MATTE BLACK/MATTE CRYSTAL
MATTE BLACK/CRYSTAL PHOTO
MATTE DARK GREY/CRYSTAL H
MATTE DARK GREY/CLEAR
MAT CRYSTAL CARGO KHA/CRY
MATTE NAVY/PHOTO BLUE
MT BLACK/MATTE CRYSTAL CL
MT CRYSTAL DK GRY/CRYSTAL
MT CRYSTAL NY/CRY BLEACH
MATTE OBSIDIAN/HYPER JADE
SATIN TAUPE
SATIN MINT
SATIN ROSE
TEAL
MATTE GREEN
MATTE BLUE
PETROLEUM
LAVENDER
LILAC
RUST
OLIVE GREEN
BLUE HORN
SATIN PURPLE
SHINY ROSE GOLD
ANTHRACITE/BLACK
MIDNIGHT NAVY-PHOTO BLUE
NAVY/RACER BLUE
CRYSTAL PURPLE
GUNMETAL-DARK GREY-CLEAR
SATIN WALNUT-DEEPEST GREE
SATIN BLUE-MIDNIGHT NAVY-
BLACK/ANTHRACITE
SATIN BLACK-HYPER JADE
BRUSHED GUNMETAL-FLASH LI
BLACK-VOLT
BLACK-HYPER PUNCH
DARK GREY-HYPER JADE
CARGO KHAKI-TOTAL ORANGE
STRIPED SMOKE
STRIPED BLUE
STRIPED WINE
STRIPED LIGHT BROWN
STRIPED VIOLET
WHITE/VD PK W/GRY PK FL
GOLD/KHAKI
GOLD/GRADIENT GREY
GOLD/GRADIENT KHAKI BEIGE
MATTE H2O PLASMA ION
MATTE H2O GREEN ION
MATTE MAGNET GREY-SILVER
MATTE BLACK GREY
MATTE BLACK-BLUE ION
MATTE BLACK-GREEN ION
MATTE BLACK-COPPER
MATTE DEEP NAVY GREY
MATTE UTILITY GREEN GREY
MATTE BLACK-RED ION
MATTE BLACK-SUPER SILVER
JET-DARK COPPER
DARK SLATE BLUE
SLATE
CRYSTAL
DEEP WINE
BRUSHED BLACK CHROME
BRUSHED BROWN
TURTLE DOVE
MARBLE BROWN
MARBLE VIOLET
MARBLE RED
BLACK GREY PAISLEY
BLACK&WHITE PAISLEY
BROWN PAISLEY
HAVANA PAISLEY
GREEN PAISLEY
BLUE PAISLEY
GOLD-BROWN
GOLD-GREY
ROSE GOLD-PEACH
GOLD-KHAKI
GOLD-TRANSPARENT PEACH
GOLD-TRANSPARENT GREY
GOLD-CARAMEL
GOLD-TRANSPARENT LIGHT BR
BLACK WITH STARPHOSPHO TE
AZURE WITH GLITTER TEMPLE
AQUA WITH PHOSPHO TEMPLES
FUCHSIA WITH GLITTER TEMP
ROSE WITH PHOSPHO TEMPLES
ONYX MATTE W/STAR PHOSPHO
BLACK MATTE W/STAR PHOSPH
BROWN MATTE
DARK GREEN
AQUA MATTE W/STAR PHOSPHO
MID BLUE MATTE
BLONDE TORTOISE
MATTE WHITE
MATTE KHAKI
MATTE NAVY BLUE
MATTE BLUE NIGHT
MATTE BURGUNDY
MATTE RED
BLACK/MATTE BLACK W/GRY L
MT BLACK/DP PEWTER W/GRY
WOLF GREY/COOL GREY/TEAL
MT GREY TORTOISE W/DK GRY
DARK ATOMIC TEAL/BLACK/AM
DARK TEAL/BLACK/AMBER LEN
CLEAR W/GREEN LENS
MT BLACK/CAMO W/DK LENS
MATTE BLACK-DEEP PEWTER W
MATTE BLK-CINNABAR W-GRN
DARK GREY/GREEN
MT WF GREY-G RED W-GREY L
WOLF GREY/TEAL
CARGO KHAKI/DARK GREY
MT CRY SEAWD-CYB W-GREEN
MT NAVY/CAMO W/GRY SIL FL
MT DARK OBS-WHITE W-GREY
MT SQUAD BL-DP PEW W-GREY
MIDNIGHT TEAL/DARK GREY
MATTE DARK GUNMETAL
MATTE DARK BROWN
PAISLEY/BLACK
PAISLEY/GREEN
PAISLEY/BLUE
PAISLEY/VIOLET
LIGHT GOLD/AZURE
LIGHT GOLD/BLUE
LIGHT GOLD/BLACK
LIGHT GOLD/TAUPE
BLACK/BLACK VISETOS
SHINY DARK GUN/BLACK
SHINY DARK GUN/KHAKI
TORTOISE/COGNAC VISETOS
BLUE/PINK VISETOS
MATTE BLACK/BLACK VISETTO
SILVER/SILVER MARBLE GLIT
GOLD/GOLD MARBLE GLITTER
SHINY ROSE GOLD/BROWN VIS
SHINY ROSE GOLD/NUDE VISE
BLUE PEARLIZED
VIOLET PEARLIZED
SHINY GOLD-GREY BLUE HORN
SHINY GOLD-HAVANA
SHINY GOLD-BLACK
BROWN/NUDE GRADIENT
GRADIENT BURNT
GRADIENT MAUVE
HAVANA BROWN
GUN
BROWN GRADIENT
MATTE H2O-GREY
MATTE H2O-ROSE GOLD ION
MATTE H2O-PLASMA ION
MATTE H2O-SILVER ION
MATTE BLACK/SMOKE
MATTE MAGNET GREY/RED
MATTE BLACK/SILVER
MATTE DEEP NAVY/BLUE SKY
ANTIQUE GOLD
GRADIENT BORDEAUX
SMOKE CRYSTAL
OLIVE CRYSTAL
MATTE SMOKE HORN
MATTE OLIVE HORN
BROWN HORN GRADIENT
BLUE-BROWN HORN GRADIENT
LILAC HORN GRADIENT
OXBLOOD
WHISKEY
BROWN TORTOISE
BERRY TORTOISE
SHINY NAVY
MATTE H2O BLACK GREY
BLACK/MATTE BLACK W/GREY
MATTE BLK/TUMB GRY W/GREY
WOLF GREY/DP PEWT W/GREY
MT WF GRY/DP P W/GREY LEN
MT CAR KHAKI W/GRN GUN FL
MT OBSIDIAN W/GRY SIL FL
SKY BLUE
SATIN EARTH
SILVER/BROWN MARBLE
GOLD-PEACH
GOLD-BLONDE HAVANA
BLACK-HAVANA
HAVANA-AMBER
MATTE LIGHT GREY
BLONDE HAVANA
SATIN BLACK-PHOTO BLUE
BRUSHED GUNMETAL-TOTAL OR
SATIN WALNUT-GREEN STRIKE
GUNMETAL-WOLF GREY
SATIN BLUE-PHOTO BLUE
BRUSHED GUNMETAL-TEAM RED
SATIN BLACK-WOLF GREY
SATIN WALNUT
BRUSHED GUNMETAL-TIDEPOOL
SLATE BLUE
TORTOISE-BLACK
HAVANA/BLUE
HAVANA/VIOLET
TORTOISE/BLACK
TRANSPARENT BORDEAUX/BORD
ROSE/NUDE VISETTOS
BLACK/COGNAC VISETTOS
MT BLACK/SPEED TINT UML R
MATTE BLACK/WHITE/SPEED E
MATTE GREY/SPEED EXTRA WH
MT WHITE/SPEED TINT UML G
MATTE CARGO KHAKI/SPEED U
MATTE OBSIDIAN/SPEED TINT
MATTE TEAM RED/SPEED TINT
MT OBSID/DP RYL BLU/MAX S
MATTE NAVY/SPEED TINT
MT BLK/WOLF GRY/GRY SILVE
MT BLK/BR CRIM/GRY SIL FL
MT VOLT GREN/WH/GRY SIL F
MATTE KHAKI/GREY SILVER F
MT OBSID/RACR BLU/GRY SIL
MT BR CRIMSON/WHITE/GRY S
MT BLK/SILVR/GRY SUPER SI
MT BLK/GOLD/GREY ML GOLD
MATTE GREY/SPEED ML WHITE
MT ANTH/GUMTL/GREY ML GRE
MT WH/BR CRIMSN/GRY SUPER
MT BK/BRGHT CRIMSON/MAX S
MT BLK/WOLF GREY/GRY SILV
MT OBSID/DP RYL BLU/GRY S
SMOKE
MT ANTHRA-GUNMTL-POLAR GR
JET/GREY
MATTE TORTOISE/BRONZE
MATTE H2O MAGNET GREY
MATTE H2O SKY BLUE
MATTE H2O ROSE GOLD ION
MATTE H2O BLUE ION
CRYSTAL BROWN
CRYSTAL OLIVE
MATTE BLACK W/UNIVERSITY
CLEAR/UNIVERSITY RED
MATTE OBSIDIAN/SILVER
MATTE BLACK/UNIVERSITY RE
ANTHRACITE/UNIVERSITY RED
MATTE BLACK-GUNMETAL
MATTE TUMBLED GREY-GREEN
MATTE CRYSTAL CLEAR-BRIGH
MATTE CRYSTAL CLEAR/BLUE
MATTE BLACK/CRYSTAL CLEAR
COBBLESTONE/SILVER
BLACK/CARGO KHAKI
MATTE BLACK/DARK GREY
MATTE BLACK/WHITE
MATTE BLACK/MATTE RED
ANTHRACITE/SPACE BLUE
MATTE DARK GREY/VOLT
BLACK-DARK GREY
BLACK-TEAM RED
MATTE GREY/BLUE
MATTE TORTOISE-GREEN STRI
MATTE MIDNIGHT NAVY-DARK
DARK GREY-TOTAL ORANGE
CARGO KHAKI-FLASH LIME
BLACK-FLASH LIME
BLACK-PHOTO BLUE
ANTHRACITE/RED
TEAM RED-BRIGHT CRIMSON
SATIN BLACK-TEAM RED
SATIN GUNMETAL-VOLT
BEIGE
DARK TURQUOISE
KHAKI BROWN GRADIENT
BLACK-DARK RUTHENIUM
BLUE-DARK RUTHENIUM
BROWN-SHINY GOLD
BLUE BARK
BUTTER HORN
SHINY UMBER HORN
MATTE CRYSTAL SEA BLUE
SHINY RED HORN
MOSS BROWN HORN
OLIVE HORN
BLUE-YELLOW TORTOISE
NUDE TORTOISE
CRYSTAL SLATE
CRYSTAL JADE
CRYSTAL SMOKE
INDIGO
MATTE BLACK/DEEP PEWTER W
BROWN W/BROWN POLARIZED L
MUSHROOM
SHIMMER DARK TORTOISE
SKY BLUE TORTOISE
AQUAMARINE
TOPAZ TORTOISE
PACIFIC TORTOISE
BLUSH TORTOISE
CYAN BLUE TORTOISE
ROUGE TORTOISE
CRYSTAL TAN
ROSE NUDE
BLACK GREY CAMOUFLAGE
BROWN PEACH CAMOUFLAGE
BLUE BROWN CAMOUFLAGE
PURPLE VIOLET CAMOUFLAGE
SATIN BLUE-DEEP ROYAL BLU
CRYSTAL BROWN W-CHEETAH
GOLD-NUDE
GOLD-BLACK
GRAD GREY/TURTLEDOVE
GRAD ANTIQUE ROSE/NUDE
GRADIENT PETROL
SMOKE GREY
GOLDEN CHESTNUT
GRAPE
GOLD/NUDE
GOLD/BROWN
GOLD/BLACK
BURNT
ONYX
CRYSTAL BLUE
MT BLACK/SIL W/GREY POL L
MATTE COFFEE
MATTE DEEP SEA
BROWN CRYSTAL
GREY CRYSTAL
BLUSH CRYSTAL
RED CRYSTAL
AQUA
MATTE BLACK GREEN G15
MATTE MAGNET GREY SILVER
MATTE NAVY/BLUE SKY ION
BRUSHED GUNMETAL-VOLTAGE
SATIN GUNMETAL-TEAM RED
BLACK-WHITE
SATIN BLACK-ANTHRACITE
BRUSHED GUNMETAL-OBSIDIAN
WALNUT-CARGO KHAKI
HUNTER
HAVANA/TURQUOISE
BROWN/STRAWBERRY
BLUE/NUDE
BLACK/NUT
GEOMETRIC FUCHSIA AZURE
GEOMETRIC BLUE RED
GEOMETRIC BROWN GREY
SATIN GOLDEN
GREYBROWN
AMETHYST
MERLOT
BROWN/TEAL
NAVY/LAVENDER
PLUM/ROSE
BROWN/LAVENDER
NAVY/TEAL
PLUM/LILAC
PLUM TORTOISE
SMOKEY GRAPE
VIOLET TORTOISE
PURPLE BLUE
MT BLACK/SIL W/GRY SIL FL
MT TORTOISE W/GRN GUN FL
BLACK/VOLT W/GRY W/SIL FL
MT WF GRY/OBS W/GY SIL FL
MATTE A/BLACK W/ DK GREY
WHITE W/GREY SILVER FLASH
MATTE CAR KHA/BLK W/ OUT
MATTE OBS/OC FG W/GRY SIL
BROWN ROSE
PEACH
LIGHT TURTLEDOVE
DARK BLUE CHROME
BROWN/JADEITE
TEAL/GOLD
NAVY/MAGENTA
BURGUNDY/PLUM
BLACK GOLD
NAVY/CORAL
BURGUNDY/ASH ROSE
MATTE BLUE NAVY
TURQUOISE
MATTE EGGPLANT
TEAL HORN
MATTE TOKYO TORTOISE
SHINY DEEP RED
SHINY BLACK/SMOKE
MATTE DARK TORTOISE/BROWN
MATTE CRYSTAL NAVY/BLUE
MATTE CRYSTAL/RED ION
MATTE BLACK/BLUE
SHINY TORTOISE/BROWN
SHINY KHAKI/GOLDEN
MATTE CRYSTAL GREY/SMOKE
TOKYO TORTOISE/GREEN
SHINY RED/RED ION
CHROME
BLACK/SHINY GOLD
BROWN/SHINY GOLD
ORCHID/SHINY GOLD
STRIPED GREY/BROWN/GREEN
HAVANA/HONEY
BLUE/LILAC
BURGUNDY/ROSE
BLACK/MILK
CRYSTAL BURGUNDY
SHINY GOLD/BLACK
SHINY LIGHT GOLD/BURGUNDY
SHINY GOLD/BROWN
SHINY GOLD/CREAM
MARBLE GREY
GREEN HORN
GREY BLUE HORN
SHINY PALLADIUM/MARBLE GR
SHINY GOLD/MARBLE BROWN
MATTE TORTOISE/BLACK
BAROQUE BROWN/ORANGE QUAR
MATTE SQUADRON BLUE/GUNME
MAGENTA
PINK
MATTE UTILITY GREEN/GREY
MATTE NAVY/GREY
BLACK VISETOS/BLACK
COGNAC VISETOS/BLACK
NUDE VISETOS
GREEN VISETOS
BROWN/COGNAC VISETOS
CORAL VISETOS
BLACK/BLACK VISETTOS
BROWN/COGNAC VISETTOS
BLUE/COGNAC VISETTOS
BLACK MARBLE
TORTOISE BLACK
BROWN MARBLE
RED MARBLE
CRYSTAL SAPPHIRE
CRYSTAL RUBY
BLACK/ACID GREEN
DARK HAVANA/ORANGE
LIGHT HAVANA/PURPLE
GREEN/OCHRE
DARK BLUE/ELECTRIC BLUE
LAPIS CRYSTAL
AZURE
ORCHID
SATIN DENIM
SEMIMATTE RED
MINT BROWN CAMOUFLAGE
ORCHID BROWN CAMOUFLAGE
COBALT GREEN PURPLE CAMOU
COSMETIC PINK
GREY HAVANA
SAND
STRIPED BORDEAUX
BROWN HAVANA
PETROL HAVANA
PURPLE HAVANA
HAVANA/PURPLE
STRIPED BLUE/FUCHSIA
VIOLET/ORANGE
STRIPED BORDEAUX/AZURE
BLACK PAISLEY
TURTLE DOVE PAISLEY
BORDEAUX PAISLEY
POUDRE
MAT BLACK PAISLEY
MAT PURPLE PAISLEY
GREY HORN/OCHRE
LIGHT BROWN HORN/ORANGE
BLACK/TURTLE DOVE
DARK HAVANA/BURNT
TURTLE DOVE/CYCLAMEN
STRIPED HONEY/DARK BLUE
STRIPED GREY/CYCLAMEN
STRIPED GREEN/ORANGE
BLACK TORTOISE
ANTIQUE ASH BROWN
JET/BLACK
BLACK/VACHETTA
MILKY BONE
MILKY NAVY
BLACK/ CREAM
CREAM TORTOISE
BEIGE MARBLE
ROSE MARBLE
MINK TORTOISE
BONE
SATIN TITANIUM
GOLD/TRANSPARENT PEACH
GOLD/TRANSPARENT LIGHT GR
GOLD/HAVANA
GOLD/TRANSPARENT KHAKI
GOLD/TRANSPARENT GREEN
ROSE GOLD/TRANSPARENT BRO
GOLD/HAVANA/GRAD YELLOW L
GOLD/HAVANA/GRAD CYCLAMEN
SILVER/LIGHT GREY
GOLD/ROSE
LIGHT BURNT
CARAMEL
GOLD/LIGHT BROWN
GOLD/GREY
GOLD/ROSE PEACH
GOLD/BROWN PEACH
GOLD/HAVANA/GRAD ROSE HON
GOLD/HAVANA/GRAD GREEN/RO
BURGUNDY TORTOISE
MATTE ANTHRA/BRIGHT CRIMS
TOBACCO
AUTUMN BLUE
MUSTARD TORTOISE
PETROL TORTOISE
TURTLE GRADIENT
BLUE GRADIENT
BURGUNDY GRADIENT
LIGHT SHINY GOLD/BLACK
SHINY GOLD/IVORY
SHINY GOLD/RED
IVORY/PLUM
TORTOISE/PETROL
BLACK/TORTOISE
TORTOISE/IVORY
WOOD/BROWN
WOOD/TAUPE
WOOD/SMOKE
COPPER
TORTOISE/GREEN
TOKYO TORTOISE/RUBY
BLUE/GREY
SLATE VISETOS
BURNT VISETOS
SPARKLY IVORY
SPARKLY JEANS
SPARKLY PINK
BLACK VISETOS
STRIPED BLUE/BLUE VISETOS
STRIPED ORCHID/VIOLET VIS
HAVANA BLUE
HAVANA VIOLET
STRIPED KHAKI
BUTTERSCOTCH
BLACK/CORAL
BRUSHED GUNMTL/TIDE POOL
SATIN WALNUT/TUMBLED GREY
MATTE BLACK/PURE PLATINUM
SATIN WALNUT/GREEN STRIKE
BLUE/PURE PLATINUM
SATIN BLUE/PHOTO BLUE
GREY MARBLE
BLONDE HAVANA/SHINY TEAL
WHITE
MEDIUM HAVANA
ROSEGOLD
YELLOW GOLD
MATTE LIGHT BROWN
STRIPED BROWN
MATTE ANTH W/GRN MIRR LEN
MT MLK/P WI W/GRY W BL PC
MATTE SQUAD BLU W/BL MIRR
CRY CL/MED OL W/TRI COP L
BLACK W/GREY POL LENS
MT BROWN W/BRWN POL LENS
BLACK W/GREY POLARIZED LE
CRYSTAL YELLOW
GRADIENT BLACK
GRADIENT GREY/TURTLEDOVE
GRADIENT CARAMEL/CHAMPAGN
GRADIENT TURTLEDOVE/LIGHT
MT PLATNM/VOLT/SPED WHITE
BLACK/GUNMETAL
MARINA BLUE
DARK PLUM
FUCHSIA
DARK NAVY
MATTE BLACK/GREY SILVER F
MATTE ANTHRACITE/DARK GRE
MATTE OBSIDIAN/GREY SILVE
MATTE GREY/SPEED TINT
MATTE BLACK/VOLT/SPED ML
BLUE/GREY BLUE FLASH
MATTE SHARK
GREY TORTOISE
MATTE DEEP GREEN
MATTE COLLEGIATE NAVY
MATTE ANTHRACITE W/GREY L
MATTE BLACK W/GREY LENS
MATTE GRAVEL/FIRE
CRYSTAL SHARK/SMOKE FLASH
MATTE SHARK/BLUE
MATTE DARK COMPASS/COBALT
MATTE NIGHT SHADOW
MATTE MERCURY/BLUE SKY FL
MATTE COLLEGIATE NAVY/SMO
MATTE DARK COMPASS/AMBER
BLACK/SMOKE
MATTE SHARK/SMOKE
SATIN BLACK/SMOKE
GUNMETAL/SMOKE
BLUE COMPASS/ORANGE FLASH
SATIN GUNMETAL/GREEN
SATIN GUNMETAL/SMOKE
MATTE BLACK/GREEN
MATTE CINDER/BROWN
SATIN GREY MOSS
SATIN SAND
HAVANA/BEIGE
TOKYO HAVANA/PURPLE
BLUE/HAVANA
PETROL GREEN
BLACK/DARK RUTHENIUM
BLUE AVIATION
SHINY SILVER
WOLF GREY
MATTE ANTHRACITE
MATTE SEAWEED
MATTE OBSIDIAN
MATTE TORTOISE/G15
MATTE BLUE CHALK/SMOKE
MATTE MAGNET GREY/SMOKE
MATTE TORTOISE/GREEN
MATTE DEEP SEA/SMOKE
BROWN/MINT
BROWN/ROSE
GREEN TORTOISE
BLACK W/GREY BLACK MIRROR
MATTE TORTOISE W/BROWN LE
MT PURPLE W/GRY SILVER FL
MATTE BLACK/SOLAR RED
SPACE BLUE
MATTE CARGO
MATTE CARGO KHAKI
MT BLACK/GRN GRA W/GRN FL
MT BLACK/R GRAD W/ML RD L
MT BLACK/S RD W/GRN SUN L
MT BLACK/HY CRI W/GR AMAR
WH/TUR GRN W/GRN FL LENS
MT TORT W/GRN GUN FLASH L
MT GREEN W/GRN TRIFLECT L
MT RED W/GRY TRIFLECT COP
CLEAR W/GREY SILVER FLASH
MT BLACK W/DARK GREY LENS
BLACK W/DARK GREY LENS
MT BLK/CL GR W/GRY SIL FL
WOLF GREY/GREY LENS
MT GREY W/GREY SILVER FL
MT BLACK/RA GRN W/DK GRY
MT BLACK/CLEAR F W/DK GRY
MT BLK/R GRN PRI W GR ML
MT BLK/CR F W/GY ML AMA L
MT CARGO KHAKI W/GRY COP
MT BLUE W/GREEN TRI PET L
CLEAR W/GREY SUPER FLASH
MT BLACK W/HY VIO F DK GY
MT GREY W/DARK GREY LENS
MT BLACK W/GRY ML RD FL L
BLACK W/GRY ML GOLD FLASH
MT W GRY W/GRY BLU NI FL
MT BLACK/AUR GRN PR W/AM
MT ANTHRA W/GRY VIO FL LE
MT BLACK/CL F W/GRN ML SU
MT GRN W/GRN TRI PT LENS
MT CARGO K W/GRY W/ML GRN
MT BLACK/S RED FA W/DK GY
ANTHRACITE W/GRY SIL FL L
CARGO KHAKI W/OUTDOOR TIN
MT SQUAD BL W/GRY SIL FL
MATTE RED W/DARK GREY LEN
MT TORT W/GRN GUN FL LENS
MATTE BACK W/DARK GREY LE
MT BLACK W/GREY POL LENS
BLACK W/GREY SILVER FL LE
MATTE ANTH/WF GY W/OUT LE
MT GREY W/GREY W/SILVER F
MT ANTHRACITE W/DARK GREY
MT SP BL/RA GRN W/DK GY L
MT CAR KHAKI W/GRY TRI CO
MT NAVY W/GRN STAN MIRR L
MATTE BLACK CKARK LITTLE
BORDEAUX HORN
BLACK HORN
AQUA HORN
ROSE HORN
HAVANA/CRYSTAL
BORDEAUX/CRYSTAL
TURQUOISE PAISLEY
RED PAISLEY
ORANGE PAISLEY
MATTE AMBER TORTOISE
MATTE BLACK/GUNMETAL
MATTE GUNMETAL/GUNMETAL
MATTE BROWN/GUNMETAL
MATTE OLIVE/GUNMETAL
MATTE TAUPE
MATTE BRICK
MATTE ANTIQUE GOLD
ANTHRACITE/GREY SUPER IVO
DK TEAL/GREY ML AMARANTHI
PORT WINE/GREEN ML SUNSHI
MT BLACK/ANTHRA W/GRY SIL
MT BLUE W/GRN TRI PET LEN
RED MATTE
SAGE
MATTE BLACK W/DARK GREY L
MT TOK TORT W/GRN GUN FL
MATTE CRYSTAL CLEAR/SILVE
JAPANESE GOLD
SOFT TORTOISE/HORN
CRYSTAL OXBLOOD
TITANIUM
MILKY CLOUDY BLUE
MILKY ROSE
SATIN NICKEL
MATTE CRYSTAL GREY/BLUE S
MATTE MAGNET GREY/BLUE IO
BLACK LEOPARD
BROWN LEOPARD
CRYSTAL CHARCOAL
MILKY TAUPE
MATTE CRYSTAL CLEAR
BROWN/TORTOISE
PURPLE/TORTOISE
RED/TORTOISE
CRYSTAL TEAL
TEAL LEOPARD
RED LEOPARD
CHARCOAL LEOPARD
CYRSTAL GRAY
CRYSTAL BLUSH
CRYSTAL BEIGE
CYRSTAL GREEN
MILKY CHARCOAL
BLACK/GLITTER
BROWN/GLITTER
PURPLE/GLITTER
MAGENTA/GLITTER
TEAL/PURPLE TORTOISE
LAVENDER/BLUSH TORTOISE
PEACH/BLUSH TORTOISE
MARBLE BEIGE
GOLD/POUDRE
BLACK HAVANA
BLUE VISETOS
BROWN LUREX
BLUE LUREX
RED LUREX
SHINY GOLD/WHITE
SHINY GOLD/ROSE
BROWN/ACID VISETOS
NAVY/SILVER GLITTER VISET
PETROL/ROSE VISETOS
AZURE VISETOS
VIOLET VISETOS
TURTLEDOVE VISETOS
ROSE BOUQUET
MARBLE BLUE
MARBLE PINK
DARK RUTHENIUM
AMBER
MATTE BLACK/SKY BLUE
MATTE CRYSTAL SLATE/BLUE
MATTE WOODGRAIN/COPPER IO
MATTE REDWOOD/BLUE SKY
BROWN FADE
SHINY GUNMETAL/BLUE
STRIPED BLACK/WHITE
STRIPED GREY
BROWN/STRIPED GREY
STRIPED BLUE/RED
RED SNAKE
ROSE SNAKE
TORTOISE/RED
TORTOISE/PURPLE
HAVANA/SHINY GOLD
PETROL/SHINY GOLD
BLACK/PETROL
ANTHRACITE/GREY SILVER FL
BLACK/SMOKE GRADIENT
WALNUT/BROWN
MATTE GOLD/G15
SHINY BLACK/SILVER FLASH
MATTE NAVY/BLUE FLASH
BLUE GREY/BLUE FLASH
GREY CRYSTAL/SMOKE
BROWN CRYSTAL/BROWN
NAVY CRYSTAL/BLUE
SHINY GUNMETAL/SMOKE
MATTE BLACK/SILVER FLASH
MATTE BLUE/GOLF
BLACK/BLACK PAISLEY
DARK BROWN/ORANGE PAISLEY
DARK BLUE/TURQUOISE PAISL
BORDEAUX/RED PAISLEY
BROWN/ORANGE PAISLEY
RED HAVANA
BLUE/TURQUOISE PAISLEY
STRIPED HONEY
PURPLE PAISLEY
TURTLEDOVE PAISLEY/BORDEA
DARK BROWN PAISLEY
FOG PAISLEY/PETROL
TURQUOISE BLUE MARBLE
VIOLET MARBLE
STRIPED ANTIQUE ROSE
STRIPED CYCLAMINE
STRIPED PURPLE
LIGHT BLUE
STRIPED RED
STRIPED GREY/PEACH
STRIPED BROWN/MINT
STRIPED PURPLE/LILAC
TAUPE MARBLE
GREEN MARBLE
PURPLE MARBLE
MATTE PETROL
CHARCOAL TORTOISE
CRYSTAL PETROL
CRYSTAL VIOLET
CRYSTAL RED
BLUE VIOLET
SAGE HORN
LAVENDAR
BLACK/JET
MILKY FOREST GREEN
NAVY HORN
HONEY TORTOISE
STRIPED AZURE BROWN
STRIPED PURPLE AZURE
STRIPED RED TORTOISE
GREY SNAKE
BLUE SNAKE
BLACK/ICE
TORTOISE/NUDE
LIGHT GREEN
MATTE AZURE
MATTE CYCLAMEN
LIGHT ROSE
MATTE REDWOOD
MATTE MAGNET GREY
MATTE DEEP NAVY
PONY BROWN
MATTE BEET RED
GREEN BOULDER TORTOISE
GLASS BLUE
MATTE BLACK/BLUE SKY ION
MATTE DEEP NAVY/BLUE ION
DEEP MOCHA
SUNRISE RED
STRIPED GREY BROWN
STRIPED AZURE TORTOISE
GOLD/TURTLEDOVE
ROSE GOLD/TURTLEDOVE
GRADIENT BLACK/GREY
GREDIENT GREY/ROSE
GRADIENT BROWN/HONEY
GRADIENT TURTLEDOVE/PEACH
GRADIENT BORDEAUX/GREY
MIDNIGHT NAVY
MILITARY HAVANA
MATTE BLACK/GREEN ML INFR
MT PLATINUM/GREEN IVORY/G
MT WINE/GRN FLSH INFRARED
MT BLACK/GRN ML INFRARED/
MT GREY/GRN SUPER IVORY/G
MT OBSIDIAN/GREY ML BLUE/
MATTE BLACK/GREY ML INFRA
MT BLACK/GRN/GREY ML INFR
MATTE BLACK/GREY ML BLUE
MATTE SEAWEED/GREY ML GRE
MATTE BLACK/GREY W/ BLACK
OBSIDIAN/BROWN SUPER IVOR
MATTE BLACK/POLARIZED GRE
BLONDE HAVAN
TORTOISE BLUE
STRIPED AQUA
STRIPED CYCLAMEN
GREY/HAVANA
SPOTTED HAVANA
BURNT HAVANA
GRAPHITE
GOLD SAND
EMERALD/TEAL GRADIENT
FUSCHIA/ORANGE GRADIENT
REAL TREE/BROWN
MILKY BLUE
MILKY PURPLE
BLACK/CARGO
ALMOND BROWN
BLACK/MT BLACK/ W/GRY POL
MATTE BLACK W/GREY POLARI
TOBACCO HORN
BLUE HORN DUSK
GUNMETAL/BLACK
BROWN/NAVY
BLACK/BLACK
MATTE ABYSS
MULTI TORTOISE
MATTE FOREST
MATTE GREY/MELON
MATTE TEAL
DARK SATIN BROWN
SATIN OLIVE
TORTOISE/GOLDEN
GREEN BOULDER TORTOISE/BL
BLUE GREY/SILVER FLASH
GREEN BOULDER TORTOISE/G1
STRAWBERRY
DUST
MAPLE RED
CRYSTAL CLEAR
BLACK W/GREY FLBL NIGHT L
WO GRY W/GRY ML VIOLET FL
MT WOLF GREY W/GRY ML RED
MT SEAWEED W/GRY FL ML GR
MATTE BLACK/SILVER W/GREY
CHARCOAL LAMINATE
BROWN/BLUSH LAMINATE
BLUE LAMINATE
PURPLE/MAUVE LAMINATE
BROWN / BLUSH LAMINATE
GREEN LAMINATE
BLUEGREY
LILAC HAVANA
PLUM LAMINATE
GRAY GRADIENT
GREEN GRADIENT
PLUM GRADIENT
MILKY CREAM
SHINY LIGHT GUNMETAL
CARGO KHAKI/VOLT
ANTHRACITE/PURE PLATINUM
OBSIDIAN/TIDE POOL BLUE
MIDNIGHT TURQUOISE
MARBLE BLACK
SAND/AZURE IRIDESCENT VIS
PEACH/ROSE IRIDESCENT VIS
PURPLE/SAND IRIDESCENT VI
ROSE/HONEY IRIDESCENT VIS
BLACK/CAMO LION
HAVANA/CAMO LION
SHINY DARK GUNMETAL/BLACK
SHINY GOLD/KHAKI
SHINY GOLD/TORTOISE
BEIGE / BLACK LAMINATE
BLACK/GOLD
IVORY/GOLD
OLIVE GREEN/AMBER GOLD
BORDEAUX/GOLD
BLACK GREY STONE
CRYSTAL QUARTZ
BROWN GREIGE STONE
BROWN QUARTZ
MATTE SILVER
SHINY DARK GUNMETAL
SHINY YELLOW GOLD
BLACK/G15
BLACK ANIMALIER
ANTIQUE ROSE ANIMALIER
HAVANA/BRONZE
HAVANA/PEACH
HAVANA/BROWN
HAVANA/GREEN ROSE
AMBER/WHITE MARBLE
GREEN/HAVANA
PEARL/CHAMPAGNE
PETROL/HAVANA
GOLD/HAVANA/BROWN LENS
GOLD/HAVANA/GREEN LENS
GOLD/HAVANA/FLASH BLUE LE
GOLD/MARBLE/REVO ROSE PEA
HAVANA TURQUOISE
HAVANA FUCHSIA
MEDIUM HORN
LIGHT HORN
WOLF GREY/RACER BLUE
MIDNIGHT NAVY/ELECTRO GRE
SQUADRON BLUE/MAX ORANGE
BLACK/GREY PAISLEY
BROWN/TAUPE PAISLEY
BORDEAUX/BORDEAUX PAISLEY
HAVANA/BLACK
PURPLE/GREY HAVANA
PLUMBERRY
BLUE-VIOLET/TEAL
BROWN PEWTER
MATTE BLACK RED/GREY
TORTOISE/G15
ANTHRACITE BLACK/W/GREEN
MATTE BLACK/GUNMETAL W/GR
DARK ATOMIC TEAL/BLACK W/
JET BLACK/GREY
MATTE BLACK/ORANGE ION
MATTE BLACK/SILVER ION
MATTE BLACK/BLUE ION
MATTE CRYSTAL SHADOW/BLUE
BROWN/BROWN PAISLEY
BLUE/BLUE PAISELY
BROWN/TURTLE DOVE PAISLEY
PETROL/AZURE PAISLEY
VIOLET PAISLEY
STRIPED BLUE GREY
MATTE BLACK/PEW W/AMBER L
ANTHRA/GUNMETAL W/GRN LEN
ANTHRACITE/COOL GREY W/GR
WOLF GREY/COOL GREY W/TEA
MT BLACK/DK AT TL W/AM LE
CAR KHAKI/MED OLI W/GRN L
MT DK TM RD/BLACK W/DK GR
MATTE DK TEAL/GREY GREEN
MATTE ANTHR BLK W/DK GRY
BLACK W/GREY SILVER FLASH
S SO WH W/GREY W SIL FL L
MATTE S VO W/GRY W/ SIL F
MATTE SOLAR RED/GREY PINK
MATTE GREY/GREY ML INFRAR
MT BLACK W/GREY STA GRN F
SHINY BLACK/GREY
WOODGRAIN/COPPER ION
TORTOISE PURPLE
MATTE MAGNET GREY/SILVER
MATTE SHADOW/COPPER
STRIPED BURNT BLUE
TORTOISE TURQUOISE
TORTOISE ROSE
SPOTTED BLUE
SPOTTED WINE
SHINY TORTOISE/G15
MATTE SOFT ICE/PEARL ION
CRYSTAL SHADOW/PEARL ION
CHARCOAL HORN/MARBLE
BROWN HORN / MARBLE LAMIN
NAVY HORN / MARBLE LAMINA
OPALINE BLUE
WINE/OCHRE
TURTLEDOVE/BRICK
BURGUNDY/GREY
HAVANA MULTICOLOR
BLU TORTOISE
GOLD BOURBON
GOLD/DARK BROWN
ROSE GOLD/NUDE
ROSE GOLD/BURGUNDY
BRIGHT GOLD
HAVANA/BURGUNDY
MILITARY/OCHRE
PETROL/BERRY
RUBY/PETROL
TURTLEDOVE/VIOLET
HAVANA/GREEN
VIOLET/BERRY
CHERRY/RED
PEACH/BRICK
AMBER/OCHRE
BEIGE TORTOISE
GEOMETRIC SAND
GEOMETRIC FOREST
GEOMETRIC BLUE
PINK TORTOISE
STRIPED SKY
STRIPED BOUQUET
BROWN/PINK TORTOISE
IRIDESCENT BLUE TORTOISE
IRIDESCENT PURPLE TORTOIS
GOLD/BEIGE
YELLOW
BLK/SIL/GREY POLARIZED LE
CAMEL
MATTE BLACK/DARK GREY LEN
MATTE TORTOISE/DARK BROWN
MATTE RUNWAY GOLD
MIDNIGHT TEAL
OPALINE WINE
OPALINE PEACH
GOLD/TORTOISE
ROSE GOLD/BORDEAUX
CRYSTAL PEACH
CRYSTAL KHAKI
CRYSTAL GREY
MATTE CHAPARRAL/BLUE
ALPINE TUNDRA/ORANGE
MATTE TORTOISE/AMBER
MATTE BLACK/ORANGE
MATTE COLLEGIATE NAVY/BLU
MATTE GRILL/RED
MATTE GRILL/ SMOKE
MATTE TORTOISE/AMBER FLAS
MATTE EVER BLUE/ BLUE
MATTE PIXEL/GREEN
MATTE SAGE/SILVER
GREY DUST
MATTE PIXEL/SILVER
SHINY BURGUNDY
SHINY CRYSTAL
CRYSTAL BORDEAUX
SATIN WALNUT/AMBER
SATIN BLACK/BLUE
SATIN GUNMETAL/ORANGE
SATIN WALNUT/FIRE
MATTE EVER BLUE/SILVER
ARMY GREEN
MATTE ARMY GREEN
SHINY GREEN
SHINY RED FUCHSIA
MATTE BLACK/SCOPH
MATTE BLACK/JAIME
MATTE BLACK/IUNA
MATTE BLACK/BRIAN
MATTE BLACK/ROSE GOLD ION
SEQUIOA
MATTE ATOMIC TEAL
MATTE PORT WINE
CLEAR
MATTE TEAM RED
HONEY WALNUT
AGAVE BLUE
SMOKE CRYSTAL/MULTI
MATTE CRYSTAL/BLACK
SMOKE CRYSTAL/BLACK
CRYSTAL/BLACK
MATTE SHADOW/RESIN
BLACK/RESIN
SATIN GUNMETAL/BARK
SATIN GUNMETAL/TORTOISE
MATTE BLACK/CRYSTAL
GREY HORN/BOULDER
SHINY TORTOISE/OLIVE
MATTE NAVY/SPACE BLUE
MATTE ABYSS BLUE
SATIN BLACK/RESIN
SATIN NAVY AGAVE
SATIN EGGPLANT
SATIN BLACK/GUNMETAL
SATIN BLACK/TEAL
SATIN GUNMETAL/GREY
SATIN WALNUT/GUNMETAL
SATIN ACAI/POMEGRANATE
SATIN NAVY/GUNMETAL
SHINY NICKEL
SHINY LIGHT GOLD
WHITE/CRYSTAL
CREAM TORTOISE/BLACK HORN
BROWN TORTOISE/CRYSTAL BR
ROSE TORTOISE/CRYSTAL ROS
BROWN HORN/SOFT TORTOISE
OLIVE HORN/TORTOISE
BLACK/CREAM HORN
SOFT TORTOISE/CREAM TORTO
BLUE/LIGHT BLUE HORN
BLUSH/BLUSH HORN
DARK TORTOISE/HORN/CRYSTA
GREEN/TOKYO TORTOISE
SHINY TITANIUM
SATIN ROSE GOLD
TEAL TORTOISE/CRYSTAL TEA
LIGHT BROWN HORN
LIGHT BLUE HORN
BLUSH HORN
MATTE WOLF GREY/GREY SILV
BLACK/GREY SILVER FLASH
MATTE VOLT/GREY SILVER FL
STRIPED TRNSPARENT BLUE P
STRIPED TRANSPARENT RED
BLACK/CLEAR LENS
SOLID BLACK
CRYSTAL NUDE
COOL GREY/GREY ML INFRARE
MATTE BLUE/GREY BLUE FLAS
MATTE BLACK/MATTE RACER B
BRUSHED GUNMETAL/MATTE AN
GUNMETAL/SOLAR RED
MATTE CRYSTAL GREY
MATTE TORTOISE/HONEY
WALNUT/VOLT
GUNMETAL/WOLF GREY
SATIN GUNMETAL/BLACK
BLACK/CRYSTAL CLEAR GUNME
BLACK/TOTAL CRIMSON
MATTE TORTOISE/ANTHRACITE
MATTE SPACE BLUE
BLACK/NEPTUNE GREEN
MATTE TORTOISE/PORT WINE
MATTE WOLF GREY/GREEN
GOLD/GREEN LENS
GOLD/BROWN LENS
GOLD/PETROL LENS
GOLD/LIGHT WINE LENS
GREIGE
AVIO
GOLD/BLONDE HAVANA
GOLD/IVORY
GOLD/MARBLE BEIGE
GOLD/PEACH LENS
GOLD/RAINBOW LENS
GOLD/FLASH BROWN LENS
SATIN GRAPHITE
HAVANA BLACK
TORTOISE/TURTLEDOVE VISET
BLUE/BLUE VISETOS
HAVANA/BURNT VISETOS
MILKY GREEN
MILKY PLUM
STRIPED GREY/BLUE
STRIPED BROWN/YELLOW
STRIPED SMOKE ROSE
STRIPED PURPLE BROWN
BLACK/BROWN TORTOISE
BLUE/GREY TORTOISE
DARK BROWN HORN
BLACK/IGUCHI
BLACK/SCOPH
BLACK/JAMIE LYNN
BLACK/IUNA TANTI
KHAKI MARBLE
SATIN GUNMETAL/DARK GREY
SATIN NAVY/WOLF GREY
AUTUMN
MINT GREEN
MATTE PALLADIUM
MATTE DARK BLUE
MATTE ELECTRIC BLUE
CRYSTAL CHAMPAGNE
MATTE GREEN/GREY
MATTE BLUE/GREY
MATTE RED/GREY
JET /G15
CHERRY
BLACK/POLAR
MATTE BLACK/GOLF FE
MT THUNDER BLUE/GOLF MLKY
PEWTER/GOLF MILKY BLUE
WALNUT/GOLF FLASH ELECTRI
TOTAL CRIMSON/GOLF FE
COPPER/BRN BRONZE FLSH GO
SATIN PEWTER/TERRAIN TINT
SATIN BLACK/GREY
MT DRK GRY/COURSE TINT/GR
MT GRY/TERRAIN TINT/GRY S
MATTE DARK GREY/GOLF FE
MATTE WOLF GREY/TERRAIN T
COGNAC
BLUSH/BROWN GRADIENT
OLIVE/BLUE GRADIENT
MILKY BROWN
MILKY BURGUNDY
NICKEL / GOLD
SMOKE LAMINATE
BROWN LAMINATE
NAVY PEACH LAMINATE
RED BLUE LAMINATE
BLUSH LAMINATE
MATTE GOLD
MARBLE GREEN
MARBLE ROSE
BLACK/PURPLE PAISLEY
PINK PAISLEY
YELLOW/BROWN PAISLEY
GREY/PETROL PAISLEY
LILAC PAISLEY
ORANGE/BURNT PAISLEY
GREEN HAVANA
HAVANA BURGUNDY
HAVANA PURPLE
BROWN/NUDE
MARBLE ROUGE
MARBLE BROWN AZURE
MARBLE BROWN RED
GOLD ROSE
GOLD BLUE
MARBLE PURPLE
MARBLE BROWN-AZURE
MARBLE BROWN-PURPLE
WALNUT/AMBER
SATIN NAVY/SILVER
GREY/GREY
KHAKI/TURTLE DOVE
PEACH/PEACH
ROSE/ROSE
MARBLE BLACK ROSE
STRIPED AQUA CYCLAMEN
STEELGREY
BLUE WITH AZURE PHOSPHO T
SHINY GUMETAL/MAPLE
SHINY GOLD/BIRCH
SHINY GOLD/PUMPKIN
BLACK/MARBLE
MINT FLOWERS
LILAC FLOWERS
ROSE FLOWERS
GREY/PEACH
CHOCOLATE/SKYBLUE
GREEN/ROSE
CHERRY/PEACH
SHINY GOLD/MINT
SHINY ROSE GOLD/BORDEAUX
BLACK/RUTHENIUM
HAVANA/DARK RUTHENIUM
HAVANA/GOLD
SHINY ROSE GOLD/NUDE
SHINY GOLD/BURGUNDY
HAVANA/KHAKI
STRIPED MILITARY GREEN
BLACK/THUNDER BLUE
BLACK/WOLF GREY
BLUE BARK/GREY
MATTE BLUE TORTOISE/SMOKE
BLACK BARK/SMOKE
MATTE TOKYO TORTOISE/G15
SATIN BLACK/CRYSTAL
SATIN EARTH/HONEY
SATIN NAVY/SPACE BLUE
BLUE HORN DUSK/SMOKE
TOBACCO HORN/SMOKE
TOKYO TORTOISE/G15
BLACK/BURGUNDY LAMINATE
DARK ROSE
BLUSH/BURUGNDY LAMINATE
VINTAGE TORTOISE
LIGHT GREY
BLACK/WHITE/BLUE
BLUE STORM
PEWTER
SATIN WALNUT/CARGO KHAKI
BLACK/CHARCOAL TORTOISE
MATTE BLACK/CHARCOAL TORT
SATIN TITANIUM/BROWN HORN
SATIN TITANIUM/NAVY HORN
SATIN NICKEL/AMBER TORTOI
CHARCOAL TORTOISE/TITANIU
CREAM TORTOISE/NICKEL
ROSE TORTOISE/ROSE GOLD
BLACK/GREY HORN
CHARCOAL TORTOISE/CRYSTAL
CHARCOAL HORN
SLATE HORN
CHARCOAL HORN GRADIENT
AMBER HORN GRADIENT
BLUSH HORN GRADIENT
MATTE BLACK/GOLD
MATTE BROWN/GOLD
MATTE TAUPE/ROSE GOLD
MATTE TEAL/NICKEL
BLACK/NICKEL
GREY HORN/ROSE GOLD
SOFT TORTOISE/GOLD
LIGHT BLUE HORN/GOLD
BLUE BROWN
RED GRADIENT
NAVY/CREAM LAMINATE
BURGUNDY/BLACK LAMINATE
BLACK/WHITE CONFETTI
TEAL GRADIENT
BLACK/WHITE LAMINATE
BLUSH/BURGUNDY LAMINATE
MATTE TEAL HORN
MATTE RED HORN
ANTIQUE RUTHENIUM/HAVANA
ANTIQUE RUTHENIUM/BLACK
MATTE MUSTARD
SMOKE HORN
RED HORN
BLUSH CAMEL
TAUPE BROWN
MINT
BEIGE/NAVY GRADIENT
PINK GRADIENT
BROWN WOOD
CRYSTAL SMOKE/BLACK
CRYSTAL LIGHT GREEN/MINT
CRYSTAL LIGHT BLUE/SKY
CRYSTAL/STRIPES
KHAKI TORTOISE
MILKY PINK
MILKY PALE YELLOW
MILKY ORANGE
CRYSTAL/WHITE
CRYSTAL/GREEN
CARGO GREEN
SMOKE TORTOISE
WHITE/BLACK STRIPES
SILVER/GREEN
SILVER/VIOLET
ROSE GOLD/PINK
GUNMETAL/OXBLOOD
SILVER/BLUE
GOLD/MAIZE
BLACK GRADIENT
BLUSH GRADIENT
SILVER/GOLD
CRYSTAL BEIGE/BROWN
CRYSTAL PALE YELLOW/CREAM
CRYSTAL SMOKE/STRIPES
CRYSTAL TAUPE/BROWN
CRYSTAL LIGHT BLUE/NAVY
CHARCOAL/GREY GRADIENT
GREEN/TEAL GRADIENT
PLUM/CORAL GRADIENT
CRYSTAL TEAL LAMINATE
CRYSTAL MAUVE LAMINATE
CRYSTAL BRICK RED
TORTOISE/YELLOW
TEAL/LIGHT BLUE
RED/BLUSH
GOLD/HAVANA/GRAD AZURE RO
GOLD/OPALINE BEIGE
GOLD/TRANSPARENT MUD
GOLD/GREY ORANGE YELLOW L
GOLD/BROWN ROSE SAND LENS
GOLD/PURPLE AZURE CRYSTAL
GOLD/TRANSPARENT GREY
GOLD/TRANSP.GREY/GRAD.GRE
GOLD/HAVANA/GRAD GREEN RO
GOLD/CARAMEL
GOLD/GRADIENT BROWN
GUNMETAL/BRICK
SILVER/NAVY
LIGHT BLUE TORTOISE
PEACH TORTOISE
CHARCOAL HAVANA
AMBER HAVANA
NAVY HAVANA
TAUPE TORTOISE
BLACK/SLATE
CARGO/YELLOW
OXBLOOD/BLUE
MAIZE
MILKY LIGHT BLUE
WHITE/BLUE/BEIGE LAMINATE
NAVY/TEAL/PEACH LAMINATE
TEAL LAMINATE
BLACK/BROWN LAMINATE
BURGUNDY/PINK LAMINATE
BLACK/STRIPES
GREY/BLUE
KHAKI TORTOISE/BLACK
PINE/LIGHT GREEN
BLACK/COBALT
TORTOISE/AMBER
NAVY/ORANGE
NAVY TEAL HORN
LAVENDER HORN
HAVANA/PEWTER
ANTIQUE BROWN
DARK GUNMETAL
CAFE
LILAC MIST
PINK MIST
SOFT SATIN PURPLE
ROSE 601
BLACK/GOLDEN
MOCHA MARBLE
SKY
PURPLE FANTASY
SPRING ROSE
MISTY ROSE
CAFE

*/
